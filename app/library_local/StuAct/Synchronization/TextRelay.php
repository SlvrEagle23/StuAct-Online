<?php
namespace StuAct\Synchronization;

use \Entity\Settings;

class TextRelay extends TaskAbstract
{
	protected function _run()
	{
		$config = \Zend_Registry::get('config');
		$textrelay_settings = $config->services->textrelay->toArray();
		
		$channel = new \Zend_Feed_Rss($textrelay_settings['rss_source']);
		$sync_time = Settings::getSetting('gv_sync_time', time());
		
		foreach($channel as $item)
		{
			$publication_date = strtotime($item->pubDate);
			$text_message = str_replace('TAMUCodeMaroon: ', '', $item->description);
			
			if ($publication_date > $sync_time)
			{
				foreach($textrelay_settings['destinations'] as $sms_destination)
				{
					\DF\Service\Twilio::sms($sms_destination, $text_message);
				}
			}
		}
		
		Settings::setSetting('gv_sync_time', time());
		return true;
	}
}