<?php
namespace StuAct\Synchronization;

use \Entity\Organization;
use \StuAct\WebHostManager\Listserv;

class Listservs extends TaskAbstract
{
	protected function _run()
	{
		$config = \DF\Registry::get('config');
		$settings = $config->listservs->toArray();
		$org_settings = Organization::loadSettings();

		$listserv_org = Organization::find($settings['org_id']);
		$listserv = new Listserv($listserv_org);
		
		$managed_listservs = array(
			'advisors'					=> $settings['advisor_list_name'],
			'leaders'					=> $settings['leader_list_name'],
			'sponsored_leaders'			=> $settings['leader_sponsored_list_name'],
			'treasurers'				=> $settings['treasurer_list_name'],
		);

		foreach($settings['advisor_category_lists'] as $category_name => $list_name)
		{
			$managed_listservs['advisors_'.$category_name] = $list_name;
		}
		
		foreach($managed_listservs as $list_abbr => $list_name)
		{
			$listserv->removeAllMembers($list_name, Listserv::PRIV_RECV_ONLY);
		}
		
		$advisor_positions = array_values($org_settings['officer_groups']['advisors']);
		$listserv_positions = array_merge($advisor_positions, array(ORG_OFFICER_CHIEF_STUDENT_LEADER, ORG_OFFICER_TREASURER));

		$listserv_addresses_raw = $this->em->createQuery('SELECT oo.position, u.email, o.category FROM \Entity\OrganizationOfficer oo LEFT JOIN oo.user u LEFT JOIN oo.organization o WHERE oo.position IN (:positions) AND o.status IN (:statuses) GROUP BY u.id')
			->setParameter('positions', $listserv_positions)
			->setParameter('statuses', $org_settings['active_codes'])
			->getArrayResult();
		
		$addresses = array();
		
		// Populate arrays containing all advisors and advisors for specific categories of organization.
		foreach($listserv_addresses_raw as $listserv_address_raw)
		{
			switch($listserv_address_raw['position'])
			{
				case ORG_OFFICER_CHIEF_STUDENT_LEADER:
					$addresses['leaders'][] = $listserv_address_raw['email'];

					if ($listserv_address_raw['category'] == "Sponsored")
						$addresses['sponsored_leaders'][] = $listserv_address_raw['email'];
				break;
				
				case ORG_OFFICER_TREASURER:
					$addresses['treasurers'][] = $listserv_address_raw['email'];
				break;
				
				default:
					$addresses['advisors'][] = $listserv_address_raw['email'];

					if (isset($listserv_address_raw['category']))
						$addresses['advisors_'.$listserv_address_raw['category']][] = $listserv_address_raw['email'];
				break;
			}
		}
		
		foreach($managed_listservs as $list_abbr => $list_name)
		{	
			$listserv->addMembers($list_name, $addresses[$list_abbr], Listserv::PRIV_RECV_ONLY, FALSE);
		}
	}
}