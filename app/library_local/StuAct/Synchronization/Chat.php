<?php
/** 
 * WebIM account synchronization with Staff Directory
 */
namespace StuAct\Synchronization;

use \Entity\Settings;
use \Entity\User;
use \Entity\Role;

class Chat extends TaskAbstract
{
	protected function _run()
	{
		$directory_feed = file_get_contents('https://dsanet.tamu.edu/directory/index/index/id/13/format/feed');
		$areas = \Zend_Json::decode($directory_feed);

		$db = new \Zend_Db_Adapter_Pdo_Mysql(array(
			'host'		=> 'mysql2.dsa.reldom.tamu.edu',
			'username' 	=> 'app_stuactonline',
			'password'	=> 'uQ3AfiMaNRiD7eQoF0LY',
			'dbname'	=> 'app_stuactonline_webim',
		));
		$db->getConnection();
		$db->query('SET NAMES utf8');
		$db->query('SET CHARACTER SET utf8');

		$staff_member_role = Role::getRepository()->findOneByName('Staff Member');

		$active_users = array();

        foreach($areas as $area)
        {
            $chat_record = array(
                'vclocalname'           => $area['name'],
                'vccommonname'          => $area['name'],
                'vclocaldescription' => '',
                'vccommondescription' => '',
            );
            
            // Locate an existing group by this name.
            $chat_group_id = NULL;
            
            $existing_record = $db->fetchRow('SELECT groupid FROM chatgroup WHERE vclocalname = ?', array($chat_record['vclocalname']));
            if ($existing_record)
                $chat_group_id = (int)$existing_record['groupid'];
            
            // Update or insert the group information.
            if ($chat_group_id)
            {
                $db->update('chatgroup', $chat_record, 'groupid = '.$chat_group_id);
            }
            else
            {
                $db->insert('chatgroup', $chat_record);
                $chat_group_id = $db->lastInsertId();
            }

            foreach((array)$area['listings'] as $listing)
            {
            	foreach((array)$listing['users'] as $user)
            	{
            		$user_obj = User::getRepository()->findOneByUin($user['uin']);

            		if ($user_obj instanceof User)
            		{
            			$active_users[] = $user_obj->id;

            			// Assign "Staff Member" role.
            			$user_obj->roles->removeElement($staff_member_role);
            			$user_obj->roles->add($staff_member_role);
            			$user_obj->save();

            			// Assign chat permissions.
            			if ($user_obj->chat_password)
            				$password = $user_obj->chat_password;
            			else
            				$password = \DF\Utilities::generatePassword();

            			$listing_name = $user_obj->firstname.' '.$user_obj->lastname.' - '.$listing['title'];
            			$listing_email = $user_obj->email;
			            
			            $chat_record = array(
			                'vclogin'               => 'stuactstaff_'.$listing['id'].'_'.$user_obj->id,
			                'vcpassword'            => md5($password),
			                'vclocalename'          => substr($listing_name, 0, 64),
			                'vccommonname'          => substr($listing_name, 0, 64),
			                'vcemail'               => $listing_email,
			                'iperm'                 => 65526,
			            );
			            
			            $operator_id = NULL;
			            if ($user_obj->chat_username)
			            {
			                $operator_id_raw = $db->fetchRow('SELECT operatorid FROM chatoperator WHERE vclogin = ?', array($chat_record['vclogin']));
			                
			                if ($operator_id_raw)
			                    $operator_id = (int)$operator_id_raw['operatorid'];
			            }
			            
			            if ($operator_id == 0)
			            {
			                $db->insert('chatoperator', $chat_record);
			                $operator_id = $db->lastInsertId();

			                $user_obj->chat_username = $chat_record['vclogin'];
			                $user_obj->chat_password = $password;
			                $user_obj->save();
			            }
			            else
			            {
			                $db->update('chatoperator', $chat_record, 'vclogin = '.$db->quote($chat_record['vclogin']));
			            }
			            
			            // Add the user to the proper groups.
			            $db->delete('chatgroupoperator', 'operatorid = '.$operator_id);
			            
			            if (isset($area_chat_groups[$listing['area_id']]))
			            {
			                $new_record = array(
			                    'groupid'       => $chat_group_id,
			                    'operatorid'    => $operator_id,
			                );
			                $db->insert('chatgroupoperator', $new_record);
			            }
            		}
            	}	
            }
        }

        $this->em->createQuery('UPDATE Entity\User u SET u.chat_username=NULL, u.chat_password=NULL WHERE u.id NOT IN (:user_id)')
        	->setParameter('user_id', $active_users)
        	->execute();

		return true;
	}
}

