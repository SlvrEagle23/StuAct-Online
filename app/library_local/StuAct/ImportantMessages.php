<?php
namespace StuAct;

use \Entity\User;
use \Entity\Organization;
use \Entity\PreEventForm;
use \Entity\NewOrganization;

class ImportantMessages
{
	public static function fetchForUser(User $user)
	{
		$acl = \DF\Acl::getInstance();
		$important_messages = array();

		$is_admin = $acl->userAllowed('administer all', $user);

		$em = \DF\Doctrine\Entity::getEntityManager();
		$org_settings = Organization::loadSettings();
		
		// Upcoming events.
		$upcoming_events = $user->event_attendance;

		if (count($upcoming_events) > 0)
		{
			$event_manage_url = \DF\Url::route(array('module' => 'events', 'action' => 'registered'));

			foreach($upcoming_events as $ea)
			{
				$event = $ea->event;
				if (!$event->isUpcoming())
					continue;

				$event_text = 'You are currently registered to attend the "<b>'.$event->title.'</b>" event on '.$event->date_text.'. ';
				
				if ($event->location)
					$event_text .= 'This event will take place at '.$event->location.'. ';
				
				$event_text .= 'For more information, <a href="'.$event_manage_url.'">click here</a>.';
				
				$important_messages[] = array(
					'icon'	=> 'calendar',
					'title' => 'Upcoming Event',
					'text'	=> $event_text,
				);
			}
		}
		
		// Pending advisor requests.
		if ($acl->userAllowed('admin_orgs', $user) && !$is_admin)
		{
			$visible_requests = $org_settings['officer_groups']['advisors'];
			$requests_raw = $em->createQuery('SELECT COUNT(oor.id) AS num_requests FROM Entity\OrganizationOfficerRequest oor WHERE oor.position IN (:positions)')
				->setParameter('positions', $visible_requests)
				->getArrayResult();

			$num_requests = (int)$requests_raw[0]['num_requests'];
			
			if ($num_requests > 0)
			{
				$important_messages[] = array(
					'icon'	=> 'user_gray',
					'title'	=> $num_requests.' Advisor Requests',
					'text'	=> '<a href="/online/admin/view_pending_requests.php">Manage Advisor Requests</a>',
				);
			}
		}

		// Pending constitutions.
		if ($acl->userAllowed(array('admin_orgs','admin_const'), $user) && !$is_admin)
		{
			$num_const_raw = $em->createQuery('SELECT COUNT(o.id) AS num_const FROM Entity\Organization o WHERE o.flag_constitution = :const_flag AND (o.constitution_claimant IS NULL OR o.constitution_claimant = :empty OR o.constitution_claimant = :username)')
				->setParameter('const_flag', ORG_FLAG_SACT_ACTION_REQ)
				->setParameter('empty', '')
				->setParameter('username', $user->username)
				->getArrayResult();

			$num_const = (int)$num_const_raw[0]['num_const'];

			if ($num_const)
			{
				$important_messages[] = array(
					'icon'	=> 'script',
					'title' => $num_const.' Pending Constitutions',
					'text'	=> '<a href="/online/admin/manage_constitutions.php">Manage Constitutions</a>',
				);
			}
		}

		// Pending Web/Drupal Requests
		if ($acl->userAllowed('administer web sites', $user))
		{
			$num_it_raw = $em->createQuery('SELECT COUNT(o.id) AS num_it FROM Entity\Organization o WHERE o.it_web_pending = 1 OR o.it_web_drupal_pending = 1')
				->getArrayResult();

			$num_it = (int)$num_it_raw[0]['num_it'];

			if ($num_it > 0)
			{
				$important_messages[] = array(
					'icon'	=> 'world',
					'title' => $num_it.' Pending IT Requests',
					'text'	=> '<a href="'.\DF\Url::route(array('module' => 'admin', 'controller' => 'webs')).'">Manage IT Requests</a>',
				);
			}
		}

		// Pre-Event Planning Forms
		if ($acl->userAllowed('admin_forms_pep', $user) && !$is_admin)
		{
			$unclaimed_pep = $em->createQuery('SELECT COUNT(p.id) AS total FROM Entity\PreEventForm p WHERE p.phase = :phase AND p.claimant_user_id IS NULL')
				->setParameter('phase', PreEventForm::PHASE_DEPT_ACTIVE)
				->getArrayResult();

			$unclaimed_pep = (int)$unclaimed_pep[0]['total'];

			if ($unclaimed_pep > 0)
			{
				$important_messages[] = array(
					'icon'	=> 'application_edit',
					'title' => $unclaimed_pep.' Unclaimed PEP Forms',
					'text'	=> '<a href="/online/admin/manage_forms_pep.php">Manage Pre-Event Forms</a>',
				);
			}

			$claimed_pep = $em->createQuery('SELECT COUNT(p.id) AS total FROM Entity\PreEventForm p WHERE p.phase = :phase AND p.claimant_user_id = :user_id')
				->setParameter('phase', PreEventForm::PHASE_DEPT_ACTIVE)
				->setParameter('user_id', $user->id)
				->getArrayResult();

			$claimed_pep = (int)$claimed_pep[0]['total'];

			if ($claimed_pep > 0)
			{
				$important_messages[] = array(
					'icon'	=> 'application_edit',
					'title' => $claimed_pep.' PEP Forms Assigned to You',
					'text'	=> '<a href="/online/admin/manage_forms_pep.php">Manage Pre-Event Forms</a>',
				);
			}
		}

		// NSO Forms
		if ($acl->userAllowed('admin_forms_nso', $user) && !$is_admin)
		{
    		if ($acl->userAllowed('admin_forms_nso_rec', $user))
    		{
    			$num_forms = $em->createQuery('SELECT COUNT(no.id) AS total FROM Entity\NewOrganization no WHERE no.phase = :phase')
    				->setParameter('phase', NewOrganization::PHASE_AWAITING_ASSIGNMENT)
    				->getArrayResult();

    			$num_forms = (int)$num_forms[0]['total'];

    			if ($num_forms > 0)
    			{
    				$important_messages[] = array(
						'icon'	=> 'application_edit',
						'title' => $num_forms.' NSOs Awaiting Assignment',
						'text'	=> '<a href="/online/admin/manage_nsos.php?phase='.NewOrganization::PHASE_AWAITING_ASSIGNMENT.'">Manage NSO Applications</a>',
					);
    			}
    		}

    		if ($acl->userAllowed('admin_forms_nso_risk', $user))
    		{
    			$num_forms = $em->createQuery('SELECT COUNT(no.id) AS total FROM Entity\NewOrganization no WHERE no.phase = :phase AND no.claimant_user_id = :user_id')
    				->setParameter('phase', NewOrganization::PHASE_AWAITING_REVIEW)
    				->setParameter('user_id', $user->id)
    				->getArrayResult();

    			$num_forms = (int)$num_forms[0]['total'];

    			if ($num_forms > 0)
    			{
    				$important_messages[] = array(
						'icon'	=> 'application_edit',
						'title' => $num_forms.' NSOs Awaiting Review',
						'text'	=> '<a href="/online/admin/manage_nsos.php?phase='.NewOrganization::PHASE_AWAITING_REVIEW.'">Manage NSO Applications</a>',
					);
    			}
    		}

    		if ($acl->userAllowed('admin_forms_nso_sofc', $user))
    		{
    			$num_forms = $em->createQuery('SELECT COUNT(no.id) AS total FROM Entity\NewOrganization no WHERE no.phase = :phase')
    				->setParameter('phase', NewOrganization::PHASE_DEPT_SOFC)
    				->getArrayResult();

    			$num_forms = (int)$num_forms[0]['total'];

    			if ($num_forms > 0)
    			{
    				$important_messages[] = array(
						'icon'	=> 'application_edit',
						'title' => $num_forms.' NSOs Pending SOFC',
						'text'	=> '<a href="/online/admin/manage_nsos.php?phase='.NewOrganization::PHASE_DEPT_SOFC.'">Manage NSO Applications</a>',
					);
    			}
    		}
		}

		// Training expiration.
		$training_url = \DF\Url::route(array('module' => 'training'));
		$training_lifespan = $org_settings['lifespan_training'];

		$timestamp_limit = time() - $training_lifespan;
		$timestamp_notification = $timestamp_limit + $org_settings['training_notification'];

		if ($user->flag_training == USR_FLAG_MEETS_REQ && $user->flag_training_timestamp < $timestamp_notification)
		{
			$training_expires = $user->flag_training_timestamp + $training_lifespan;
	
			$important_messages[] = array(
				'icon'	=> 'user',
				'text'	=> 'Your training credit for the '.$training_name.' will expire on '.date('F j, Y', $training_expires).'. For instructions on renewing this credit, visit your <a href="'.$training_url.'">training profile</a>.',
			);
		}
		
		return $important_messages;
	}
	
	public static function fetchForOrg(Organization $org)
	{
		$important_messages = array();

		$org_settings = Organization::loadSettings();
		$acl = \DF\Acl::getInstance();
		$org_acl = \StuAct\Acl\Organization::getInstance();
		$user = \DF\Auth::getLoggedInUser();

		$em = \DF\Doctrine\Entity::getEntityManager();

		$is_admin = $acl->isAllowed('admin_orgs');
		$position = $org->getUserPosition();

		// Roster Requests Notice
		$requests_num = 0;

		if (count($org->officer_requests) > 0)
		{
			foreach($org->officer_requests as $request)
			{
				if ($org_acl->canModifyPosition($request->position, $org, $user))
					$requests_num++;
			}
		}

		if ($requests_num != 0)
		{
			$important_messages[] = array(
				'icon'	=> 'group',
				'text'	=> 'There are currently '.$requests_num.' pending request(s) for this organization\'s roster. To view these requests, <a href="'.$org->route('roster').'">click here</a>.',
			);
		}
		
		// Member renewal process notification
		if ($org->status == ORG_STATUS_IN_TRANSITION && !$position->isRenewed())
		{
			$important_messages[] = array(
				'icon'	=> 'user_go',
				'text'	=> 'You must complete the annual officer renewal process for this organization. This process will allow you to indicate whether you will continue to serve as an officer for the coming year. To complete this requirement, <a href="'.$org->route(array('controller' => 'roster', 'action' => 'renew')).'/members/renew">click here</a>.',
			);
		}

		// Organization Cycle Notice
		if ($org->status == ORG_STATUS_RECOGNIZED)
		{
			$rec = $org->getRecognitionManager();
			$cycle_change = $rec->getCycleChange();
			$buffer = time() + 86400*30;

			if ($cycle_change <= $buffer)
			{
				$important_messages[] = array(
					'icon'	=> 'flag_blue',
					'text'	=> 'Your next recognition cycle is approaching! On '.date('F j, Y', $cycle_change).', your organization\'s recognition requirements will be reset for the upcoming year. Any changes made after '.date('F j, Y', $cycle_change).' will carry across to the next recognition cycle.',
				);
			}
		}

		// Pre-Event Planning Form Reminders
		$pep_totals_raw = $em->createQuery('SELECT fp.phase, COUNT(fp.id) AS num_forms FROM Entity\PreEventForm fp WHERE fp.org_id = :org_id GROUP BY fp.phase')
			->setParameter('org_id', $org->id)
			->getArrayResult();

		$pep_totals = array();
		foreach($pep_totals_raw as $pep_total)
			$pep_totals[$pep_total['phase']] = (int)$pep_total['num_forms'];

		$pep_url = \DF\Url::route(array('module' => 'form_preevent', 'controller' => 'index', 'action' => 'index', 'org_id' => $org->id));

		if ((int)$pep_totals[PreEventForm::PHASE_INITIAL] > 0)
		{
			$num_forms = $pep_totals[PreEventForm::PHASE_INITIAL];
			$important_messages[] = array(
				'icon'	=> 'page_error',
				'text'	=> 'This organization has '.$num_forms.' incomplete Pre-Event Planning Form(s). To view all forms for this organization, <a href="'.$pep_url.'">click here</a>.',
			);
		}
		if ((int)$pep_totals[PreEventForm::PHASE_MODIFY] > 0)
		{
			$num_forms = $pep_totals[PreEventForm::PHASE_MODIFY];
			$important_messages[] = array(
				'icon'	=> 'page_red',
				'text'	=> 'This organization has '.$num_forms.' Pre-Event Planning Form(s) that require additional modifications for advisor approval. To view all forms for this organization, <a href="'.$pep_url.'">click here</a>.',
			);
		}
		if ((int)$pep_totals[PreEventForm::PHASE_ADVISOR] > 0)
		{
			$num_forms = $pep_totals[PreEventForm::PHASE_ADVISOR];
			$important_messages[] = array(
				'icon'	=> 'page_green',
				'text'	=> 'This organization has '.$num_forms.' Pre-Event Planning Form(s) that require advisor approval to continue. To view all forms for this organization, <a href="'.$pep_url.'">click here</a>.',
			);
		}
		if ((int)$pep_totals[PreEventForm::PHASE_DEPT_ACTIVE] > 0)
		{
			$num_forms = (int)$pep_totals[PreEventForm::PHASE_DEPT_ACTIVE];
			$important_messages[] = array(
				'icon'	=> 'page_find',
				'text'	=> 'The Department of Student Activities is currently reviewing '.$num_forms.' Pre-Event Planning Form(s) submitted by this organization. To view all forms for this organization, <a href="'.$pep_url.'">click here</a>.',
			);
		}

		return $important_messages;
	}
}