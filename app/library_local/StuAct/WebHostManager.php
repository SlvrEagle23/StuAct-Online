<?php
namespace StuAct;

class WebHostManager
{
	const USER_WWW_DIRECTORY = 'www';
	const USER_WEB_DIRECTORY = 'public_html';
	const USER_OLD_DIRECTORY = 'public_html/old';
	const USER_BACKUP_DIRECTORY = 'backup';
	
	private $_cpanel_server;
	private $_cpanel_host;
	private $_cpanel_user;
	private $_cpanel_accesshash;
	
	private $_cpanel_error;
	private $_cpanel_message;
	
	private $_cpanel_default_package;
	private $_ssh;
	private $_http_client;
		
	public function __construct()
	{
		$config = \Zend_Registry::get('config');
		$settings = $config->services->cpanel->toArray();
	
		$this->_cpanel_server = $settings['server_index'];
		$this->_cpanel_host = $settings['host'];
		$this->_cpanel_user = $settings['user'];
		$this->_cpanel_accesshash = preg_replace("'(\r|\n)'","",$settings['accesshash']);
		$this->_cpanel_default_package = $settings['default_package'];
		$this->_cpanel_error = false;
	}
	
	// Create an account.
	public function createAccount($domain, $username, $password, $package = NULL)
	{
		if (is_null($package))
		{
			$package = $this->_cpanel_default_package;
		}

		$request_info = array(
			'username'	=> $username,
			'domain'	=> $domain,
			'password'	=> $new_password,
			'plan'		=> $package,
		);
		$result = $this->requestWhmApi('createacct', $request_info);
		
		return ($result && !$this->_cpanel_error);
	}
	
	// Change an account password.
	public function changePassword($username, $new_password)
	{
		$new_password_info = array(
			'user'		=> $username,
			'pass'		=> $new_password,
		);
		$result = $this->requestWhmApi('passwd', $new_password_info);
				
		return ($result && !$this->_cpanel_error);
	}
	
	// Change an account's associated domain.
	public function changeDomain($username, $new_domain)
	{
		$new_info = array(
			'domain'	=> $new_domain,
			'user'		=> $username,
			'CPTHEME'	=> 'x3',
			'HASCGI'	=> 0,
			'LANG'		=> 'english',
			'MAXFTP'	=> '2',
			'MAXSQL'	=> '3',
			'MAXPOP'	=> '0',
			'MAXLST'	=> '0',
			'MAXSUB'	=> 'null',
			'MAXPARK'	=> 'null',
			'MAXADDON'	=> 'null',
			'shell'		=> 0,
		);
		$user_change_request = $this->requestWhmApi('modifyacct', $new_info);
		
		if ($user_change_request)
		{
			$user_change_result = $this->getLastMessage();
			
			$operation_status = $user_change_result['modifyacct']['result'][0]['status'];
			return ($operation_status == NCE_OPT_YES);
		}
		else
		{
			return FALSE;
		}
	}
	
	// Change an account's associated username.
	public function changeUsername($username, $new_username)
	{
		$user_info_request = $this->requestWhmApi('listaccts', array('searchtype' => 'user', 'search' => $username));
		
		if ($user_info_request)
		{
			$user_info = $this->getLastMessage();
			$user_domain = $user_info['listaccts']['acct'][0]['domain'];
		
			$new_info = array(
				'domain'	=> $user_domain,
				'user'		=> $username,
				'newuser'	=> $new_username,
				'CPTHEME'	=> 'x3',
				'HASCGI'	=> 0,
				'LANG'		=> 'english',
				'MAXFTP'	=> '2',
				'MAXSQL'	=> '3',
				'MAXPOP'	=> '0',
				'MAXLST'	=> '0',
				'MAXSUB'	=> 'null',
				'MAXPARK'	=> 'null',
				'MAXADDON'	=> 'null',
				'shell'		=> 0,
			);
			$user_change_request = $this->requestWhmApi('modifyacct', $new_info);
			
			if ($user_change_request)
			{
				$user_change_result = $this->getLastMessage();
			
				$operation_status = $user_change_result['modifyacct']['result'][0]['status'];
				return ($operation_status == NCE_OPT_YES);
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}

	// Suspend an account.
	public function suspendAccount($username, $reason = 'Suspended From Remote Site')
	{
		$request_info = array(
			'user'		=> $username,
			'reason'	=> $reason,
		);
		$result = $this->requestWhmApi('suspendacct', $request_info);
		
		return ($result && !$this->_cpanel_error);
	}
	
	// Unsuspend an account.
	public function unsuspendAccount($username)
	{
		$request_info = array(
			'user'		=> $username,
		);
		$result = $this->requestWhmApi('unsuspendacct', $request_info);
		
		return ($result && !$this->_cpanel_error);
	}
	
	// Kill an account.
	public function killAccount($username)
	{
		$request_info = array(
			'user'		=> $username,
		);
		$result = $this->requestWhmApi('removeacct', $request_info);
		
		return ($result && empty($this->_cpanel_error));
	}
	
	// Show version.
	public function showVersion()
	{
		$version_request = $this->requestWhmApi('version');
		
		if ($version_request)
		{
			$version_data = $this->getLastMessage();
			
			return $version_data['version']['version'];
		}
		else
		{
			return FALSE;
		}
	}
	
	// List accounts.
	public function listAccounts()
	{
		$result = $this->request('/scripts2/listaccts', array(
			'nohtml'		=> 1,
			'viewall'		=> 1,
		));
		
		if ($this->_cpanel_error) {
			return false;
		}
		else
		{
			$page = split("\n",strip_tags($result));
			foreach ($page as $line)
			{
				list($acct,$contents) = split("=", $line);
				if ($acct != "")
				{
					$allc = split(",", $contents);
					$accts[$acct] = $allc;
				}
			}
			return($accts);
		}
	}
	
	// List packages.
	public function listPackages()
	{
		$result = $this->request('/scripts/remote_listpkg');
		
		if ($this->_cpanel_error) {
			return false;
		}
		else
		{
			$page = split("\n",strip_tags($result));
			foreach ($page as $line)
			{
				list($pkg,$contents) = split("=", $line);
				if ($pkg != "")
				{
					$allc = split(",", $contents);
					$pkgs[$pkg] = $allc;
				}
			}
			return($pkgs);
		}
	}
	
	// Retrieve the last error message.
	public function getLastError()
	{
		return $this->_cpanel_error;
	}
	
	public function getLastMessage()
	{
		return $this->_cpanel_message;
	}
	
	private function getHttpClient()
	{
		if (!is_object($this->_http_client))
		{
			$this->_http_client = new \Zend_Http_Client();
            
            $adapter = new \Zend_Http_Client_Adapter_Curl();
            $adapter->setConfig(array(
                'curloptions' => array(
                    CURLOPT_SSL_VERIFYPEER => FALSE,
                    CURLOPT_SSL_VERIFYHOST => FALSE,
                ),
            ));
            
            $this->_http_client->setAdapter($adapter);
            
			$this->_http_client->setConfig(array(
				'timeout'		=> 45,
				'keepalive' 	=> true,
                
			));
            
			$this->_http_client->setHeaders('Authorization: WHM '.$this->_cpanel_user.":".$this->_cpanel_accesshash);
		}
		
		$this->_http_client->resetParameters();
		
		return $this->_http_client;
	}
		
	// Internal request processing function.
	public function request($request_uri, $request_params = array())
	{
		$this->_cpanel_error = false;
		$this->_cpanel_message = NULL;
		
		$client = $this->getHttpClient();
		
		$client->setUri('https://'.$this->_cpanel_host.':2087'.$request_uri);
		$client->setParameterGet($request_params);
		
		try
		{
			$response = $client->request('GET');
		}
		catch(\Exception $e)
		{
			$this->_cpanel_error = 'Server is currently unavailable. ('.$e->getMessage().')';
			return false;
		}
        
		if ($response->isSuccessful())
		{
			return $response->getBody();
		}
		else
		{	
			$this->_cpanel_error = 'HTTP Error: '.$response->getStatus().' '.$response->getMessage();
			return false;
		}
	}
	
	// Internal cPanel API
	public function requestCpanelApi($function, $args = array())
	{
		return $this->requestCpanelJsonApi($function, $args);
	}
	
	public function requestCpanelJsonApi($function, $request_params = array())
	{
		$request_uri = '/json-api/cpanel';

		$function_parts = array_slice(explode('::', $function), -2);
		$request_params['cpanel_jsonapi_module'] = $function_parts[0];
		$request_params['cpanel_jsonapi_func'] = $function_parts[1];
		$request_params['cpanel_jsonapi_apiversion'] = 2;
				
		$request_result = $this->request($request_uri, $request_params);
		
		if ($request_result)
		{
			try
			{
				$this->_cpanel_message = \Zend_Json::decode($request_result);
			}
			catch(\Exception $e)
			{
				throw new \DF\Exception\DisplayOnly('Could not parse JSON: '.$request_result);
			}
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	// Internal WHM API
	public function requestWhmApi($api_function, $args)
	{
		$request_uri = '/xml-api/'.$api_function;
		$request_params = $args;
		
		$request_result = $this->request($request_uri, $request_params);
		
		if ($request_result)
		{
			$xml_array = \DF\Export::XmlToArray($request_result);
			$this->_cpanel_message = $xml_array;
			
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function requestWhmJsonApi($api_function, $args = array())
	{
		$request_uri = '/json-api/'.$api_function;
		$request_params = $args;
		
		$request_result = $this->request($request_uri, $request_params);
		
		if ($request_result)
		{
			try
			{
				$this->_cpanel_message = \Zend_Json::decode($request_result);
			}
			catch(\Exception $e)
			{
				throw new \DF\Exception\DisplayOnly('Could not parse JSON: '.$request_result);
			}
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	/**
	 * SSH Functions
	 */
	
	// Check a user's directory to determine if it is empty.
	public function userDirectoryInfo($cpanel_user, $directory_name)
	{
		$files_to_ignore = array('.htaccess', 'cgi-bin', 'old', 'favicon.ico');
		
		$ssh = $this->getSSH();
		$folder_name = WebHostManager\SSH::getUserDirPath($cpanel_user, $directory_name);
				
		$directory_listing = $ssh->command('ls -A '.$folder_name);
		
		$return_data = array(
			'empty'	=> true,
			'num_files' => 0,
			'files' => array(),
		);
		
		if ($directory_listing)
		{
			$files_in_directory = explode("\n", $directory_listing);
			
			foreach($files_in_directory as $file_name)
			{
				if (!empty($file_name) && !in_array($file_name, $files_to_ignore))
				{
					$return_data['files'][] = $file_name;
					$return_data['num_files']++;
					$return_data['empty'] = false;
				}
			}
		}
		
		return $return_data;
	}
	
	// Copies a user's "www" directory to a "www/old" folder.
	public function userWebBackup($cpanel_user)
	{
		$ssh = $this->getSSH();
		$current_directory = WebHostManager\SSH::getUserDirPath($cpanel_user, self::USER_WEB_DIRECTORY);
		$backup_directory = WebHostManager\SSH::getUserDirPath($cpanel_user, self::USER_BACKUP_DIRECTORY);
		
		$ssh->command('mkdir '.$backup_directory);
		$ssh->command('cp -r '.$current_directory.'. '.$backup_directory);
		$ssh->command('rm -rf --preserve-root '.$current_directory.'*');
		$ssh->command('mkdir '.$current_directory.'old');
		$ssh->command('cp -r '.$backup_directory.'. '.$current_directory.'old');
		$ssh->command('chmod -R 777 '.$current_directory.'old');
		$ssh->command('rm -rf --preserve-root '.$backup_directory);

		// Zip the files, then move the zip file into the old folder.
		$ssh->command('zip -r '.$current_directory.'old_files.zip '.$current_directory.'old');
		$ssh->command('rm -rf --preserve-root '.$current_directory.'old');
		$ssh->command('mkdir '.$current_directory.'old');
		$ssh->command('mv '.$current_directory.'old_files.zip '.$current_directory.'old');
	}
	
	// Restores a user's "www" directory from files contained in the "www/old" folder.
	public function userWebBackupRestore($cpanel_user)
	{
		$ssh = $this->getSSH();
		$current_directory = WebHostManager\SSH::getUserDirPath($cpanel_user, self::USER_WEB_DIRECTORY);
		$backup_directory = WebHostManager\SSH::getUserDirPath($cpanel_user, self::USER_BACKUP_DIRECTORY);
		
		$ssh->command('mkdir '.$backup_directory);
		$ssh->command('cp -r '.$current_directory.'old/. '.$backup_directory);
		$ssh->command('rm -rf --preserve-root '.$current_directory.'*');
		$ssh->command('rm -f '.$current_directory.'.htaccess');
		$ssh->command('cp -r '.$backup_directory.'. '.$current_directory);
		$ssh->command('chmod -R 777 '.$current_directory);
		$ssh->command('rm -rf --preserve-root '.$backup_directory);
	}
	
	// Clears a user's "www" directory.
	public function userWebClear($cpanel_user)
	{
		$ssh = $this->getSSH();
		$current_directory = WebHostManager\SSH::getUserDirPath($cpanel_user, self::USER_WEB_DIRECTORY);
		
		$ssh->command('rm -rf --preserve-root '.$current_directory.'*');
		$ssh->command('rm -rf --preserve-root '.$current_directory.'.htaccess');
	}
	
	// Checks for illicit files in all user directories.
	public function userIllicitFilesCheck()
	{
		$scan_results = array();
		$total_search_criteria = count($GLOBALS['cpanel_illicit_search_list']);
		$current_search_criteria = 0;
	
		$ssh = $this->getSSH();
		$exempt_folders = \Zend_Registry::get('cpanel_illicit_scan_exempt', array());
		
		foreach($GLOBALS['cpanel_illicit_search_list'] as $search_criteria => $search_software)
		{
			if (isset($GLOBALS['progress']))
			{
				$current_search_criteria++;
				$current_percent = floor(($current_search_criteria / $total_search_criteria) * 100);
				$GLOBALS['progress']->update($current_percent, 'Special Synchronization: cPanel Illicit Files - Scanning for '.$search_software);
			}
			
			$search_results = $ssh->command('locate */public_html/*'.$search_criteria);
			
			if ($search_results)
			{
				$search_lines = explode("\n", $search_results);
				foreach($search_lines as $search_result)
				{
					$path_parts = explode("/", $search_result);
					
					if ($path_parts[1] == "home" && $path_parts[3] == "public_html" && !in_array($path_parts[2], $exempt_folders))
					{
						$scan_result = array();
						$scan_result['account'] = $path_parts[2];
						$scan_result['software'] = $search_software;
						$scan_result['path'] = trim($search_result);
						$scan_result['folder'] = str_replace($search_criteria, '', implode('/', array_slice($path_parts, 3)));
										
						$scan_results[] = $scan_result;
					}
				}
			}
		}
		
		\Zend_Registry::set('cpanel_illicit_scan_time', NCE_TIME);
		\Zend_Registry::set('cpanel_illicit_scan_results', $scan_results);
	}
	
	// Creates and manages a single SSH connection for all transactions.
	public function getSSH()
	{
		if (!$this->_ssh)
			$this->_ssh = new WebHostManager\SSH($this->_cpanel_server);
		
		return $this->_ssh;
	}
}