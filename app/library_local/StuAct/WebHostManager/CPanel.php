<?php
namespace StuAct\WebHostManager;

use \Entity\Organization;

class CPanel
{
	protected $_cpanel_server;

	protected $_cpanel_host;
	protected $_cpanel_accesshash;
	protected $_cpanel_theme;
	protected $_cpanel_error;
	
	protected $_cpanel_username;
	protected $_cpanel_password;
	protected $_cpanel_domain;
	
	protected $_whm;
	protected $_org;
	
	public function __construct($org)
	{
		Organization::loadSettings();
		
		if (!($org instanceof Organization))
			$org = Organization::find((int)$org);

		if (!($org instanceof Organization))
			throw new \DF\Exception\DisplayOnly('Organization not valid.');
		
		$this->_org = $org;
		
		$this->_cpanel_server = $org->it_web_type;
		$this->_cpanel_username = $org->it_web_username;
		$this->_cpanel_password = $org->it_web_password;
		$this->_cpanel_domain = $org->it_web_addr;
		
		$config = \Zend_Registry::get('config');
		$settings = $config->services->cpanel->toArray();
			
		$this->_cpanel_host = $settings['host'];
		$this->_cpanel_accesshash = $settings['accesshash'];
		$this->_cpanel_theme = $settings['cpanel_theme'];
		$this->_cpanel_error = false;
		
		$this->_whm = new \StuAct\WebHostManager();
	}
	
	public function getWhm()
	{
		return $this->_whm;
	}
	
	public function resetWebPassword($use_same_password = FALSE)
	{
		if ($this->_org->it_web_type && !$this->_org->it_web_suspended)
		{
			if ($this->checkStatus())
			{
				if ($use_same_password)
				{
					$old_password = $new_password = $this->_org->it_web_password;
				}
				else
				{
					$old_password = $this->_org->it_web_password;
					$new_password = \DF\Utilities::generatePassword();
		
					// Only update database if all previous updates went through.
					$this->_org->it_web_password = $new_password;
					$this->_org->it_web_password_change_timestamp = time();
				}
				
				// Update cPanel password remotely.
				$this->_whm->changePassword($this->_org->it_web_username, $new_password);
				
				// Set local variables.
				$this->_cpanel_username = $this->_org->it_web_username;
				$this->_cpanel_password = $new_password;
				
				// Synchronize all mailing list passwords (if the group has them).
				$this->syncMailingListPasswords();
				
				// Update Drupal password if needed.
				if ($this->_org->it_web_drupal == 1)
				{
					$drupal = new \StuAct\Drupal($this->_org);
					$drupal->synchronizePassword();
				}
				
				$this->_org->save();
			}
		}		
	}
	
	// Check the status of the remote server.
	public function checkStatus()
	{
		$cpanel_status = \Entity\Settings::getSetting('cpanel_status', 'online');
		$cache_timestamp = \Entity\Settings::getSetting('cpanel_status_timestamp', 0);
		
		if ($cache_timestamp < (time() - 600))
		{
			$status_result = $this->pingServer();
			$cpanel_status_new = ($status_result) ? "online" : "offline";
			
			$cpanel_status = $cpanel_status_new;
			\Entity\Settings::setSetting('cpanel_status', $cpanel_status);
			\Entity\Settings::setSetting('cpanel_status_timestamp', time());
		}
		
		return ($cpanel_status == "online");
	}
	
	public function pingServer()
	{
		$status_request = $this->_whm->requestWhmJsonApi('loadavg');
        
		if ($status_request)
		{
			$status_report = $this->_whm->getLastMessage();
			$five_minute_load = $status_report['five'];
			
			return ($five_minute_load < 15);
		}
		else
		{
			return FALSE;
		}
	}
	
	/**
	 * Master Account Functions
	 */
	 
	// Verify the username/password combination.
	public function checkLogin()
	{
		$client = new \Zend_Http_Client();
		$client->setConfig(array('timeout' => 60));
		
		$remote_uri = 'http://'.$this->_cpanel_host.':2082/frontend/'.$this->_cpanel_theme.'/index.html';
		$client->setUri($remote_uri);
		$client->setAuth($this->_cpanel_username, $this->_cpanel_password);
		$client->setParameterGet($request_params);
		
		try
		{
			$response = $client->request('GET');
			return (!$response->isError());
		}
		catch(\Exception $e)
		{
			return false;
		}
	}
	
	// Change the master cPanel account password.
	public function setPassword($password)
	{
		return $this->_whm->changePassword($this->_cpanel_username, $password);
	}

	// Retrieve contact e-mail address.
	public function getContactEmail()
	{
		$email = array();
		preg_match('/email" value="(.*)"/', $this->request('/contact/index.html'), $email);
		return $email[1];
	}

  	// Modify contact e-mail address.
	public function setContactEmail($email)
	{
		$response = $this->request('/contact/saveemail.html', array(
			'email'		=> $email,
		));
		
		if(strpos($response, 'has been'))
		{
			return true;
		}
		return false;
	}

  	// List all domains in the cPanel account.
	public function listDomains()
	{
		$domainList = array();
		preg_match_all('/<option value="([^"]*)/', $this->request('/mail/addpop2.html'), $domainList);
		if(count($domainList[1]) > 0)
		{
			return $domainList[1];
		}
		return false;
	}
	
	// List all full e-mail accounts.
	public function listMailAccounts()
	{
		$email_request = $this->_whm->requestCpanelApi('Email::listpopswithdisk', array('user' => $this->_cpanel_username));
			
		if ($email_request)
		{
			$email_data_raw = $this->_whm->getLastMessage();
			
			$email_accounts = array();
			foreach($email_data_raw['cpanelresult']['data'] as $email_address)
			{
				$email_accounts[] = $email_address['email'];
			}
			
			return $email_accounts;
		}
		else
		{
			return FALSE;
		}
	}
	
	// List MySQL database users.
	public function listDBUsers()
	{
		$accountList = array();
		preg_match_all('/\?user=([^"]*)/', $this->request('/sql/index.html'), $accountList);
		return $accountList[1];
	}
	// List MySQL databases.
	public function listDatabases()
	{
		$databaseList = array();
		preg_match_all('/deldb.html\?db=([^"]*)/', $this->request('/sql/index.html'), $databaseList);
		return $databaseList[1];
	}
	
	// List FTP accounts.
	public function listFTPAccounts()
	{
		$accountList = array();
		preg_match_all('/passwdftp.html\?acct=([^"]*)/', $this->request('/ftp/accounts.html'), $accountList);
		return array_unique($accountList[1]);
	}
	
	// List parked domains.
	public function listParked()
	{
		$domainList = array();
		preg_match_all('/<option value="([^"]*)/', $this->request('park/index.html'), $domainList);
		return $domainList[1];
	}
	
	// List add-on domains.
	public function listAddons()
	{
		$domainList = array();
		$data = explode('Remove Addon', $this->request('/addon/index.html'));
		preg_match_all('/<option value="(.*)">(.*)<\/option>/', $data[1], $domainList);
		return $domainList[0];
	}
	
	// List subdomains.
	public function listSubdomains()
	{
		$domainList = array();
		$domains = explode('</select>', $this->request('/subdomain/index.html'));
		$domains = explode('</select>', $domains[2]);
		preg_match_all('/<option value="(.*)">(.*)<\/option>/', $domains[0], $domainList);
		return $domainList[2];
	}
	
	// List Apache redirects.
	public function listRedirects()
	{
		$redirectList = array();
		preg_match_all('/<option value="\/([^"]*)/', $this->request('/mime/redirect.html'), $redirectList);
		return $redirectList[1];
	}
	
	/**
	 * E-mail Functions
	 */
	
	// Create new e-mail address.
	public function createEmail($address, $password)
	{
		$response = $this->request('/mail/doaddpop.html', array(
			'email'		=> $this->filterEmailAddress($address, FALSE),
			'domain'	=> $this->_cpanel_domain,
			'password'	=> $password,
			'password2'	=> $password,
			'quota'		=> '250',
		));
		
		return (strpos($response, 'success') !== FALSE);
	}
	
	// Change the password of an e-mail account.
	public function changeEmailPassword($address, $password)
	{
		$response = $this->request('/mail/dopasswdpop.html', array(
			'email'		=> $this->filterEmailAddress($address, FALSE),
			'domain'	=> $this->_cpanel_domain,
			'password'	=> $password,
			'password2'	=> $password,
		));
		
		return (strpos($response, 'success') !== FALSE && strpos($response, 'failure') === FALSE);
	}

	// Delete an e-mail account.
	public function deleteEmail($address)
	{
		$response = $this->request('/mail/dodelpop.html', array(
			'email'		=> $this->filterEmailAddress($address, FALSE),
			'domain'	=> $this->_cpanel_domain,
		));
		
		return (strpos($response, 'success') !== FALSE);
	}
	
	// List e-mail forwarders.
	public function listForwarders()
	{
		$email_request = $this->_whm->requestCpanelApi('Email::listforwards', array('user' => $this->_cpanel_username));
		
		if ($email_request)
		{
			$email_data_raw = $this->_whm->getLastMessage();
			
			$forwarder_accounts = array();
			foreach($email_data_raw['cpanelresult']['data'] as $email_address)
			{
				$forwarder_accounts[$email_address['dest']][] = $email_address['forward'];
			}
			
			return $forwarder_accounts;
		}
		else
		{
			return FALSE;
		}
	}
	
	// Create an e-mail forwarder.
	public function addForwarder($address, $forwards)
	{
		$forwards_array = explode("\n", $forwards);
		
		$response = TRUE;
		
		foreach($forwards_array as $forward)
		{
			if (!empty($forward))
			{
				$response = $this->request('/mail/doaddfwd.html', array(
					'email'		=> $this->filterEmailAddress($address, FALSE),
					'domain'	=> $this->_cpanel_domain,
					'fwdemail'	=> $forward,
					'fwdopt'	=> 'fwd',
				));
				
				$response = ($response) && (strpos($response, 'will now be copied') !== FALSE);
			}
		}
		
		return $response;
	}
	
	// Change the destination of an e-mail forwarder.
	public function changeForwarderDestination($address, $new_forward)
	{
		$delete_result = $this->deleteForwarder($address);
		$add_result = $this->addForwarder($address, $new_forward);
		
		return ($delete_result && $add_result);
	}
		
	// Delete an e-mail forwarder.
	public function deleteForwarder($address)
	{
		$forwarders = $this->listForwarders();
		
		$address = $this->filterEmailAddress($address, TRUE);
		
		if (isset($forwarders[$address]))
		{
			$forwards = $forwarders[$address];
			
			foreach($forwards as $forward)
			{
				$response = $this->request('/mail/dodelfwd.html', array(
					'email'		=> $address,
					'emaildest'	=> $forward,
				));
			}
		}
		return true;
	}
	
	/** 
	 * Mailing List Functions
	 */
	
	// List mailing lists.
	public function listMailingLists()
	{
		$email_request = $this->_whm->requestCpanelApi('Email::listlists', array('user' => $this->_cpanel_username));
		
		if ($email_request)
		{
			$email_data_raw = $this->_whm->getLastMessage();
			
			$mailing_lists = array();
			foreach($email_data_raw['cpanelresult']['data'] as $mailing_list)
			{
				$mailing_lists[] = $mailing_list['list'];
			}
			
			return $mailing_lists;
		}
		else
		{
			return FALSE;
		}
	}
	
	// Create a new mailing list.
	public function addMailingList($address)
	{
		$response = $this->request('/mail/doaddlist.html', array(
			'email'		=> $this->filterEmailAddress($address, FALSE),
			'password'	=> $this->_cpanel_password,
			'password2'	=> $this->_cpanel_password,
			'domain'	=> $this->_cpanel_domain,
		));
		
		if (stristr($response, 'A forwarder already exists') !== FALSE)
		{
			$this->_cpanel_error = 'A listserv with this name already exists.';
			return false;
		}
		else if (stristr($response, 'maximum allowed mailing lists') !== FALSE)
		{
			$this->_cpanel_error = 'You cannot create any more listservs.';
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// Synchronize all mailing list passwords to the new master password.
	public function syncMailingListPasswords()
	{
		$mailing_lists = $this->listMailingLists();
		
		if ($mailing_lists)
		{
			foreach($mailing_lists as $list_address)
			{
				$this->changeMailingListPassword($list_address, $this->_cpanel_password);
			}
		}
		
		return true;
	}
	
	// Change a mailing list's password.
	public function changeMailingListPassword($address, $new_password)
	{
		$response = $this->request('/mail/dopasswdlist.html', array(
			'email'		=> $this->filterEmailAddress($address, TRUE),
			'password'	=> $new_password,
			'password2'	=> $new_password,
		));
		
		return ($this->_cpanel_error);
	}
	
	// Delete an existing mailing list.
	public function deleteMailingList($address)
	{
		$response = $this->request('/mail/realdodellist.html', array(
			'email'		=> $this->filterEmailAddress($address, TRUE),
		));
		
		return true;
	}
	
	/**
	 * Statistics
	 */
	
	public function getStatistics($month = NULL)
	{}
	
	/**
	 * Apache optimization
	 */
	public function enableCompression()
	{
		$optimized_types = array(
			'text/html',
			'text/plain',
			'text/xml',
			'text/css',
			'application/xhtml+xml',
			'application/xml',
			'application/rss+xml',
			'application/atom_xml',
			'application/javascript',
			'application/x-javascript',
			'application/x-httpd-php',
			'application/x-httpd-fastphp',
			'application/x-httpd-eruby',
		);
		
		$response = $this->request('/optimize/dooptimize.html', array(
			'deflate'		=> 'list',
			'deflate-mime-list'	=> implode(' ', $optimized_types),
		));

		return ($this->_cpanel_error);
	}
	
	public function disableCompression()
	{
		$response = $this->request('/optimize/dooptimize.html', array(
			'deflate'		=> 'disabled',
			'deflate-mime-list'	=> '',
		));
		return ($this->_cpanel_error);
	}
	
	/**
	 * Utility Functions
	 */

	// Retrieve the last error message.
	public function getLastError()
	{
		return $this->_cpanel_error;
	}
	
	// Format an e-mail address according to cPanel standards.
	public function filterEmailAddress($address, $include_domain = TRUE)
	{
		if (strpos($address, '@') !== FALSE)
		{
			$address = substr($address, 0, strpos($address, '@'));
		}
		
		$address = preg_replace("/[^a-zA-Z0-9-_]/", "", $address);
		
		return ($include_domain) ? $address.'@'.$this->_cpanel_domain : $address;
	}

	// Internal request processing function.
	protected function request($request_uri, $request_params = array())
	{
		$this->_cpanel_error = FALSE;
		
		$client = new \Zend_Http_Client();
		$client->setConfig(array('timeout' => 60));
		
		if (substr($request_uri, 0, 3) == "tmp")
			$client->setUri('http://'.$this->_cpanel_host.':2082/'.$request_uri);
		else
			$client->setUri('http://'.$this->_cpanel_host.':2082/frontend/'.$this->_cpanel_theme.$request_uri);
		
		$client->setAuth($this->_cpanel_username, $this->_cpanel_password);
		$client->setParameterGet($request_params);
		
		try
		{
			$response = $client->request('GET');
		}
		catch(\Exception $e)
		{
			// Ignore "invalid chunk size" messages.
			if (stristr($e->getMessage(), 'chunk size') === FALSE)
				throw new \DF\Exception\DisplayOnly('cPanel Server Unavailable: The cPanel server is currently unavailable. Please try to access this feature again later.');
			else
				return FALSE;
		}
		
		if ($response->getStatus() == 200)
		{
			return $response->getBody();
		}
		else
		{	
			$this->_cpanel_error = 'HTTP Error: '.$response->getStatus();
			return false;
		}
	}	
}