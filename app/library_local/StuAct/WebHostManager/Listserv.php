<?
namespace StuAct\WebHostManager;

use \Entity\Organization;

// Directory on remote server where the mailman management binaries are stored.
define("MAILMAN_BINARY_DIRECTORY", "/usr/local/cpanel/3rdparty/mailman/bin");

// Listserv member status codes.
define("NCE_LISTSERV_MEMBER_DISABLED", 1);
define("NCE_LISTSERV_MEMBER_RECV_ONLY", 3);
define("NCE_LISTSERV_MEMBER_SEND_ONLY", 4);
define("NCE_LISTSERV_MEMBER_SEND_RECV", 10);

class Listserv
{
	const PRIV_DISABLED = 1;
	const PRIV_RECV_ONLY = 3;
	const PRIV_SEND_ONLY = 4;
	const PRIV_SEND_RECV = 10;

	private $_server_index;
	
	private $_ssh;
	private $_email_validator;
	
	private $_org_username;
	private $_org_password;
	private $_org_domain;
	
	public function __construct($org_info)
	{
		Organization::loadSettings();
		
		// Accept either an existing array of organization information or an Org ID.
		if ($org_info instanceof Organization)
		{
			$org_info = $org_info->toArray(TRUE, TRUE);
		}
		else if (is_int($org_info))
		{
			$org = \Entity\Organization::find($org_info);
			$org_info = $org->toArray(TRUE, TRUE);
		}
		
		if ($org_info['it_web_type'] != 0)
		{
			$this->_server_index = $org_info['it_web_type'];
			$this->_org_username = $org_info['it_web_username'];
			$this->_org_password = $org_info['it_web_password'];
			$this->_org_domain = $org_info['it_web_addr'];
		}
		else
		{
			throw new \DF\Exception\DisplayOnly('Organization Not Hosted on cPanel: Listserv support is only available for organizations hosted on cPanel.');
			exit;
		}
	}
	
	// List all listservs currently associated with an organization.
	public function listAll()
	{
		$ssh = $this->getSSH();
		
		$raw_result = $ssh->command(MAILMAN_BINARY_DIRECTORY.'/list_lists -V '.$this->_org_domain);
		$raw_lines = explode("\n", $raw_result);
		
		if (stristr($raw_lines[0], "No matching mailing lists found") !== FALSE)
		{
			return array();
		}
		else
		{
			unset($raw_lines[0]);
			
			$listservs = array();
			
			foreach($raw_lines as $raw_line)
			{
				if (stristr($raw_line, '-') !== FALSE)
				{
					$listserv_name = strtolower(trim(substr($raw_line, 0, strpos($raw_line, '-'))));
					$listservs[] = array(
						'name'		=> $listserv_name,
						'address'	=> $listserv_name.'@'.$this->_org_domain,
					);
				}
			}
			
			return $listservs;
		}
	}
		
	// Retrieve member information for all current members of a given list.
	public function listMembers($list_address)
	{
		$ssh = $this->getSSH();
				
		$raw_result = $ssh->command(MAILMAN_BINARY_DIRECTORY.'/withlist -r nce.info '.$this->getListName($list_address));
		$raw_lines = explode("\n", $raw_result);
		
		$members = array();
		$permission_types = self::getPermissionTypes();
		
		foreach($raw_lines as $raw_line)
		{
			if (stristr($raw_line, '|') !== FALSE)
			{
				$line_parts = explode('|', $raw_line);
				
				$address = trim($line_parts[0]);
				$is_moderated = ($line_parts[1] == 'Y') ? TRUE : FALSE;
				$is_nomail = ($line_parts[2] == 'Y') ? TRUE : FALSE;
				
				if (!$is_moderated && !$is_nomail)
				{
					$status = NCE_LISTSERV_MEMBER_SEND_RECV;
				}
				else if ($is_moderated && !$is_nomail)
				{
					$status = NCE_LISTSERV_MEMBER_RECV_ONLY;
				}
				else if (!$is_moderated && $is_nomail)
				{
					$status = NCE_LISTSERV_MEMBER_SEND_ONLY;
				}
				else
				{
					$status = NCE_LISTSERV_MEMBER_DISABLED;
				}
				
				$members[] = array(
					'address'		=> $address,
					'status'		=> $status,
					'status_name'	=> $permission_types[$status],
				);
			}
		}
		
		usort($members, array($this, 'sortMembers'));
		
		return $members;
	}
	
	// Add a member to a listserv, and optionally set the moderation flag for the newly added member.
	public function addMember($list_address, $member, $status = NCE_LISTSERV_MEMBER_RECV_ONLY, $override_status = TRUE)
	{
		return $this->addMembers($list_address, array($member), $status, $override_status);
	}
	
	// Add multiple members to the listserv.
	public function addMembers($list_address, $members, $status = NCE_LISTSERV_MEMBER_RECV_ONLY, $override_status = TRUE)
	{
		// Compose a list of all existing users in the listserv; speeds up performance by preventing duplicate additions.
		$existing_members = $this->listMembers($list_address);
		$existing_addresses = array();
		$existing_status = array();
		
		if ($existing_members)
		{
			foreach($existing_members as $existing_member)
			{
				$existing_addresses[] = $existing_member['address'];
				$existing_status[$existing_member['address']] = $existing_member['status'];
			}
		}
		
		$addresses_to_add = array();
		$addresses_to_set_status = array();
		
		foreach($members as $member)
		{
			$email = trim($member);
			$email = str_replace('mailto:', '', $email);
			
			if ($this->checkValidEmail($email))
			{
				if (!in_array($email, $existing_addresses) && !in_array($email, $addresses_to_add))
				{
					$addresses_to_add[] = $email;
				}
				
				if (!isset($existing_status[$email]) || ($override_status && $existing_status[$email] != $status))
				{
					$addresses_to_set_status[] = $email;
				}
			}
		}
		
		// Connect to server to add in bulk any addresses that should be added.
		if ($addresses_to_add)
		{
			set_time_limit(300);

			$ssh = $this->getSSH();
			
			$temp_file = $ssh->generateRandomFilename();
			$address_list = implode('\\n', $addresses_to_add);

			// Copy the e-mail address to a temporary file, use the file in the command below, then remove the file.
			$ssh->command("echo -e '".$address_list."' > ".$temp_file);
			$add_results = $ssh->command(MAILMAN_BINARY_DIRECTORY.'/add_members -r '.$temp_file.' -w n -a n '.$this->getListName($list_address));
			$ssh->command("rm -f ".$temp_file);
		}
		
		// Individually set the moderation flags for users as needed.
		if ($addresses_to_set_status)
		{
			$this->setStatus($list_address, $addresses_to_set_status, $status);
		}
		
		return count($addresses_to_add);
	}
	
	// Remove a single member from a listserv.
	public function removeMember($list_address, $member)
	{
		return $this->removeMembers($list_address, array($member));
	}
	
	// Clear the current list of members on the listserv. Can optionally specify a status to only remove all members with this status.
	public function removeAllMembers($list_address, $status = NULL)
	{
		$current_membership = $this->listMembers($list_address);
		
		if (!empty($current_membership))
		{
			$members_to_delete = array();
			
			foreach($current_membership as $member)
			{
				if (is_null($status) || ($member['status'] == $status))
				{
					$members_to_delete[] = $member['address'];
				}
			}
			
			$this->removeMembers($list_address, $members_to_delete);
		}
	}

	// Remove an array of members from a listserv.
	public function removeMembers($list_address, $member_list)
	{
		if (!is_array($member_list))
		{
			$member_list = array($member_list);
		}
		
		if (!empty($member_list))
		{
			$addresses_to_remove = array();
			
			foreach($member_list as $email)
			{
				$email = trim($email);
				
				if ($this->checkValidEmail($email))
				{
					$addresses_to_remove[] = $email;
				}
			}
			
			if ($addresses_to_remove)
			{
				$ssh = $this->getSSH();
				$temp_file = $ssh->generateRandomFilename();
				$address_list = implode('\\n', $addresses_to_remove);
	
				// Copy the e-mail address to a temporary file, use the file in the command below, then remove the file.
				$ssh->command("echo -e '".$address_list."' > ".$temp_file);
				$remove_results = $ssh->command(MAILMAN_BINARY_DIRECTORY.'/remove_members -f '.$temp_file.' -n -N '.$this->getListName($list_address));
				$ssh->command("rm -f ".$temp_file);
			}
		}
			
		return true;
	}
	
	// Set the user status of a listserv member or array of members.
	public function setStatus($list_address, $member_list, $status)
	{
		switch($status)
		{
			case NCE_LISTSERV_MEMBER_SEND_RECV:
				$moderated_status = FALSE;
				$nomail_status = FALSE;
				break;
				
			case NCE_LISTSERV_MEMBER_SEND_ONLY:
				$moderated_status = FALSE;
				$nomail_status = TRUE;
				break;
			
			case NCE_LISTSERV_MEMBER_RECV_ONLY:
				$moderated_status = TRUE;
				$nomail_status = FALSE;
				break;
			
			case NCE_LISTSERV_MEMBER_DISABLED:
			default:
				$moderated_status = TRUE;
				$nomail_status = TRUE;
				break;
		}

		$this->setProperty($list_address, $member_list, 'mod', $moderated_status);
		$this->setProperty($list_address, $member_list, 'nomail', $nomail_status);
	}
	
	// Set the user status of all users.
	public function setStatusAll($list_address, $status)
	{
		$current_membership = $this->listMembers($list_address);
		
		if (!empty($current_membership))
		{
			$members_to_modify = array();
			
			foreach($current_membership as $member)
			{
				$members_to_modify[] = $member['address'];
			}
			
			$this->setStatus($list_address, $members_to_modify, $status);
		}
	}

	// Set a given property for one or more members of a mailing list.
	public function setProperty($list_address, $member_list, $property_name, $property_value = TRUE)
	{
		if (!is_array($member_list))
		{
			$member_list = array($member_list);
		}
		
		foreach($member_list as $member_key => $member_addr)
		{
			if (!$this->checkValidEmail($member_addr))
			{
				unset($member_list[$member_key]);
			}
		}
		
		$value_string = ($property_value) ? '1' : '0';
		
		$ssh = $this->getSSH();
		$mod_results = $ssh->command(MAILMAN_BINARY_DIRECTORY.'/withlist -r nce.set '.$this->getListName($list_address).' '.implode(',', $member_list).' '.$property_name.' '.$value_string);
	}
	
	// Set a given property for all members of a mailing list.
	public function setPropertyAll($list_address, $property_name, $property_value = TRUE)
	{
		$value_string = ($property_value) ? '1' : '0';
		
		$ssh = $this->getSSH();
		$ssh->command(MAILMAN_BINARY_DIRECTORY.'/withlist -r nce.setall '.$this->getListName($list_address).' '.$property_name.' '.$value_string);
	}
		
	/**
	 * Internal private functions.
	 */
	
	// Maintain one SSH connection for all transactions.
	private function getSSH()
	{
		if (!$this->_ssh)
		{
			$this->_ssh = new SSH($this->_server_index);
		}
		
		return $this->_ssh;
	}
	private function getEmailValidator()
	{
		if (!$this->_email_validator)
		{
			$this->_email_validator = new \Zend_Validate_EmailAddress();
		}
		
		return $this->_email_validator;
	}
	
	// Check for valid e-mail address.
	public function checkValidEmail($email_address)
	{
		$validator = $this->getEmailValidator();
		return $validator->isValid($email_address);
	}

	// Reformat a listserv's address for use with shell functions.
	public function getListName($list_address)
	{
		return $this->formatListAddress($list_address).'_'.$this->_org_domain;
	}
	
	// Format list addresses.
	private function formatListAddress($list_address)
	{
		if (stristr($list_address, '@') !== FALSE)
		{
			$list_address = substr($list_address, 0, strpos($list_address, '@'));
		}
		
		return preg_replace("/[^a-zA-Z0-9-_]/", "", $list_address);
	}
	
	// Sort member results.
	private function sortMembers($a, $b)
	{
		if ($a['status'] == $b['status'])
		{
			return strcmp(strtolower($a['address']), strtolower($b['address']));
		}
		else if ($a['status'] < $b['status'])
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	/**
	 * Static Functions
	 */

	public static function getPermissionTypes()
	{
		return array(
			NCE_LISTSERV_MEMBER_DISABLED		=> 'Disabled',
			NCE_LISTSERV_MEMBER_RECV_ONLY		=> 'Receive Only',
			NCE_LISTSERV_MEMBER_SEND_ONLY		=> 'Send Only',
			NCE_LISTSERV_MEMBER_SEND_RECV		=> 'Send/Receive',
		);
	}
}