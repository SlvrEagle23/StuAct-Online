<?
namespace StuAct\Organization;

class OrganizationAbstract
{
	protected $_org;
	protected $_org_id;
	protected $_settings;

	public function __construct(\Entity\Organization $org)
	{
		$this->_org = $org;
		$this->_org_id = $org->id;

		$this->_settings = \Entity\Organization::loadSettings();
	}

	public static function factory(\Entity\Organization $org)
	{
		return new static($org);
	}
}