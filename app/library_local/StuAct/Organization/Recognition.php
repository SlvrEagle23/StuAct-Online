<?php
namespace StuAct\Organization;

use \Entity\Organization;

class Recognition extends OrganizationAbstract
{
	// Automated synchronization of organization's recognition status.
	public function synchronize()
	{
		// Exempt and unrecognized organizations are not synchronized.
		$no_recognition_statuses = array(ORG_STATUS_EXEMPT, ORG_STATUS_NOT_RECOGNIZED);
		if (in_array($this->_org->status, $no_recognition_statuses))
		{
			$this->_org->date_last_synchronized = time();
			return true;
		}

		set_time_limit(0);

		// Evaluate all flags for the organization.
		$this->evaluateFlags();
		$this->evaluateStatus();

		$settings = $this->_settings;

		// Recognition constants.
		$org_status = (int)$this->_org->status;
		$date_status_changed = (int)$this->_org->date_status_changed;
		$date_last_synchronized = (int)$this->_org->date_last_synchronized;
		$date_last_notified = (int)$this->_org->date_last_notified;
		
		// Retrieve timestamp for beginning of cycle change in current year.
		$org_cycle_month = (int)$this->_org->cycle;
		$start_of_cycle = $this->getCycleChange();
		
		$settings['start_of_cycle'] = $start_of_cycle;
		
		// Compose various timestamps for use below.			
		$first_notification = $start_of_cycle - $this->_settings['prerec_first_notification'];
		$second_notification = $start_of_cycle - $this->_settings['prerec_second_notification'];

		$time_until_status_change = $this->getNextStatusChange();
		$time_until_status_notification = $time_until_status_change - $this->_settings['status_change_notification'];

		$settings['time_until_status_change'] = $time_until_status_change;

		// Steps only take place if organization is "Recognized"
		if ($org_status == ORG_STATUS_RECOGNIZED)
		{
			// First notification prior to cycle change
			if ($date_last_notified < $first_notification && time() > $first_notification && time() < $start_of_cycle)
			{
				$this->_org->notify('cycle_change_approaching', $settings);
				$this->_org->date_last_notified = time();
			}
			// Second notification prior to cycle change
			else if ($date_last_notified < $second_notification && time() > $second_notification && time() < $start_of_cycle)
			{
				$this->_org->notify('cycle_change_approaching', $settings);
				$this->_org->date_last_notified = time();
			}
			// Cycle Change ("Recognized" -> "In Transition")
			else if ($date_status_changed < $start_of_cycle && time() > $start_of_cycle)
			{
				// Switch status to "In Transition"
				$this->_org->status = ORG_STATUS_IN_TRANSITION;
				$this->_org->status_desc = 'Completing annual recognition process.';

				// Notify organization.
				$additional_info = $this->_settings;
				$additional_info['time_of_restriction'] = time() + $this->_settings['time_until_restricted'];
				$this->_org->notify('cycle_change', $additional_info);
				
				// Notify officers that they need to renew their positions.
				$student_leaders = $this->_org->getStudentLeaders();
				foreach((array)$student_leaders as $leader)
				{
					if (!$leader->isRenewed())
						$this->_org->notify('member_renew', array('position' => $leader));
				}

				// Cancel staff override, reset many flags.
				$this->_org->cycle_override = 0;
				$this->_org->setFlag('leaders_present', ORG_FLAG_DOES_NOT_MEET_REQ);
				$this->_org->setFlag('leaders_trained', ORG_FLAG_DOES_NOT_MEET_REQ);
				$this->_org->setFlag('leaders_renewed', ORG_FLAG_DOES_NOT_MEET_REQ);
				$this->_org->setFlag('officers_eligible', ORG_FLAG_DOES_NOT_MEET_REQ);
				$this->_org->setFlag('advisors_present', ORG_FLAG_DOES_NOT_MEET_REQ);
				$this->_org->setFlag('advisors_trained', ORG_FLAG_DOES_NOT_MEET_REQ);
				$this->_org->setFlag('updates', ORG_FLAG_DOES_NOT_MEET_REQ);
				
				// Only reset constitution flag if it wasn't recently submitted.
				if ($this->_org->flag_constitution_timestamp < $first_notification)
				{
					$this->_org->flag_constitution = ORG_FLAG_DOES_NOT_MEET_REQ;
					$this->_org->flag_constitution_timestamp = 0;
				}

				// Notify Risk Management if the group's enhanced expectations renew annually.
				if ($this->_org->miscellaneous_requirements)
				{
					$has_enhanced_requirement = FALSE;
					foreach($this->_org->miscellaneous_requirements as $requirement)
					{
						if ($requirement->renews_annually)
							$requirement->status = ORG_FLAG_MEETS_REQ;

						if (substr($requirement->type, 0, 8) == "enhanced")
							$has_enhanced_requirement = TRUE;
					}

					if ($has_enhanced_requirement)
						$this->_org->notify('enhanced_annual');
				}
			}
		}
		
		// The following steps are prevented by a staff override.
		if ($this->_org->cycle_override == 1)
			return true;

		if ($org_status == ORG_STATUS_IN_TRANSITION || $org_status == ORG_STATUS_IN_PROGRESS)
		{
			// Auto-Restrict warning
			if (time() < $time_until_status_change && time() > $time_until_status_notification && $date_last_notified < $time_until_status_notification)
			{
				$additional_info = $this->_settings;
				$additional_info['unmet_flags'] = $this->getUnmetFlags();
				$additional_info['time_of_restriction'] = time() + $this->_settings['status_change_notification'];

				$this->_org->notify('autorestrict_warning', $additional_info);
				
				// Notify staff members if the restriction involves a manual staff process.
				if (isset($additional_info['unmet_flags']['sigcard']))
				{
					$this->_org->notify('status_warning_sofc', $additional_info);
				}
				/*
				if (isset($additional_info['unmet_flags']['constitution']) && $this->_org->flag_constitution == ORG_FLAG_SACT_ACTION_REQ)
				{
					$this->_org->notify('status_warning_rec', $additional_info);
				}
				*/

				$this->_org->date_last_notified = time();
			}
			// Auto-Restrict organization
			else if (time() > $time_until_status_change)
			{
				$additional_info = $this->_settings;
				$additional_info['unmet_flags'] = $this->getUnmetFlags();
				$additional_info['time_of_suspension'] = time()+$this->_settings['time_until_suspended'];

				$this->_org->status = ORG_STATUS_RESTRICTED;
				$this->_org->status_desc = 'Organization passed recognition deadline and was restricted automatically.';

				// Delete old organization officers.
				$student_leaders = $this->_org->getStudentLeaders();
				foreach((array)$student_leaders as $leader)
				{
					if (!$leader->isRenewed())
						$leader->delete();
				}
				
				// Notify organization.
				$this->_org->notify('autorestrict', $additional_info);
			}
		}
		
		if ($org_status == ORG_STATUS_RESTRICTED)
		{
			// Auto-Suspend warning
			if (time() < $time_until_status_change && time() > $time_until_status_notification && $date_last_notified < $time_until_status_notification)
			{
				$additional_info = $settings;
				$additional_info['unmet_flags'] = $this->getUnmetFlags();
				$additional_info['time_of_unrecognition'] = $time_until_status_change;
				
				$this->_org->notify('autounrecognize_warning', $additional_info);
				$this->_org->date_last_notified = time();
			}
			// Auto-Suspend organization
			else if (time() > $time_until_status_change)
			{
				$this->_org->status = ORG_STATUS_NOT_RECOGNIZED;
				$this->_org->status_desc = 'Organization passed renewal deadline and is no longer recognized.';

				// Notify organization.
				$this->_org->notify('autounrecognize', $settings);
			}
		}
		
		if ($org_status == ORG_STATUS_SUSPENDED)
		{
			$this->_org->status = ORG_STATUS_RESTRICTED;
			$this->_org->status_desc = 'Organization automatically migrated to Recognized with Restrictions.';
		}

		$this->_org->date_last_synchronized = time();
		$this->_org->sofc_send_email = 0;
	}

	public function evaluateFlags()
	{
		foreach($this->_settings['flags'] as $flag_name => $flag_text)
		{
			$flag_func_name = '_evaluate'.str_replace(' ', '', ucwords(str_replace('_', ' ', $flag_name))).'Flag';
			call_user_func(array($this, $flag_func_name));
		}
	}
	
	// Evaluate recognition requirements for the organization and change the group's status if necessary.
	public function evaluateStatus()
	{
		// Skip this step for exempt groups.
		if ($this->_org->status == ORG_STATUS_EXEMPT || $this->_org->cycle_override == 1)
			return true;

		$org_status = $this->_org->status;
		$autoupdate_allowed = array(ORG_STATUS_IN_PROGRESS, ORG_STATUS_IN_TRANSITION, ORG_STATUS_RESTRICTED, ORG_STATUS_SUSPENDED);
		
		$unmet_requirements = $this->getUnmetFlags();
		$meets_all_requirements = (empty($unmet_requirements));

		if (($meets_all_requirements) && in_array($org_status, $autoupdate_allowed))
		{
			$this->_org->status = ORG_STATUS_RECOGNIZED;
			$this->_org->status_desc = 'Recognized by StuAct Online; all requirements for recognition met.';
		}
		else if ((!$meets_all_requirements) && ($org_status == ORG_STATUS_RECOGNIZED))
		{
			// Make sure organization is not currently in its "early bird recognition" phase.
			if (!$this->isEarlyBird())
			{
				$this->_org->status = ORG_STATUS_RESTRICTED;
				$this->_org->status_desc = 'Restricted by StuAct Online; one or more recognition requirements unmet.';
				$this->_org->notify('restrict_flags', array('unmet_flags' => $unmet_requirements));
			}
		}
	}

	public function isEarlyBird()
	{
		if ($this->_org->status != ORG_STATUS_RECOGNIZED)
			return FALSE;

		$first_notification = $this->getCycleChange() - $this->_settings['prerec_first_notification'];
		return (time() >= $first_notification);
	}
	
	// Generates an array containing any unmet recognition requirements.
	public function getUnmetFlags()
	{
		$unmet_flags = array();
		foreach($this->_settings['flags'] as $required_flag => $flag_name)
		{
			$flag_is_met = TRUE;
			$flag_value = $this->_org->getFlag($required_flag);
			
			// An in-review constitution cannot affect recognized status if the group is already fully recognized.
			if ($required_flag == "constitution" && $this->_org->status == ORG_STATUS_RECOGNIZED)
			{
				if ($flag_value == ORG_FLAG_DOES_NOT_MEET_REQ)
					$flag_is_met = FALSE;
			}
			else if ($flag_value != ORG_FLAG_MEETS_REQ)
			{
				$flag_is_met = FALSE;
			}
			
			if (!$flag_is_met)
				$unmet_flags[$required_flag] = $flag_name;
		}
		
		return $unmet_flags;
	}

	/**
	 * Advisor Flags
	 */
	
	protected function _evaluateAdvisorsPresentFlag()
	{
		$present = count($this->_org->getOfficersByGroup('sigcard_advisor_req'));
		$needed = count($this->_settings['officer_groups']['sigcard_advisor_req']);

		$flag = ($present >= $needed) ? ORG_FLAG_MEETS_REQ : ORG_FLAG_DOES_NOT_MEET_REQ;
		$this->_org->flag_advisors_present = $flag;
	}

	protected function _evaluateAdvisorsTrainedFlag()
	{
		$advisors = $this->_org->getAdvisors();

		if (count($advisors) == 0)
		{
			$this->_org->flag_advisors_trained = ORG_FLAG_DOES_NOT_MEET_REQ;
			return;
		}

		$meets_flag = TRUE;
		foreach($advisors as $position)
		{
			if ($position->user->flag_training == 0)
				$meets_flag = FALSE;
		}

		$flag = ($meets_flag) ? ORG_FLAG_MEETS_REQ : ORG_FLAG_DOES_NOT_MEET_REQ;
		$this->_org->flag_advisors_trained = $flag;
	}

	/**
	 * Student Leader Flags
	 */

	protected function _evaluateLeadersPresentFlag()
	{
		$present = count($this->_org->getOfficersByGroup('sigcard_officer_req'));
		$needed = count($this->_settings['officer_groups']['sigcard_officer_req']);

		$flag = ($present >= $needed) ? ORG_FLAG_MEETS_REQ : ORG_FLAG_DOES_NOT_MEET_REQ;
		$this->_org->flag_leaders_present = $flag;
	}

	protected function _evaluateLeadersTrainedFlag()
	{
		$csl = $this->_org->getOfficerByPosition(ORG_OFFICER_CHIEF_STUDENT_LEADER);
		$treasurer = $this->_org->getOfficerByPosition(ORG_OFFICER_TREASURER);

		if (!$csl || !$treasurer)
		{
			$this->_org->flag_leaders_trained = ORG_FLAG_DOES_NOT_MEET_REQ;
			return;
		}

		$csl_trained = ($csl->user->flag_training != 0);
		$treasurer_trained = ($treasurer->user->flag_training != 0);

		$flag = ($csl_trained && $treasurer_trained) ? ORG_FLAG_MEETS_REQ : ORG_FLAG_DOES_NOT_MEET_REQ;
		$this->_org->flag_leaders_trained = $flag;
	}

	protected function _evaluateLeadersRenewedFlag()
	{
		$officers = $this->_org->getOfficersByGroup('sigcard_req');

		if (count($officers) == 0)
		{
			$this->_org->flag_leaders_renewed = ORG_FLAG_DOES_NOT_MEET_REQ;
			return;
		}

		$unrenewed_officers = array();
		foreach($officers as $position)
		{
			if (!$position->isRenewed())
				$unrenewed_officers[] = $position->name;
		}

		$flag = (count($unrenewed_officers) == 0) ? ORG_FLAG_MEETS_REQ : ORG_FLAG_ORG_ACTION_REQ;
		$this->_org->flag_leaders_renewed = $flag;
	}

	/**
	 * All Officer Flags
	 */
	
	protected function _evaluateOfficersEligibleFlag()
	{
		$officers = $this->_org->getOfficersByGroup('gpr');

		if (count($officers) == 0)
		{
			$this->_org->flag_officers_eligible = ORG_FLAG_DOES_NOT_MEET_REQ;
			return;
		}

		$ineligible_officers = array();
		foreach($officers as $position)
		{
			if (!$position->isEligible())
				$ineligible_officers[$position->position] = $position->name;
		}

		if (count($ineligible_officers) == 0)
		{
			$this->_org->setFlag('officers_eligible', ORG_FLAG_MEETS_REQ);
		}
		else
		{
			$current_flag = $this->_org->getFlag('officers_eligible');

			if ($current_flag != ORG_FLAG_ORG_ACTION_REQ)
			{
				$this->_org->setFlag('officers_eligible', ORG_FLAG_ORG_ACTION_REQ);
				$this->_org->notify('ineligible_officer', array('ineligible_officers' => $ineligible_officers));

				foreach($ineligible_officers as $off_position => $off_name)
					$this->_org->notify('ineligible_individual', array('position' => $off_position));
			}
		}
	}

	/**
	 * Organization-Wide Flags
	 */
	
	// Profile Update recognition flag
	protected function _evaluateUpdatesFlag()
	{
		// Get time of expiration and the minimum time when a notification should be sent.
		$expiration_time = $this->_org->date_last_updated + $this->_settings['lifespan_profile'];
		$notify_time = $expiration_time - $this->_settings['status_change_notification'];
		
		// Determine if a notification was already sent out within this cycle.
		$notified_recently = ($this->_org->flag_updates_notification > (time() - $this->_settings['status_change_notification']));
		
		if ($expiration_time >= time())
		{
			if ($notify_time < time() && !$notified_recently)
			{
				// Notify the organization that their profile must be updated if applicable.
				$this->_org->notify('profile_needs_update', array('time_until_expiration' => $expiration_time - time()));
				$this->_org->flag_updates_notification = time();
			}
			
			$this->_org->setFlag('updates', ORG_FLAG_MEETS_REQ);
		}
		else
		{
			$this->_org->setFlag('updates', ORG_FLAG_DOES_NOT_MEET_REQ);
		}
	}
	
	// SOFC Signature Card Recognition Flag
	protected function _evaluateSigcardFlag()
	{
		// Restore requirement if current officer signature card arrangement exactly matches the most recently approved one.
		$current_arrangement = (array)$this->_org->getPositionsOnSigcard();
		$approved_arrangement = (array)$this->_org->flag_sigcard_approved_positions;

		foreach((array)$approved_arrangement as $position => $user_id)
		{
			$is_req = isset($this->_settings['officer_groups']['sigcard_req'][$position]);
			$is_approved = isset($approved_arrangement[$position]);

			if ($is_req && !$is_approved)
				$approved_arrangements[$position] = $user_id;
		}

		ksort($approved_arrangement);
		ksort($current_arrangement);

		$meets_req = (!empty($current_arrangement) && !empty($approved_arrangement) && $current_arrangement == $approved_arrangement);
		$this->_org->setFlag('sigcard', ($meets_req) ? ORG_FLAG_MEETS_REQ : ORG_FLAG_DOES_NOT_MEET_REQ);
	}
	
	// Constitution Recognition Flag
	protected function _evaluateConstitutionFlag()
	{
		$threshold_time = $this->_org->flag_constitution_timestamp + $this->_settings['lifespan_constitution'];
		
		if (time() > $threshold_time)
			$this->_org->setFlag('constitution', ORG_FLAG_DOES_NOT_MEET_REQ);
	}
	
	// Miscellaneous Recognition Flag
	protected function _evaluateMiscFlag()
	{
		$meets_requirement = TRUE;

		if ($this->_org->miscellaneous_requirements)
		{
			foreach($this->_org->miscellaneous_requirements as $requirement)
			{
				if ($requirement->status != ORG_FLAG_MEETS_REQ)
					$meets_requirement = FALSE;
			}
		}

		$flag = ($meets_requirement) ? ORG_FLAG_MEETS_REQ : ORG_FLAG_DOES_NOT_MEET_REQ;
		$this->_org->setFlag('misc', $flag);
	}

	// Internal script to determine cycle change date.
    public function getCycleChange()
    {
        $org_cycle_month = (int)$this->_org->cycle;
        
        $current_year_cycle_change = mktime(0, 0, 0, $org_cycle_month, 15, date('Y'));
        $next_year_cycle_change = mktime(0, 0, 0, $org_cycle_month, 15, date('Y')+1);
        
        return ($this->_org->date_status_changed < $current_year_cycle_change) ? $current_year_cycle_change : $next_year_cycle_change;
    }

    public function getNextStatusChange()
    {
        $settings = Organization::loadSettings();
        $date_status_changed = (int)$this->_org->date_status_changed;

        switch($this->_org->status)
        {
            case ORG_STATUS_RECOGNIZED:
                return $this->getCycleChange();
            break;
            
            case ORG_STATUS_IN_TRANSITION:
            case ORG_STATUS_IN_PROGRESS:
                // Adjust time for extended recognition cycles.         
                if ($this->_org->status == ORG_STATUS_IN_TRANSITION && isset($this->_settings['extended_cycles'][$this->_org->cycle]))
                    $date_status_changed += $this->_settings['extended_cycles'][$this->_org->cycle];
                
                return $date_status_changed + $this->_settings['time_until_restricted'];
            break;
            
            case ORG_STATUS_RESTRICTED:
                return $date_status_changed + $this->_settings['time_until_suspended'];
            break;
            
            case ORG_STATUS_SUSPENDED:
                return $date_status_changed + $this->_settings['time_until_unrecognized'];
            break;
        }
    }
}