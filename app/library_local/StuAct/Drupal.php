<?php
namespace StuAct;

use \Entity\Organization;

class Drupal
{
	// Force all installations to be upgraded even if the version number is the same.
	const FORCE_UPGRADE = FALSE;
	
	protected $_drupal_config;
	protected $_org;
	protected $_org_info;
	
	/**
	 * Standard constructor/get/set functions.
	 */
	
	public function __construct($org)
	{		
		// Localize configuration variables.
		$this->_drupal_config = self::loadSettings();
		
		// Pull organization information.
		$this->setOrg($org);
		
		// Get current Drupal version.
		self::getCurrentVersion();
	}
	
	public function getOrg()
	{
		return $this->_org;
	}
	public function getOrgInfo()
	{
		return $this->_org_info;
	}
	
	public function setOrg($org)
	{
		if (!($org instanceof Organization))
			$org = Organization::find((int)$org);
		
		if (!($org instanceof Organization))
			throw new \DF\Exception\DisplayOnly('Org not found in system.');
		
		$this->_org = $org;
		$this->_org_info = $org->toArray();
	}
	public function reloadOrgInfo()
	{
		$this->_org_info = $this->_org->toArray();
	}
	
	/** 
	 * Automated Installation
	 */
	 
	public function install($cli_mode = FALSE, $complete_reinstall = FALSE, $page = 1)
	{
		// Extend the script's running time and suspend the active session to handle the heavy load.
		\DF\Session::suspend();
		set_time_limit(300);
		
		$org_id = $this->_org_info['org_id'];
		$source_dir = $this->_drupal_config['source_dir'];
		$target_dir = $this->_drupal_config['target_dir'];
		
		$ajax_mode = false;
		
		if ($complete_reinstall)
		{
			$is_overwrite = ($this->_org_info['it_web_drupal'] == 1);
			$this->_org_info['it_web_drupal'] = 0;
		}
		
		if ($this->_org_info['it_web_type'] != 10)
			throw new \DF\Exception\DisplayOnly('Organization Not Hosted on cPanel: The organization you specified is not currently hosted on our cPanel server. They must be relocated before setting up Drupal.');
		
		$account_username = $this->_org_info['it_web_username'];
		$account_password = $this->_org_info['it_web_password'];
		$account_sitename = $this->_org_info['org_name'];
		
		// Back up old files if necessary.
		if (isset($_REQUEST['backup']) && $_REQUEST['backup'] == 1)
		{
			$whm = new WebHostManager();
			$whm->userWebBackup($account_username);
		}
		
		// Establish basic account information.	
		$account_theme = $GLOBALS['drupal_config']['theme_default'];
		
		// Set theme and login information in the database.
		$updated_org_info = array(
			'it_web_drupal'				=> $this->_org_info['it_web_drupal'],
		);
		$this->_org->setInfo($updated_org_info);
		$this->reloadOrgInfo();
			
		$is_fresh_install = ($this->_org_info['it_web_drupal'] != 1 || $complete_reinstall);
		$is_upgrade = (
			self::FORCE_UPGRADE
			|| (defined('NCE_DRUPAL_FORCE_UPGRADE') && NCE_DRUPAL_FORCE_UPGRADE)
			|| (floatval(NCE_DRUPAL_VERSION) > floatval($this->_org_info['it_web_drupal_version']))
		);
		$is_update = (!$is_fresh_install && !$is_upgrade);
		
		if ($is_fresh_install)
			$config_mode = 'install';
		else if ($is_upgrade)
			$config_mode = 'upgrade';
		else
			$config_mode = 'update';
		
		// Halt installation if update cannot be completed via AJAX.
		if (($ajax_mode || $cli_mode) && ($is_upgrade || $is_fresh_install))
			throw new \DF\Exception\DisplayOnly('Org uses a previous version of Drupal, and must be updated manually.');
		
		// Halt installation if update isn't necessary.
		if (($ajax_mode) && NCE_DRUPAL_VERSION == $this->_org_info['it_web_drupal_version'])
			throw new \DF\Exception\DisplayOnly('Org already up-to-date.');
				
		$remote_site = $this->_drupal_config['remote_site_base'].$this->_org_info['it_web_username'];

		// Upload and extract files in remote directory.
		if ($page == 1)
		{
			// Copy the Drupal installation files to the remote server and extract them.
			$this->pushFiles();
			
			// Run the management script to load the remote configuration files.
			$this->reloadRemoteConfig($config_mode);
			
			// Push images to server if each respective directory is empty.
			$this->resetHeaderImages(FALSE);
			
			if($is_fresh_install)
			{
				if ($is_overwrite)
				{
					$this->runRemoteAction('db_backup');
				}
				
				// Establish MySQL connection with remote site.
				$remote_db = $this->getDatabase();
				$new_db_name = $account_username.'_drupal';
				
				// Create database and assign apropriate permissions.
				$remote_db->query("DROP DATABASE IF EXISTS ".$new_db_name);
				$remote_db->query("CREATE DATABASE IF NOT EXISTS ".$new_db_name);	
				$remote_db->query("GRANT ALL ON ".$remote_db->quoteIdentifier($new_db_name).".* TO '".$account_username."'@'localhost'");		
				$remote_db->query("SET PASSWORD FOR '".$account_username."'@'localhost' = PASSWORD('".$account_password."')");
				
				// Run installation script on remote site.
				return array(
					'action' 	=> 'iframe',
					'url'		=> $remote_site.'/install.php?profile=default&locale=en',
				);
			}
			else if ($is_upgrade)
			{
				$this->checkModuleUpdates();
				// $this->runRemoteAction('db_backup');
			
				$remote_query_string = array(
					'op'		=> 'Update',
					'token'		=> 'STUACT',
				);
				
				return array(
					'action' 	=> 'iframe',
					'url'		=> $remote_site.'/update.php?'.http_build_query($remote_query_string),
				);
			}
		}
	
		if ($page == 2)
		{
			// Establish MySQL connection to remote server for following operations.
			$remote_db = $this->getDatabase();
			$new_db_name = $account_username.'_drupal';

			if ($is_fresh_install)
			{		
				// Insert data for site administrator into newly created tables.
				$new_user_data = array(
					'name'		=> $account_username,
					'pass'		=> md5($account_password),
					'mail'		=> $account_username.'@'.$this->_org_info['it_web_addr'],
					'created'	=> time(),
					'access'	=> time(),
					'status'	=> 1,
				);
				$remote_db->update($new_db_name.'.users', $new_user_data, 'uid = 1');
				
				// Add the "Home" link to the header menu.
				$new_menu_data = array(
					'menu_name'		=> 'primary-links',
					'plid'			=> 0,
					'link_path'		=> '<front>',
					'router_path'	=> '',
					'link_title'	=> 'Home',
					'options'		=> serialize(array('attributes' => array('title' => ''))),
					'module'		=> 'menu',
					'hidden'		=> 0,
					'external'		=> 1,
					'weight'		=> 0-10,
					'depth'			=> 1,
					'customized'	=> 1,
					'p1'			=> 106,
				);
				$remote_db->insert($new_db_name.'.menu_links', $new_menu_data);
								
				// Assign upload viewing permissions to anonymous users.
				$new_permission_data = array(
					'perm'		=> 'access content, view uploaded files'
				);
				$remote_db->update($new_db_name.'.permission', $new_permission_data, "rid = 1");
				
				// Assign upload viewing permissions to authenticated users.
				$new_permission_data = array(
					'perm'		=> 'access comments, post comments, post comments without approval, access content, view uploaded files, access tinymce'
				);
				$remote_db->update($new_db_name.'.permission', $new_permission_data, "rid = 2");
				
				// Add new node types
				$new_node_data = array(
					'type'			=> 'page',
					'name'			=> 'Page',
					'module'		=> 'node',
					'description'	=> 'Pages provide general information about the organization and are updated less often than Stories. Examples include "About Us", "History", "Contact Us", etc.',
					'help'			=> '<div class="messages"><b>To add a page to the site\'s header menu:</b><br /><ul><li>Complete the "Title" and "Body" sections of the form below.</li><li>Click the "Menu Settings" link, which will open a new panel.</li><li>For the "Title" field in the menu settings panel, use the same text that you used at the top of this page.</li><li>For the "Parent Item" field in the menu settings panel, choose "Primary Links".</li><li>You can optionally provide a "Weight", which determines how far to the right or left on the menu the page will appear. If all pages are left with a weight of 0, they will be in alphabetic order.</li></ul></div>',
					'has_title'		=> 1,
					'title_label'	=> 'Title',
					'has_body'		=> 1,
					'body_label'	=> 'Body',
					'min_word_count' => 0,
					'custom'		=> 1,
					'modified'		=> 1,
					'locked'		=> 0,
				);
				$remote_db->insert($new_db_name.'.node_type', $new_node_data);
				
				$new_node_data = array(
					'type'			=> 'story',
					'name'			=> 'Story',
					'module'		=> 'node',
					'description'	=> 'Stories are posted directly to the organization\'s homepage and are useful for time-sensitive information, such as an application being available, interviews being held, upcoming meeting times, etc.',
					'help'			=> '',
					'has_title'		=> 1,
					'title_label'	=> 'Title',
					'has_body'		=> 1,
					'body_label'	=> 'Body',
					'min_word_count' => 0,
					'custom'		=> 1,
					'modified'		=> 1,
					'locked'		=> 0,
				);
				$remote_db->insert($new_db_name.'.node_type', $new_node_data);
			}
			
			if ($is_upgrade || $is_fresh_install)
			{
				// Add the right-side navigation bar to the tamu-modern theme.
				$new_block_data = array(
					'module'	=> 'user',
					'delta'		=> 1,
					'theme'		=> 'tamumodern',
					'status'	=> 1,
					'weight'	=> 0,
					'region'	=> 'right',
					'custom'	=> 0,
					'throttle'	=> 0,
					'visibility' => 0,
				);
				$remote_db->delete($new_db_name.'.blocks', 'module = '.$remote_db->quote('user').' AND theme = '.$remote_db->quote('tamumodern'));
				$remote_db->insert($new_db_name.'.blocks', $new_block_data);
			}
		}
	
		if ($page == 2 || $is_update)
		{
			// Reload the remote configuration again (Drupal's installer messes stuff up)
			$this->reloadRemoteConfig($config_mode);
			
			// Install additional modules (upload, tinymce, etc.)
			$module_update = $this->runRemoteAction('update_modules');
			
			if (!$module_update['successful'])
				throw new \DF\Exception\DisplayOnly('The module update script could not be called for '.$this->_org_info['it_web_username'].': The message returned was '.$module_update['message']);
			
			// Synchronize header images if needed.
			$this->runRemoteAction('sync_header_images');
	
			$updated_org_info = array(
				'it_web_drupal'				=> 1,
				'it_web_drupal_pending'		=> 0,
				'it_web_drupal_version'		=> NCE_DRUPAL_VERSION,
				'it_web_drupal_installdate'	=> time(),
			);
			$this->_org->fromArray($updated_org_info);
			$this->_org->save();
			
			if ($is_fresh_install)
			{
				$this->_org->notify('it_drupal');
			}
			
			return array(
				'action' => 'success',
			);
		}
	}
	
	// Pushes the installation files to the remote server.
	public function pushFiles()
	{
		$account_username = $this->_org_info['it_web_username'];
		$remote_site = $this->_drupal_config['remote_site_base'].$account_username;
		$source_dir = $this->_drupal_config['source_dir'];
		$target_dir = $this->_drupal_config['target_dir'];
		
		$drupal_file_base = 'DrupalInstall_'.NCE_DRUPAL_VERSION.'.zip';
		$zip_file_base = 'drupal-installer/'.$drupal_file_base;
		
		$zip_file_name = \DF\File::getFilePath($zip_file_base);
		$zip_file_url = \DF\File::getFileUrl($zip_file_base);
		
		// Zip files if needed.
		set_time_limit(300);
		if (!file_exists($zip_file_name))
		{
			$zip = new \DF\Zip($zip_file_name);
			$zip->addDirectory($source_dir, '', array('.hg'));
			$zip->save();
		}
		
		// Copy Files to CPanel Server
		$whm = new WebHostManager($this->_org_info['it_web_type']);
		$ssh = $whm->getSSH();
		
		$user_web_directory = WebHostManager::USER_WEB_DIRECTORY;
		
		$zip_destination = $ssh->getUserDirPath($account_username, WebHostManager::USER_WEB_DIRECTORY);
		$www_directory = $ssh->getUserDirPath($account_username, WebHostManager::USER_WWW_DIRECTORY);
		$temp_dir = $ssh->getUserDirPath($account_username, 'www_temp');
		
		// Pull zip file to remote server.
		$source_modified_time = filemtime($zip_file_name);
		$destination_filename = '~/drupal/DrupalInstall_'.NCE_DRUPAL_VERSION.'_'.date('Ymd-his', $source_modified_time).'.zip';
		
		$file_exists = trim($ssh->command('[ -f '.$destination_filename.' ] && echo "YES" || echo "NO"'));
		if (strtoupper($file_exists) == "NO")
			$ssh->command('wget '.$zip_file_url.' -O '.$destination_filename);
		
		// Copy all folders that contain keepable user-generated content
		$ssh->command('mkdir '.$temp_dir);
		
		$folders_to_keep = array(
			'/sites/default',
			'/header-images-tamumodern',
			'/files',
		);
		foreach($folders_to_keep as $folder)
		{
			$ssh->command('mkdir -p '.$temp_dir.$folder);
			$ssh->command('cp -R -f '.$zip_destination.$folder.'/* '.$temp_dir.$folder);
		}
		
		// Clear out the entire www directory.
		$ssh->command('rm -rf --preserve-root '.$zip_destination.'/*');
		$ssh->command('rm -rf --preserve-root '.$zip_destination.'/.hg');
		$ssh->command('rm -f --preserve-root '.$zip_destination.'/.htaccess');
		
		$ssh->command('cp -R -f '.$temp_dir.'/* '.$zip_destination);
		$ssh->command('rm -rf --preserve-root '.$temp_dir);
		
		// Unzip files.
		$ssh->command('unzip -o '.$destination_filename.' -d '.$zip_destination);
		
		// Assign the correct permissions to the new files.
		$ssh->command('chown -RH nobody:nobody '.$zip_destination);
		
		$ssh->command('chmod -R 755 '.$zip_destination);
		
		$system_folders = array('/includes', '/modules', '/misc', '/scripts', '/profiles', '/management', '/sites');
		foreach($system_folders as $system_folder)
			$ssh->command('chmod -R 555 '.$zip_destination.$system_folder);
		
		$ssh->command('chmod 555 '.$zip_destination);
		$ssh->command('chmod 555 '.$www_directory);
		$ssh->command('chmod 555 '.$zip_destination.'/index.php');
		
		$ssh->command('chmod 755 '.$zip_destination.'/sites/default');
		$ssh->command('chmod 555 '.$zip_destination.'/sites/default/settings.php');
		
		$ssh->command('chmod -R 744 '.$zip_destination.'/files');
		$ssh->command('chmod -R 744 '.$zip_destination.'/sites/default/files');
		
		// Slight delay here to let the file permission cache update from the changes above.
		// (Yes, it sounds silly, but this makes a world of difference in preventing failed installations.)
		sleep(3);
	}
		
	// Calls a function on the remote Drupal site to perform maintenance or retrieve information.
	public function runRemoteAction($action_name, $remote_vars = array())
	{
		$remote_site = $this->_drupal_config['remote_site_base'].$this->_org_info['it_web_username'];
		$management_url = $remote_site.'/sact_management.php';

		$remote_vars = serialize($remote_vars);
		
		$access_salt = sha1(time());	
		$params = array(
			'access_salt'	=> $access_salt,
			'access_result'	=> hash_hmac('sha1', $access_salt, 'SACT1928'),
			'action'		=> $action_name,
			'remote_vars'	=> base64_encode($remote_vars),
		);

		$client = new \Zend_Http_Client($management_url);
		$client->setConfig(array(
			'timeout'      	=> 60,
			'maxredirects'	=> 0,
		));
		$client->setHeaders(array(
			'User-Agent'	=> 'StuActOnline (Googlebot Compatible)',
		));
		$client->setParameterPost($params);
		
		try
		{
			$response = $client->request('POST');
		}
		catch(\Zend_Http_Client_Exception $e)
		{
			return array(
				'successful'		=> false,
				'message'			=> 'Could not submit client request. Exception: '.$e->getMessage(),
			);
		}
		
		if ($response->isSuccessful())
		{
			$response_body = $response->getBody();
			
			// Responses sent from the remote site are base64 encoded for reliable transport.
			$response_processed = unserialize(base64_decode($response_body));
			
			if ($response_processed['status'] == "OK")
			{			
				return array(
					'successful'		=> true,
					'message'			=> '',
					'response'			=> $response_processed,
				);
			}
			else
			{
				$message = 'The remote site did not return an OK status code. ';
				$message .= 'Raw response: '.\DF\Utilities::print_r($response_body, TRUE);
				$message .= 'Processed response: '.\DF\Utilities::print_r($response_processed, TRUE);
				
				return array(
					'successful'		=> false,
					'message'			=> $message,
					'response'			=> $response_processed,
				);
			}
		}
		else
		{
			return array(
				'successful'		=> false,
				'message'			=> 'HTTP response received was not HTTP 200 OK.<br>URL: '.$management_url.'<br>Response: '.$response->getStatus().'<br>Headers:'.\DF\Utilities::print_r($response->getHeaders(), TRUE),
			);
		}
	}
	
	/**
	 * Installation Verification
	 */
	 
	public function checkInstallation()
	{}
	 
	// Verify that the remote site's hash for certain files is the same as the one distributed to them.
	public function checkFileHash()
	{}
	
	/**
	 * Reload the remote configuration file with organization data.
	 * @param $mode The settings to use (update, upgrade, or install)
	 */
	public function reloadRemoteConfig($mode = 'update')
	{
		$this->reloadOrgInfo();
		
		// Modify flatfile configuration.
		$replacements = array(
			'{NCE_SKIN_NAME}'		=> $theme_name,
			'{NCE_DB_USER}'			=> $this->_org_info['it_web_username'],
			'{NCE_DB_PASS}'			=> $this->_org_info['it_web_password'],
			'{NCE_SITE_NAME}'		=> $this->_org_info['name'],
			'{NCE_SITE_DOMAIN}'		=> $this->_org_info['it_web_addr'],
			'{NCE_UPDATE_MODE}'		=> ($mode == 'upgrade' || $mode == 'install') ? time() : 0,
		);
		$this->runRemoteAction('write_config', $replacements);
		
		// Modify variables in database.
		$variables = array(
			'#force'		=> ($mode == 'install'),
			'site_name'		=> $this->_org_info['name'],
			'theme_default' => 'tamumodern',
			'sao_theme_scheme' => 'classic-light',
			'sao_theme_color' => '#500000',
			'sao_theme_background_color' => '#500000',
			'sao_header_images' => 'dynamic',
		);
		
		$this->runRemoteAction('write_variables', $variables);
	}
	
	// Run cron job on remote site.
	public function runCron()
	{
		return $this->runRemoteAction('cron');
	}
	
	// Validate an autologin ticket.
	public function validateAutologinTicket($ticket_id = '')
	{
		global $db;
		
		// Remove old tickets.
		$db->delete('drupal_login_tickets', 'ticket_timestamp <= '.(time() - 300));
		
		$ticket_row = $db->fetchRow('SELECT * FROM drupal_login_tickets AS dlt WHERE dlt.ticket_id = ? AND dlt.ticket_org_id = ? AND dlt.ticket_timestamp > ?', array($ticket_id, $this->_org_info['org_id'], time() - 300));
		
		if ($ticket_row)
		{
			$return_data = array(
				'status'		=> 'OK',
				'name'			=> $this->_org_info['it_web_username'],
				'pass'			=> $this->_org_info['it_web_password'],
			);
		}
		else
		{
			$return_data = array(
				'status'		=> 'ERROR',
			);
		}
		
		return base64_encode(serialize($return_data));
	}
	
	/**
	 * Uninstallation
	 */
	 
	public function uninstall()
	{
		$account_username = $this->_org_info['it_web_username'];
		$account_type = $this->_org_info['it_web_type'];
		
		// Check for files stored in old folder and restore them if necessary.
		$whm = new WebHostManager($account_type);
		$old_files = $whm->userDirectoryInfo($account_username, WebHostManager::USER_OLD_DIRECTORY);
		
		if (!$old_files['empty'])
		{
			$whm->userWebBackupRestore($account_username);
		}
		else
		{
			$whm->userWebClear($account_username);
		}
		
		// Update the database to prevent future updates.
		$updated_org_info = array(
			'it_web_drupal'				=> 0,
			'it_web_drupal_pending'		=> 0,
			'it_web_drupal_version'		=> 0,
			'it_web_drupal_installdate'	=> 0,
		);
		$this->_org->fromArray($updated_org_info);
		$this->_org->save();
		
		// Notify organization leadership.
		$this->_org->notify('it_drupal_uninstalled');
		
		return true;
	}
	
	/**
	 * Password Synchronization
	 */
	 
	// Synchronize the password used in the Drupal system with the one associated with the organization's profile.
	public function synchronizePassword()
	{
		// Push new configuration.
		$this->reloadRemoteConfig();

		// Update MySQL password.
		$remote_db = $this->getDatabase();
		
		try
		{
			$remote_db->query("SET PASSWORD FOR '".$this->_org_info['it_web_username']."'@'localhost' = PASSWORD('".$this->_org_info['it_web_password']."')");
			$remote_db->query("SET PASSWORD FOR '".$this->_org_info['it_web_username']."'@'%' = PASSWORD('".$this->_org_info['it_web_password']."')");
		}
		catch(\Exception $e)
		{}
		
		// Force root password update on remote site.
		$this->runRemoteAction('update_modules');
	}
	
	/**
	 * Username Synchronization
	 */
	
	// Synchronize the username used in the Drupal system and the MySQL DB.
	public function synchronizeUsername($old_username = '')
	{
		$new_username = $this->_org_info['it_web_username'];
		$new_db_name = $new_username.'_drupal';
		
		// Push new configuration.
		$this->reloadRemoteConfig();
		$remote_db = $this->getDatabase();
		
		if ($old_username)
		{
			$old_db_name = $old_username.'_drupal';

			try
			{
				// Delete old user.
				$remote_db->query("DROP USER '".$old_username."'@'localhost'");
				$remote_db->query("DROP USER '".$old_username."'@'%'");
				
				// Rename the database.
				// $remote_db->query("RENAME DATABASE ".$remote_db->quoteIdentifier($old_db_name)." TO ".$remote_db->quoteIdentifier($new_db_name));
			}
			catch (\Exception $e) { }
		}
		
		try
		{
			// Create new database.
			// $remote_db->query("CREATE DATABASE IF NOT EXISTS ".$remote_db->quoteIdentifier($new_db_name));
			
			// Create new user.
			$remote_db->query("CREATE USER '".$new_username."'@'localhost' IDENTIFIED BY PASSWORD '".$this->_org_info['it_web_password']."'");
			
			// Assign privileges on database to new user.
			// $remote_db->query("GRANT ALL PRIVILEGES ON ".$remote_db->quoteIdentifier($new_db_name.'.*')." TO '".$new_username."'@'localhost'");
			
		}
		catch(\Exception $e) { }
	}

	// Resets the header images stored on the remote server for the current user.
	public function resetHeaderImages($delete_current_images = TRUE)
	{ }
	
	// Check for module updates.
	public function checkModuleUpdates()
	{
		global $template;
		
		$updates_installed = FALSE;
		
		$modules = $this->runRemoteAction('modules');
		$system_modules = self::getDefaultModules();
		
		if ($modules['response']['status'] == "OK")
		{
			foreach($modules['response']['modules'] as $module)
			{
				if (!isset($system_modules[$module['name']]))
				{
					$update = self::isModuleUpdated($module['name'], $module['version']);
										
					if (!$update['is_latest'])
						return $this->_installFromUrl($update_info['latest_version_url'], 'module');
				}
			}
		}
		
		return $updates_installed;
	}

	public function installProject($project_name)
	{
		$project_name = str_replace(array('http://drupal.org/project/', ' '), array('', ''), strtolower($project_name));
		$update_info = self::isModuleUpdated($project_name);

		if ($update_info['latest_version_url'])
			return $this->_installFromUrl($update_info['latest_version_url'], $update_info['type']);
		else
			return false;
	}

	protected function _installFromUrl($url, $type = 'module')
	{
		$account_username = $this->_org_info['it_web_username'];
						
		// Copy Files to CPanel Server
		$whm = new WebHostManager($this->_org_info['it_web_type']);
		$ssh = $whm->getSSH();
		
		$user_web_directory = $ssh->getUserDirPath($account_username, WebHostManager::USER_WEB_DIRECTORY);

		if ($type == "module")
			$zip_destination = $user_web_directory.'/sites/default/modules';
		elseif ($type == "theme")
			$zip_destination = $user_web_directory.'/sites/default/themes';
		
		try
		{
			// Pull zip file to remote server.
			$destination_filename = $ssh->generateRandomFilename().'.tar.gz';
			$ssh->command('wget '.$url.' -O '.$destination_filename);
					
			// Unzip files.
			$ssh->command('tar -zxvf '.$destination_filename.' -C '.$zip_destination);
			
			// Remove temporary zip file.
			$ssh->command('rm -f '.$destination_filename);
			
			// Assign the correct permissions to the new files.
			$ssh->command('chown -RH '.$account_username.':'.$account_username.' '.$zip_destination);
			$ssh->command('chmod -R 775 '.$zip_destination);
		}
		catch(\Exception $e)
		{
			return false;
		}

		return true;
	}
	
	// Retrieve an instance of the MySQL connection to the remote server.
	protected $_db;
	public function getDatabase()
	{
		if (!$this->_db)
		{
			$mysql_config = $this->_drupal_config['mysql'];
			$this->_db = new \Zend_Db_Adapter_Pdo_Mysql($mysql_config);
		}
		
		return $this->_db;
	}
	
	/**
	 * Static Functions
	 */
	
	// Retrieve all modules associated with the core installation (and their current versions).
	public static function getDefaultModules()
	{
		global $drupal_config;
		
		// Pull from the modules folder for the default install.
		$info_files = glob($drupal_config['source_dir'].'/sites/all/modules/*/*.info');
		$projects = array();
		foreach($info_files as $info_file)
		{
			$config = new \Zend_Config_Ini($info_file);
			
			if (!empty($config->project))
			{
				$project_name = $config->project;
				$current_version = self::getModuleVersionNumber($config->version);
				
				$projects[$project_name] = $current_version;
			}
		}
		
		// Add the actual core Drupal project.
		$current_drupal_version = self::getCurrentVersion();
		$projects['drupal'] = floatval($current_drupal_version);
		
		return $projects;
	}
	 
	// Check for outdated modules in the default software load.
	public static function checkDefaultModules()
	{
		$projects = self::getDefaultModules();
		
		// Loop through all projects and check for updates.
		foreach($projects as $project_name => $current_version)
		{
			$update = self::isModuleUpdated($project_name, $current_version);
			
			if (!$update['is_latest'])
			{
				// Attempt to automatically extract the file to the correct spot.
				if (class_exists('Archive_Tar') && $project_name != "drupal")
				{
					$archive = new \Archive_Tar($update['latest_version_url']);
					$archive->extract($drupal_config['source_dir'].'/sites/all/modules');
				}
			}
		}
	}
	
	protected static function isModuleUpdated($project_name, $current_version = '0.0')
	{
		list($current_version_major, $current_version_minor) = explode('.', $current_version);
		
		$updates_url = 'http://updates.drupal.org/release-history/'.urlencode($project_name).'/6.x';

		try
		{
			$updates_obj = new \Zend_Config_Xml($updates_url);
			$updates = $updates_obj->toArray();
		}
		catch(\Exception $e)
		{
			throw new \DF\Exception\DisplayOnly('Module not found! A module with the name you specified could not be found.');
		}
		
		if ($updates)
		{
			foreach($updates['terms']['term'] as $term)
			{
				if ($term['name'] == "Projects")
					$module_type = ($term['value'] == "Themes") ? 'theme' : 'module';
			}

			$latest_version = 0;
			$latest_version_major = 0;
			$latest_version_minor = 0;
			$latest_version_url = NULL;
			
			foreach($updates['releases']['release'] as $release)
			{	
				if (isset($release['version_major']) && $release['version_major'] == $updates['recommended_major'])
				{
					if (($latest_version_major <= intval($release['version_major']) || $latest_version_minor <= intval($release['version_patch'])) && substr($release['download_link'], -2) == "gz")
					{
						$latest_version = floatval($release['version_major'].'.'.$release['version_patch']);
						$latest_version_major = (int)$release['version_major'];
						$latest_version_minor = (int)$release['version_patch'];
						$latest_version_url = $release['download_link'];
					}
				}
			}
			
			if ($current_version_major < $latest_version_major || $current_version_minor < $latest_version_minor)
			{
				return array(
					'is_latest'			=> FALSE,
					'type'				=> $module_type,
					'updates'			=> $updates,
					'latest_version' 	=> $latest_version,
					'latest_version_url' => $latest_version_url,
				);
			}
		}
		
		return array(
			'is_latest'			=> TRUE,
		);
	}
	
	// Extract the current version number from a string in the form of "6.x-1.3-blah".
	protected static function getModuleVersionNumber($version_string)
	{
		$version_strip = explode('-', $version_string);
		
		if (count($version_strip) > 1)
			return floatval($version_strip[1]);
		else
			return floatval($version_string);
	}
	
	public static function getCurrentVersion()
	{
		$settings = self::loadSettings();
		$source_dir = $settings['source_dir'];
		
		$version_file = $source_dir.'/sact_version.php';
		
		if (file_exists($version_file))
		{
			include_once($version_file);
			return NCE_DRUPAL_VERSION;
		}
		else
		{
			return FALSE;
		}
	}
	
	public static function loadSettings()
	{
		static $settings;
		
		if (!$settings)
		{
			$config = \Zend_Registry::get('config');
			$settings = $config->services->drupal->toArray();
		}
		
		return $settings;
	}
}