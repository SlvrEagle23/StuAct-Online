<?php
/**
 * BitBucket API Connector
 */

namespace DF\Service;

class BitBucket
{
	// Shortcuts for various HTTP functions.
	public static function get($resource, $params = array())
	{
		return self::call($resource, $params, 'GET');
	}
	public static function post($resource, $params = array())
	{
		return self::call($resource, $params, 'POST');
	}
	public static function put($resource, $params = array())
	{
		return self::call($resource, $params, 'PUT');
	}
	public static function delete($resource, $params = array())
	{
		return self::call($resource, $params, 'DELETE');
	}
	
	// Main requesting process.
	public static function call($resource, $params = array(), $method = 'GET')
	{
		$settings = self::getSettings();
		
		$client = new \Zend_Http_Client;
		
		$uri = 'https://api.bitbucket.org/1.0/repositories/'.$settings['host_username'].'/'.$settings['application_slug'].'/'.$resource;
		$client->setUri($uri);
		$client->setAuth($settings['username'], $settings['password']);
		$client->setParameterGet(array(
			'accountname' => $settings['host_username'],
			'repo_slug' => $settings['application_slug'],
		));
		$client->setConfig(array(
			'maxredirects' => 0,
			'timeout'      => 30,
		));
		
		if ($method == 'GET')
			$client->setParameterGet($params);
		else
			$client->setParameterPost($params);
		
		$response = $client->request($method);
		
		if ($response->isSuccessful())
		{
			$body = $response->getBody();
			return json_decode($body, TRUE);
		}
	}
	
	public static function getSettings()
	{
		static $settings;
		if (!$settings)
		{
			$config = \Zend_Registry::get('config');
			$settings = $config->services->bitbucket->toArray();
		}
		return $settings;
	}
}