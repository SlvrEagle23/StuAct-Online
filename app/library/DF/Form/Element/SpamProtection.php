<?php
namespace DF\Form\Element;

class SpamProtection extends \Zend_Form_Element_Captcha
{
	const PUBLIC_KEY = '6LfE7eASAAAAADg6R11mHJaFdiGKj_KNB55kB-A4';
	const PRIVATE_KEY = '6LfE7eASAAAAAIH3Wn8LUhEUihib4uO2qDxg64n7';

	public function __construct($spec, $options = NULL)
	{
		$options = array_merge((array)$options, array(
			'captcha' => 'ReCaptcha',
			'captchaOptions' => array(
				'captcha' => 'ReCaptcha',
            	'service' => new \Zend_Service_ReCaptcha(self::PUBLIC_KEY, self::PRIVATE_KEY),
			),
			'label' => 'Spam Protection',
			'description' => 'In order to prevent spam, please enter the text shown here.',
			'helper' => 'formLabel',
		));

		parent::__construct($spec, $options);
	}
}