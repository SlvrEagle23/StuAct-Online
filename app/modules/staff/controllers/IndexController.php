<?php
/**
 * Staff portal
 */

use \Entity\StaffDirectoryUser;

class Staff_IndexController extends \DF\Controller\Action
{
	public function permissions()
	{
		return $this->acl->isAllowed('staff_directory');	
	}
	
	public function indexAction()
	{
        $this->redirect('https://dsanet.tamu.edu/');
        return;
	}
}