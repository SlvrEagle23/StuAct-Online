<?php
class Account_LogoutController extends \DF\Controller\Action
{
	public function indexAction()
	{
		// Unset all session variables.
		@session_unset();

		if (\DF\Auth::isLoggedIn())
		{
			\DF\Auth::logout();
		}
		
		$this->flash('<b>Logged out of StuAct Online.</b><br>For security purposes, you should close your browser window after your session is completed.', \DF\Flash::SUCCESS);
        $this->redirectToRoute(array('module' => 'default'));
    }
}