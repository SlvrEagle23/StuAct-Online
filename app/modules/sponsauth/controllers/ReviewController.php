<?php
/**
 * Sponsored Event review page.
 */

use \Entity\SponsAuth;
use \Entity\User;

class Sponsauth_ReviewController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('is logged in');
	}
	
	public function indexAction()
	{
		$code = $this->_getParam('code');
		$record = SponsAuth::getRepository()->findOneBy(array('access_code' => $code));
		
		if (!($record instanceof SponsAuth))
			throw new \DF\Exception\DisplayOnly('Request not found!');
		
		if ($record->is_approved == 1)
        {
            if ($record->approver instanceof User)
            {
                $approver = $record->approver;
                throw new \DF\Exception\DisplayOnly('This request was reviewed and approved by '.$approver->firstname.' '.$approver->lastname.'.');
            }
            else
            {
                throw new \DF\Exception\DisplayOnly('This request has already been reviewed.');
            }
        }
		
		if (isset($_REQUEST['decision']))
		{
			if ($_REQUEST['decision'] == "approve")
			{
				$record->is_approved = 1;
                $record->approver = $this->auth->getLoggedInUser();
				$record->save();
				
				$this->alert('Request approved! No additional action is required.');
			}
			else
			{
				$record->delete();
				
				$this->alert('Request declined! No additional action is necessary.');
			}
			
			$this->redirectToRoute(array('module' => 'sponsauth', 'controller' => 'index', 'action' => 'index'));
			return;
		}
        
        $this->view->record = $record;
		
		$form = new \StuAct\Form\SponsAuth($record);
		$this->view->form = $form;
	}
}