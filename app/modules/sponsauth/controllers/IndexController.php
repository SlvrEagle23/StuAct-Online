<?php
/**
 * Sponsored Event page.
 */

use \Entity\SponsAuth;

class Sponsauth_IndexController extends \DF\Controller\Action
{	
	public function indexAction()
	{
		$calendar = new \DF\Calendar($this->_getParam('month'));
		
		// Fetch a list of events.
		$timestamps = $calendar->getTimestamps();
		$records = $this->em->createQuery('SELECT sa FROM \Entity\SponsAuth sa WHERE sa.event_starttime <= :end AND sa.event_endtime >= :start AND sa.is_approved = 1 ORDER BY sa.event_type ASC')
            ->setParameter('start', $timestamps['start'])
            ->setParameter('end', $timestamps['end'])
            ->getArrayResult();
		
		foreach($records as &$record)
		{
			$record['start_timestamp'] = $record['event_starttime'];
			$record['end_timestamp'] = $record['event_endtime'];
		}
		
		$this->view->records = $records;
		$this->view->calendar = $calendar->fetch($records);
	}

	public function detailsAction()
	{
		$id = (int)$this->_getParam('id');
		$record = SponsAuth::find($id);

		if (!($record instanceof SponsAuth))
			throw new \DF\Exception\DisplayOnly('Record not found!');

		$this->view->record = $record;
	}
	
	/* Submission Interstitial Form */
	public function submitAction()
	{}
	
	/* Sponsored Event Form */
	public function sponsoredAction()
	{
        \DF\Acl::checkPermission('is logged in');
        
		$form = new \StuAct\Form\SponsAuth(NULL, 'sponsored');
		
		if( !empty($_POST) && $form->isValid($_POST) )
        {
			$record = $form->save();
			
			\DF\Messenger::send(array(
				'to'		=> $this->current_module_config->routing->sponsored,
				'subject' 	=> 'Sponsored Event Requires Review',
				'template' 	=> 'review',
				'module' 	=> 'sponsauth',
				'vars'		=> array(
                    'type'      => 'Sponsored',
                    'form'		=> $form,
					'record'	=> $record,
					'code'		=> $record->access_code,
				),
			));
			
			$this->alert('Form successfully submitted!');
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}
		
		$this->view->form = $form;
	}
	
	/* Sponsored Authorized Form */
	public function authorizedAction()
	{
        \DF\Acl::checkPermission('is logged in');
        
		$form = new \StuAct\Form\SponsAuth(NULL, 'authorized');
		
		if( !empty($_POST) && $form->isValid($_POST) )
        {
			$record = $form->save();
			
			$dept_routing = $this->current_module_config->routing->departments->toArray();
			$dept_name = $record->dept_name;
			
			if (isset($dept_routing[$dept_name]))
			{
				\DF\Messenger::send(array(
					'to'		=> $dept_routing[$dept_name],
					'subject' 	=> 'Authorized Event Requires Review',
					'template' 	=> 'review',
					'module' 	=> 'sponsauth',
					'vars'		=> array(
                        'type'      => 'Authorized',
                        'form'		=> $form,
						'record'	=> $record,
						'code'		=> $record->access_code,
					),
				));
			}
			
			$this->alert('Form successfully submitted!');
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}
		
		$this->view->form = $form;
	}
}