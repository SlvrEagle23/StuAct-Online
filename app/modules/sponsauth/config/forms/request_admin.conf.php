<?php
/**
 * Sponsored/Authorized Event - Administrator View
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		
		'groups'		=> array(
            
            'admin_info' => array(
                'legend' => 'Administrative Details',
                'elements' => array(
                    
                    'is_approved' => array('radio', array(
                        'label' => 'Is Approved',
                        'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                    )),
                    
                ),
            ),
			
			'submit_group' => array(
				'elements' => array(
					'submit_btn' => array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Submit Form',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),
		),
	),
);