<?php
return array(
    'default' => array(
        'batch' => array(
            'label' => 'Batch Manager Center',
            'module' => 'batch',
            'permission' => 'is logged in',
            
            'pages' => array(
                'batch_grade' => array(
                    'module' => 'batch',
                    'controller' => 'index',
                    'action' => 'grades',
                ),
                'batch_roster' => array(
                    'module' => 'batch',
                    'controller' => 'index',
                    'action' => 'roster',
                ),
            ),
        ),
    ),
);