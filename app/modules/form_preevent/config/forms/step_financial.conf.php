<?php
use \DF\Utilities;

return array(
	'id' => 'pep_form',
	'method' => 'post',

	'groups' => array(

		'Financial_Procedures' => array(
			'legend'		=> 'Financial Procedures',
			'elements'		=> array(

				'File_Budget' => array('file', array(
					'label' => 'Upload Budget',
					'description' => 'Upload an event budget that covers all services required for the event, including but not limited to: UPD security, facilities services, parking, food, advertising, entertainment and stage/lighting/sound.',
				)),

				'Will_release_funds' => array('radio', array(
					'label' => 'Will you need to release funds from your account prior to this event (i.e. preparing a check for a service, getting a cash advance, having cash working fund available during your event, etc.) ?',
					'description' => 'If Yes: Please contact the <a href="http://sofc.tamu.edu" target="_blank">Student Organization Finance Center</a> to provide advance notice of your financial needs for this event.',
					'multiOptions' => array('Yes' => 'Yes', 'No' => 'No'),
					'required' => true,
				)),

				'Requires_concessions_permit' => array('radio', array(
					'label' => 'Does this event involve the sale or exchange of goods, services, or money <i>on campus</i>? For example: apparel, tickets, food, promotional materials, etc.',
					'description' => 'If yes: You will need to obtain a Concessions Permit from the Department of Student Activities at least 48 hours in advance of your event. Please visit <a href="http://studentactivities.tamu.edu/concessions" target="_blank">the concessions permit page</a> for more information about whether or not you will need a concessions permit for this event and to download the permit request form.',
					'multiOptions' => array('Yes' => 'Yes', 'No' => 'No'),
					'required' => true,
				)),
				'Applying_for_special_funding' => array('radio', array(
					'label' => 'Is the organization applying for <a href="http://studentactivities.tamu.edu/sofc/riskinitiativefunding" target="_blank">Risk Initiative</a> or <a href="http://studentactivities.tamu.edu/sofc/studentorganizationfunding" target="_blank">Student Organization</a> Funding through Student Activities?',
					'multiOptions' => array('Yes' => 'Yes', 'No' => 'No'),
				)),
				'Financial_health' => array('textarea', array(
					'label' => 'How is the financial health of the organization affected by this event?',
					'class' => 'full-width full-height',
					'required' => true,
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);