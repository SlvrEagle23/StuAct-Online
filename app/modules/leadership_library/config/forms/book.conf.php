<?php
/**
 * Add/Edit Book
 */

$categories = \Entity\LeadershipLibraryCategory::fetchSelect();

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(

            'categories' => array('multiCheckbox', array(
                'label' => 'Categories',
                'description' => 'You must assign at lease one category so the book will appear in public searches.',
                'required' => true,
                'multiOptions' => $categories,
            )),
            
            'isbn' => array('text', array(
                'label' => 'ISBN',
                'class' => 'half-width',
                'description' => 'Enter the book\'s ISBN here, and the service will automatically connect to the Google Books service to retrieve author, title, and description information. If you enter an ISBN, all fields below are not required.',
            )),
            
            'title' => array('text', array(
                'label' => 'Book Title',
                'class' => 'full-width',
            )),
            
            'author' => array('text', array(
                'label' => 'Author(s)',
                'class' => 'full-width',
            )),
            
            'description' => array('textarea', array(
                'label' => 'Description',
                'class' => 'full-width half-height',
            )),
            
            'is_lost' => array('radio', array(
                'label' => 'Is Lost',
                'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                'default' => 0,
            )),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);