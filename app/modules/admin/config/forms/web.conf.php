<?php
return array(
	'method' => 'post',
	'groups' => array(

		'web_account' => array(
			'legend' => 'Web Account',
			'elements' => array(
				'it_web_pending' => array('radio', array(
					'label' => 'Request Pending',
					'description' => 'This value is automatically set to "Yes" after a request is submitted from an organization, and should be reset to "No" after it has been processed.',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				)),

				'it_web_type' => array('radio', array(
					'label' => 'Hosting Active on cPanel',
					'description' => 'Switching this value from "No" to "Yes" will create a new cPanel account. Switching this value from "Yes" to "No" will remove the existing cPanel account.',
					'multiOptions' => array(0 => 'No', 10 => 'Yes'),
				)),

				'it_web_addr' => array('text', array(
					'label' => 'Web Address',
					'description' => 'Format: orgname.tamu.edu.',
					'class' => 'half-width',
				)),

				'it_web_username' => array('text', array(
					'label' => 'Account Username',
					'description' => 'Max length: 8 Characters.',
					'maxlength' => 8,
				)),

				'it_web_password' => array('text', array(
					'label' => 'Account Password',
					'description' => 'A random password is automatically generated and inserted into this field.',
				)),
			),
		),

		'other_functions' => array(
			'legend' => 'Special Processing',
			'elements' => array(

				'send_cname_email' => array('radio', array(
					'label' => 'Send Domain Registration E-mail to DoIT',
					'description' => 'If the web address listed above is not already registered to the cPanel server, set this field to "Yes" to send a notification to the DoIT systems team to register the domain name.',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'default' => 0,
				)),

				'update_db_only' => array('radio', array(
					'label' => 'Update StuAct Online Only (No cPanel Update)',
					'description' => 'This field should only be used in the event that the StuAct Online database and cPanel are out-of-sync due to a malfunction.',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'default' => 0,
				)),

			),
		),

		'submit_btn' => array(
			'elements' => array(
				'submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Create Account',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),

	),
);