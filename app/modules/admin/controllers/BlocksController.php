<?php
use \Entity\Block;

class Admin_BlocksController extends \DF\Controller\Action
{
    public function permissions()
    {
		return $this->acl->isAllowed('administer blocks');
    }
    
    public function indexAction()
    {
		$this->view->all_blocks = Block::fetchArray('name');
    }
	
	public function editAction()
	{
        $form = new \StuAct\Form($this->current_module_config->forms->block->form);
        
        $id = (int)$this->_getParam('id');
        if ($id != 0)
		{
			$record = Block::find($id);
			$form->setDefaults($record->toArray(TRUE, TRUE));
		}
		
        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
			
			if (!($record instanceof Block))
				$record = new Block;
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('Changes saved.', 'green');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
			return;
        }
        
        $this->renderForm($form);
        $this->view->tinymce();
	}
	
	public function previewAction()
	{
        $id = (int)$this->_getParam('id');
		$record = Block::find($id);
		$this->view->block = $record;
	}
	
	public function deleteAction()
	{
		$record = Block::find($this->_getParam('id'));
		if ($record instanceof Block)
			$record->delete();
			
		$this->alert('Record deleted.', 'green');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}
}