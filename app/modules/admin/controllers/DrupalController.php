<?php
use \Entity\Organization;

class Admin_DrupalController extends \DF\Controller\Action
{
	public function permissions()
    {
    	return $this->acl->isAllowed('administer web sites');
    }

    public function indexAction()
    {
    	if ($this->_hasParam('id'))
    		$this->redirectFromHere(array('action' => 'org'));

    	$drupal_config = $this->config->services->drupal->toArray();
    	$drupal_version_file = $drupal_config['source_dir'].'/sact_version.php';
    	require($drupal_version_file);

    	if ($this->_getParam('origin') == "auto")
			$this->_autoUpdate(NCE_DRUPAL_VERSION);

		$orgs_using_drupal = $this->em->createQuery('SELECT o FROM Entity\Organization o WHERE o.it_web_drupal = 1 AND (o.it_web_suspended IS NULL OR o.it_web_suspended = 0) ORDER BY o.it_web_drupal_version DESC, o.name ASC');

		$this->view->pager = new \DF\Paginator\Doctrine($orgs_using_drupal, $this->_getParam('page', 1));
		$this->view->drupal_version = NCE_DRUPAL_VERSION;
    }

    public function completeAction()
    {
    	$layout = \Zend_Layout::getMvcInstance();
    	$layout->setLayout('maintenance');
    	
    	$this->render();
    }

    protected function _autoUpdate($drupal_version)
    {
    	$outdated_orgs = $this->em->createQuery('SELECT o.id FROM Entity\Organization o WHERE o.it_web_drupal = 1 AND (o.it_web_suspended IS NULL OR o.it_web_suspended = 0) AND o.it_web_drupal_version != :current ORDER BY o.it_web_drupal_version DESC, o.name ASC')
			->setParameter('current', $drupal_version)
			->getArrayResult();

		if (count($outdated_orgs) > 0)
		{
			$first_org = $outdated_orgs[0]['id'];
			$this->redirectFromHere(array('action' => 'install', 'id' => $first_org));
			return;
		}
		else
		{
			$this->alert('<b>Automatic Upgrade Complete</b><br />All Drupal-powered sites are running the current version of Drupal.', 'green');
		}
    }

    public function orgAction()
    {
    	$org_id = (int)$this->_getParam('id');
		$org = Organization::find($org_id);

		if (!($org instanceof Organization))
			throw new \DF\Exception\DisplayOnly('Organization not found!');

		if ($org->it_web_type != 10)
			throw new \DF\Exception\DisplayOnly('This organization does not currently have a cPanel account. Please set up a web account first before installing Drupal.');

		$this->view->org = $org;
    }

    public function uninstallAction()
    {
    	$org_id = (int)$this->_getParam('id');
    	$org = Organization::find($org_id);

    	$drupal = new \StuAct\Drupal($org);
		$drupal->uninstall();

		$this->alert('<b>Drupal successfully uninstalled.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
	}

	public function installAction()
	{
		$org_id = (int)$this->_getParam('id');
    	$org = Organization::find($org_id);

		$drupal = new \StuAct\Drupal($org);
		$reinstall = ($this->_getParam('complete', 0) != 0);
		$result = $drupal->install(FALSE, $reinstall, (int)$this->_getParam('install_page', 1));

		if ($result['action'] == "iframe")
		{
			$this->view->url = $result['url'];
			$this->view->org = $org;

			$this->render();
		}
		else
		{
			$this->alert('<b>Drupal install successful for organization "'.$org->name.'".</b>', 'green');

			$origin = $this->_getParam('origin');
			if ($origin == "org")
				$this->redirect($org->route(array('controller' => 'web')));
			elseif ($origin == "pending")
				$this->redirectToRoute(array('module' => 'admin', 'controller' => 'webs'));
			else
				$this->redirectFromHere(array('action' => 'index', 'install_page' => NULL, 'id' => NULL, 'complete' => NULL));
		}
	}
}