<?php
use \DF\Utilities;

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'elements'		=> array(

		'show' => array('multiCheckbox', array(
			'label' => 'Include Rates',
			'multiOptions' => array(
				'general'		=> 'General Liability Insurance',
				'accident'		=> 'Accident Medical Insurance',
				'support'		=> 'Support Service Fee',
			),
		)),

		'is_sports' => array('radio', array(
			'label' => 'Is Sports?',
			'multiOptions' => array(0 => 'No', 1 => 'Yes'),
		)),

		'btn_submit' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Generate Invoice',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);