<?php
use \DF\Utilities;

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'elements'		=> array(

		'name' => array('text', array(
			'label' => 'Routing Area Name',
			'class' => 'full-width',
			'required' => true,
		)),

		'secondary' => array('text', array(
			'label' => 'Secondary Routing E-mail (if applicable)',
			'class' => 'half-width',
			'validators' => array('EmailAddress'),
		)),
		
		'dept' => array('text', array(
			'label' => 'Department Head E-mail',
			'class' => 'half-width',
			'validators' => array('EmailAddress'),
			'required' => true,
		)),
		'dean' => array('text', array(
			'label' => 'Dean E-mail',
			'class' => 'half-width',
			'validators' => array('EmailAddress'),
			'required' => true,
		)),
		
		'btn_submit' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Save Changes',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);