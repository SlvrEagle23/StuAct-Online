<?php
return array(
    'method' => 'post',
    'groups' => array(
        
        'account_info' => array(
            'legend' => 'Account Information',
            'elements' => array(
                
                'email' => array('text', array(
                    'label' => 'E-mail Address',
                    'class' => 'half-width',
                    'required' => true,
                    'validators' => array('EmailAddress'),
                )),
        
                'password' => array('password', array(
                    'label' => 'Reset Password',
                    'autocomplete' => 'off',
                    'description' => 'To reset your password, enter a new one into the field below. Otherwise, leave this field blank.',
                )),
                
            ),
        ),
        
        'general_info' => array(
            'legend' => 'General Information',
            'elements' => array(
                
                'firstname' => array('text', array(
                    'label' => 'First Name',
                    'required' => true,
                )),

                'lastname' => array('text', array(
                    'label' => 'Last Name',
                    'required' => true,
                )),

            ),
        ),
        
        'submit' => array(
            'elements' => array(
                'submit'        => array('submit', array(
                    'type'  => 'submit',
                    'label' => 'Save Profile',
                    'helper' => 'formButton',
                    'class' => 'ui-button',
                )),
            ),
        ),
        
    ),
);