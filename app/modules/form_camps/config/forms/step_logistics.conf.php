<?php

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'groups' => array(

		'policy_notice' => array(
			'elements' => array(
				'Policy' => array('markup', array(
					'markup' => '<p>Please be advised of the Camps and Programs for Minors Standard Administrative Procedure when completing the application.</p>',
				)),
			),
		),

		'program' => array(
			'legend' => 'Program Information',
			'elements' => array(
				'name' => array('text', array(
					'label' => 'Program/Event Name',
					'class' => 'full-width',
					'required' => true,
				)),

				'website' => array('text', array(
					'label' => 'Program/Event Web Site URL',
					'class' => 'full-width',
					'required' => true,
				)),

				'brochure' => array('file', array(
					'label' => 'Upload Program Brochure',
				)),
			),
		),

		'sponsor' => array(
			'legend' => 'CPM Application Contact',
			'description' => 'This information is automatically populated from your program profile. If contact information is different for this event, update the fields below.',
			'elements' => array(

				'cpm_sponsor_name' => array('text', array(
                    'label' => 'Name',
                    'class' => 'full-width',
                    'required' => true,
                )),
                'cpm_sponsor_email' => array('text', array(
                    'label' => 'E-mail Address',
                    'class' => 'half-width',
                    'required' => true,
                    'validators' => array('EmailAddress'),
                )),
                'cpm_sponsor_title' => array('text', array(
                    'label' => 'Title',
                    'class' => 'half-width',
                    'required' => true,
                )),
                'cpm_sponsor_org' => array('text', array(
                    'label' => 'Organization/Department',
                    'class' => 'half-width',
                    'required' => true,
                )),
                'cpm_sponsor_phone' => array('text', array(
                    'label' => 'Office Phone Number',
                    'required' => true,
                )),
                'cpm_sponsor_cell' => array('text', array(
                    'label' => 'Mobile Phone Number',
                )),

			),
		),

		'secondary_contact' => array(
			'legend' => 'Secondary Contact',
			'elements' => array(

				'secondary_name' => array('text', array(
                    'label' => 'Name',
                    'class' => 'full-width',
                )),
                'secondary_email' => array('text', array(
                    'label' => 'E-mail Address',
                    'class' => 'half-width',
                    'validators' => array('EmailAddress'),
                )),
                'secondary_title' => array('text', array(
                    'label' => 'Title',
                    'class' => 'half-width',
                )),
                'secondary_org' => array('text', array(
                    'label' => 'Organization/Department',
                    'class' => 'half-width',
                )),
                'secondary_phone' => array('text', array(
                    'label' => 'Office Phone Number',
                )),
                'secondary_cell' => array('text', array(
                    'label' => 'Mobile Phone Number',
                )),

			),
		),

		'sessions' => array(
			'legend' => 'Sessions',
			'description' => 'Please use the fields below to describe all of the individual sessions in the program. For each of the fields below, list each session on a new line. If several sessions have the same numbers, they can be described on a single line.',

			'elements' => array(
				'session_dates_field' => array('textarea', array(
					'label' => 'Session Date(s)',
					'description' => 'If your session dates are already listed in a spreadsheet or other document, you can attach this document using the field below instead of typing the session dates into this field.',
					'class' => 'half-width half-height',
				)),
				'session_dates_file' => array('file', array(
					'label' => 'Upload Session Date(s) File (Optional)',
				)),

				'start_of_first_session' => array('unixDate', array(
					'label' => 'Start Date of First Session',
					'required' => true,
				)),
				'end_of_last_session' => array('unixDate', array(
					'label' => 'End Date of Last Session',
					'required' => true,
				)),

				'est_num_attendees' => array('textarea', array(
					'label' => 'Estimated Number of Attendees',
					'class' => 'half-width full-height',
					'required' => true,
				)),
				'num_counselors' => array('textarea', array(
					'label' => 'Number of Counselors/Program Staff Members',
					'class' => 'half-width full-height',
					'required' => true,
				)),
				'num_volunteers' => array('textarea', array(
					'label' => 'Number of Counselors/Program Staff Members who are Volunteers',
					'class' => 'half-width full-height',
					'required' => true,
				)),
				'num_tamu_employees' => array('textarea', array(
					'label' => 'Number of Counselors/Program Staff Members who are Employees of TAMU (i.e. went through the TAMU HR hiring/payroll processes)',
					'class' => 'half-width full-height',
					'required' => true,
				)),
			),
		),

		'location_grp' => array(
			'legend' => 'Location',
			'elements' => array(
				'location' => array('multiCheckbox', array(
					'label' => 'Program Location(s)',
					'multiOptions' => array('on' => 'On Campus', 'off' => 'Off Campus'),
					'required' => true,
				)),

				'venue' => array('textarea', array(
					'label' => 'Name of Venue(s)',
					'class' => 'full-width half-height',
					'required' => true,
				)),

				'international' => array('radio', array(
					'label' => 'Does this camp include international travel?', 
					'description' => 'For more information, see the <a href="http://rules-saps.tamu.edu/PDFs/21.01.03.M1.pdf" target="_blank">TAMU rule on foreign travel</a>.',
					'class' => 'conditional-decider yesno',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
				)),

				'international_location' => array('text', array(
					'label' => 'Where?',
					'class' => 'full-width conditional international-yes',
				)),

				'international_safety' => array('textarea', array(
					'label' => 'What steps have been taken to ensure safety?',
					'class' => 'full-width half-height conditional international-yes',
				)),

				'international_studyabroad' => array('radio', array(
					'label' => 'Has Study Abroad been notified?', 
					'description' => 'It is important that you begin working with the Study Abroad Office a minimum of 3-6 months prior to departure. For more information, see the <a href="http://studentactivities.tamu.edu/risk/travel/international" target="_blank">Student Activities international travel guide</a> and the <a href="http://studyabroad.tamu.edu">Study Abroad program homepage</a>.',
					'class' => 'conditional international-yes',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				)),

				'international_travelwarning' => array('radio', array(
					'label' => 'Has a travel warning been issued by the State Department for the location abroad?', 
					'description' => 'For more information regarding travel warnings, visit <a href="http://travel.state.gov/travel/cis_pa_tw/cis_pa_tw_1168.html" target="_blank">this page</a>.',
					'class' => 'conditional international-yes',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);