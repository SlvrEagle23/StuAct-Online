<?php
return array(
    'method' => 'post',
    'groups' => array(

    	'instructions_grp' => array(
    		'legend' => 'Instructions',
    		'elements' => array(

    			'instructions' => array('markup', array(
    				'markup' => '
    					<p>Please submit the following information within 48 hours of any incident involving injury to or affecting the health or safety of a participant. If there are any witnesses involved, please provide contact information for a statement from each individual. Upon completion of this form, please forward a copy to the Office of Rules and Regulatory Compliance, Mail Stop 1181.</p>
    
    					<p>In addition to this form, camp sponsors should download the <a href="http://studentactivities.tamu.edu/files/IncidentReportForm.doc" target="_blank">Incident Report Form (Word DOC)</a> and follow the instructions listed in the document.</p>
    				',
    			)),

    		),
    	),

        'general_info' => array(
            'legend' => 'General Information',
            'elements' => array(

            	'app_id' => array('text', array(
            		'label' => 'Camp Application ID',
            		'required' => true,
            	)),

            	'program_name' => array('text', array(
            		'label' => 'Camp/Program Name',
            		'class' => 'full-width',
            		'required' => true,
            	)),

            	'Report_date' => array('unixdatetime', array(
            		'label' => 'Report Date/Time',
            	)),

            	'Incident_date' => array('unixdatetime', array(
            		'label' => 'Date/Time of Incident',
            	)),

            	'Reporting_party' => array('text', array(
            		'label' => 'Reporting Party',
            		'class' => 'full-width',
            	)),

            	'Telephone_number' => array('text', array(
            		'label' => 'Telephone Number',
            	)),

            	'Location_of_incident' => array('text', array(
            		'label' => 'Location of Incident',
            		'class' => 'full-width',
            	)),

            	'Description_of_incident' => array('textarea', array(
            		'label' => 'Detailed Description of Incident',
            		'class' => 'full-width half-height',
            		'required' => true,
            	)),

            	'Root_Cause_of_incident' => array('textarea', array(
            		'label' => 'Root Cause of Incident',
            		'class' => 'full-width half-height',
            	)),

            	'Corrective_actions_suggested' => array('textarea', array(
            		'label' => 'Corrective Action(s) Suggested',
            		'class' => 'full-width half-height',
            	)),

            ),
        ),
        
        'injured_involved_parties' => array(
            'legend' => 'Injured/Involved Parties',
            'elements' => array(

            	'Party_1__Name' => array('text', array(
            		'label' => 'Party 1 Name',
            		'class' => 'full-width',
            	)),
            	'Party_1__Address' => array('text', array(
            		'label' => 'Party 1 Address',
            		'class' => 'full-width',
            	)),
            	'Party_1__Phone_number' => array('text', array(
            		'label' => 'Party 1 Phone Number',
            	)),
            	'Party_1__Parent_or_Guardian_name' => array('text', array(
            		'label' => 'Party 1 Parent/Guardian Name',
            		'class' => 'full-width',
            	)),

            	'Party_2__Name' => array('text', array(
            		'label' => 'Party 2 Name',
            		'class' => 'full-width',
            	)),
            	'Party_2__Address' => array('text', array(
            		'label' => 'Party 2 Address',
            		'class' => 'full-width',
            	)),
            	'Party_2__Phone_number' => array('text', array(
            		'label' => 'Party 2 Phone Number',
            	)),
            	'Party_2__Parent_or_Guardian_name' => array('text', array(
            		'label' => 'Party 2 Parent/Guardian Name',
            		'class' => 'full-width',
            	)),

            ),
        ),

		'witnesses' => array(
			'legend' => 'Witnesses',
			'description' => 'Please note the name and contact information for any witnesses to the incident.',
			'elements' => array(

				'Witness_1__Name' => array('text', array(
					'label' => 'Witness 1 Name',
					'class' => 'full-width',
				)),
				'Witness_1__Phone_number' => array('text', array(
					'label' => 'Witness 1 Phone Number',
				)),
				'Witness_1__Address' => array('text', array(
					'label' => 'Witness 1 Address',
					'class' => 'full-width',
				)),

				'Witness_2__Name' => array('text', array(
					'label' => 'Witness 2 Name',
					'class' => 'full-width',
				)),
				'Witness_2__Phone_number' => array('text', array(
					'label' => 'Witness 2 Phone Number',
				)),
				'Witness_2__Address' => array('text', array(
					'label' => 'Witness 2 Address',
					'class' => 'full-width',
				)),

			),
		),

		'response_questions' => array(
			'legend' => 'Response Questions',
			'elements' => array(

				'Did_university_police_respond' => array('radio', array(
					'label' => 'Did University Police Respond?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				)),

				'Officer_responding' => array('text', array(
					'label' => 'If Yes, Officer Responding',
					'class' => 'full-width',
				)),

				'Incident_report_number' => array('text', array(
					'label' => 'If Yes, Incident Report Number',
					'class' => 'full-width',
				)),

				'Did_anyone_receive_treatment_at_a_medical_facility' => array('radio', array(
					'label' => 'Did Anyone Receive Treatment at a Medical Facility?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				)),

				'Medical_facility' => array('text', array(
					'label' => 'If Yes, Where?',
					'class' => 'full-width',
				)),

				'Medical_transportation_method' => array('text', array(
					'label' => 'If Yes, Transport Provided by',
					'class' => 'full-width',
				)),

				'Was_incident_claim_report_filed' => array('radio', array(
					'label' => 'Was an insurance claim report filed?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				)),

			),
		),
        
        'submit' => array(
            'elements' => array(
                'submit'        => array('submit', array(
                    'type'  => 'submit',
                    'label' => 'Submit Incident Report',
                    'helper' => 'formButton',
                    'class' => 'ui-button',
                )),
            ),
        ),
        
    ),
);