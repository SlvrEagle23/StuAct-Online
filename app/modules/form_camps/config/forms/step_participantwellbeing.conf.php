<?php

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'groups' => array(

		'firstaid_grp' => array(
			'legend' => 'First Aid',
			'elements' => array(
				'firstaid_note' => array('markup', array(
					'label' => 'Important Note',
					'markup' => '
						<p>First Aid information should include all activities for the event. If there will be several different types of first aid for each activity (i.e. medics, first aid trained counselors, first aid kits, etc.), specific, detailed information should be given for each activity. Listing only that the emergency medical facility will be utilized or that the facility was notified is not sufficient.</p>
					',
				)),

				'firstaid_process' => array('text', array(
					'label' => 'Please describe how first aid will be administered for the camp or program',
					'class' => 'full-width',
					'required' => true,
				)),

				'firstaid_training' => array('text', array(
					'label' => 'What type of First Aid training will be provided to program counselors?',
					'class' => 'full-width',
					'required' => true,
				)),

				'firstaid_professionals_present' => array('text', array(
					'label' => 'Which individuals with first aid or medical training (may include police or security forces) will be present (or in the vicinity) during program activities?',
					'class' => 'full-width',
					'required' => true,
				)),

				'firstaid_kit' => array('radio', array(
					'label' => 'Will a First Aid Kit be provided immediately to the location of the camp or program activities?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
				)),
			),
		),

		'medicine_grp' => array(
			'legend' => 'Medicine Distribution & Storage',
			'elements' => array(
				'who_dispenses_medication' => array('text', array(
					'label' => 'Who will be responsible for storing, securing and dispensing participants\' medications?',
					'description' => 'Note: It is recommended that parents/guardians provide written consent for any medication (including, but not limited to, over-the-counter drugs such as Tylenol and Ibuprofen) to be dispensed to their children. Parents/guardians will also need to provide dosage amounts as well as the frequency of medication administration.',
					'class' => 'full-width',
					'required' => true,
				)),

				'medication_refrigeration' => array('text', array(
					'label' => 'If participants\' medications need to be kept refrigerated, where will the medications be refrigerated with restricted access?',
					'class' => 'full-width',
					'required' => true,
				)),
			),
		),

		'food_and_allergies_GRP' => array(
			'legend' => 'Food & Allergies',
			'elements' => array(
				'allergy_policies' => array('textarea', array(
					'label' => 'Please describe which processes are in place to ensure that restrictions are appropriately applied',
					'description' => 'Examples: Providing alternative foods and taking other preventative measures to avoid exposure when allergies are noted, and ensuring that arrangements are made to prevent specific contacts in the case of severe allergies.',
					'class' => 'full-width half-height',
					'required' => true,
				)),

				'preparing_own_food' => array('radio', array(
					'label' => 'Will the Camp or program prepare and/or serve their own foods?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
				)),
			),
		),

		'heat_issues_grp' => array(
			'legend' => 'Heat Issues',
			'elements' => array(
				'heat_stroke' => array('text', array(
					'label' => 'When will counselors be provided information on recognition and treatment of heat stroke for strenuous outside activity?',
					'class' => 'full-width',
					'required' => true,
				)),

				'heat_prevention' => array('textarea', array(
					'label' => 'Please describe which measures will be taken to prevent heat exhaustion or heat stroke during strenuous outside activity',
					'description' => 'Examples: Providing cool drinks and frequently encouraging or reminding participants to consume them, breaks or rest periods from extended physical activity, and having staffers be alert for the symptoms of the onset of heat exhaustion or physical distress.',
					'class' => 'full-width half-height',
					'required' => true,
				)),
			),
		),

		'activity_grp' => array(
			'legend' => 'Activities',
			'elements' => array(

				'activities' => array('multiCheckbox', array(
					'label' => 'Select all activities that will take place during this program',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Rappelling',
						'Skin Diving',
						'Scuba Diving',
						'Snow Skiing',
						'Water Skiing',
						'Whitewater Rafting',
						'Bungee Jumping',
						'Motorsports (golf carts, bumper cars, etc.)',
						'Rodeo or any equestrian related sports',
						'Ballooning',
						'Parachuting',
						'Pyrotechnics',
						'Boating',
						'Zip Lining',
						'None of the Above',
					)),
					'required' => true,
				)),

			),
		),

		'conduct' => array(
			'legend' => 'Participant Conduct',
			'elements' => array(

				'conduct_expectations' => array('textarea', array(
					'label' => 'When are participants briefed on conduct and safety expectations?',
					'class' => 'full-width half-height',
					'required' => true,
				)),
				
			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);