<h2>New Camps Account Registered</h2>

<p>Howdy! This e-mail address was recently used to sign up for a new account for the Camps Online Application at the Department of Student Activities web site.</p>

<dl>
	<dt>E-mail Address:</dt>
    <dd>{$email_address}</dd>
</dl>

<p>
	To create new forms and view previously submitted ones, visit this address:<br />
    <a href="{$config_data.url_base}/forms/camps/index">{$config_data.url_base}/forms/camps/index</a>
</p>