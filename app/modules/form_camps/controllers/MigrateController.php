<?php
use \Entity\CampsForm;
use \Entity\CampsFormIncident;
use \Entity\CampsFormLog;
use \Entity\CampsFormProgram;
use \Entity\CampsFormRouting;
use \Entity\CampsFormUser;

class Form_Camps_MigrateController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('admin_forms_camps');
    }

    protected $_settings;
    public function preDispatch()
    {
    	parent::preDispatch();
    	$this->_settings = CampsForm::loadSettings();

    	$this->view->action = $this->_getActionName();
    }

    public function indexAction()
    {
        $this->doNotRender();
        set_time_limit(6000);

        // Re-generate indexes for all records.
        $forms = $this->em->createQuery('SELECT cf FROM Entity\CampsForm cf');
        $forms_loop = $forms->iterate();

        foreach($forms_loop as $iteration)
        {
            $record = $iteration[0];

            $record->reloadIndex();
            $this->em->persist($record);

            if ($i % 50 == 0)
            {
                $this->em->flush();
                $this->em->clear();
            }
            $i++;
        }

        $this->em->flush();
        $this->em->clear();

        echo 'Done!';
        return;

        // Detect existing programs.
        $programs = array();
        $programs_raw = CampsFormProgram::fetchArray();
        foreach($programs_raw as $program)
        {
            $name = $this->_filterName($program['name']);
            $programs[$name] = $program['id'];
        }

        // Create new programs.
        $user_programs = array();
        $name_records = $this->em->createQuery('SELECT cf.index_name, cf.user_id FROM Entity\CampsForm cf')->getArrayResult();

        foreach($name_records as $name_record)
        {
            $name = $this->_filterName($name_record['index_name']);
            $user_id = $name_record['user_id'];

            if (!isset($programs[$name]))
            {
                $record = new CampsFormProgram;
                $record->name = $name;
                $record->save();

                $programs[$name] = $record->id;
            }

            $program_id = $programs[$name];
            $user_programs[$user_id][$program_id] = $program_id;
        }

        $this->em->flush();
        $this->em->clear();

        // Associate camps with programs.
        $forms = $this->em->createQuery('SELECT cf FROM Entity\CampsForm cf');
        $forms_loop = $forms->iterate();

        foreach($forms_loop as $iteration)
        {
            $record = $iteration[0];

            $name = $this->_filterName($record->index_name);
            if (isset($programs[$name]))
            {
                $record->program = CampsFormProgram::find($programs[$name]);
            }
            $this->em->persist($record);

            if ($i % 50 == 0)
            {
                $this->em->flush();
                $this->em->clear();
            }
            $i++;
        }

        $this->em->flush();
        $this->em->clear();

        // Associate users with programs.
        $users = $this->em->createQuery('SELECT cfu FROM Entity\CampsFormUser cfu');
        $users_loop = $users->iterate();

        foreach($users_loop as $iteration)
        {
            $user = $iteration[0];
            $uid = $user->id;

            $name_parts = explode(' ', $user->name);
            $user->firstname = array_shift($name_parts);
            $user->lastname = implode(' ', $name_parts);
            $user->programs->clear();

            if (isset($user_programs[$uid]))
            {
                foreach($user_programs[$uid] as $program_id)
                {
                    $record = CampsFormProgram::find($program_id);
                    $user->programs->add($record);
                }
            }
            $this->em->persist($record);

            if ($i % 50 == 0)
            {
                $this->em->flush();
                $this->em->clear();
            }
            $i++;
        }

        $this->em->flush();
        $this->em->clear();

        echo 'Done';
    }

    protected function _filterName($name)
    {
        $name = htmlspecialchars_decode($name);

        if (strpos($name, ':') !== FALSE)
        {
            $name = substr($name, 0, strpos($name, ':'));
        }

        $str_swap = array();
        for($i = date('Y')-4; $i <= date('Y')+1; $i++)
            $str_swap[$i] = '';
        
        $name = str_replace(array_keys($str_swap), array_values($str_swap), $name);
        return trim($name);
    }
}