<?php
/**
 * MOCCASIN General Configuration
 */

define('MAROONOUT_TYPE_NORMAL', 1);
define('MAROONOUT_TYPE_BULK', 2);
define('MAROONOUT_TYPE_DELIVERY', 3);
define('MAROONOUT_TYPE_PHYSICAL', 4);

return array(
	'categories' => array(
		'maroonout'		=> 'Maroon Out',
		'maroonout_white' => 'Maroon Out (White)',
		'maroonout_bag' => 'MO Bag',
		'ewalk'			=> 'Elephant Walk',
		'jrewalk'		=> 'Junior E-Walk',
		'fishfest'		=> 'Fish Fest',
		'pulloutday'	=> 'Pull Out Day',
		'ringdance'		=> 'Ring Dance',
		'firstyell'		=> 'First Yell',
		'1111'			=> '11-11 Day',
	),

	'types' => array(
		MAROONOUT_TYPE_NORMAL		=> 'Normal',
		MAROONOUT_TYPE_BULK			=> 'Bulk Order',
		MAROONOUT_TYPE_DELIVERY		=> 'Delivery',
	    MAROONOUT_TYPE_PHYSICAL     => 'Physical Count',
	),

	'sizes' => array(
		'YM'	=> 'Youth Medium',
		'YL'	=> 'Youth Large',
		'S'		=> 'Small',
		'M'		=> 'Medium',
		'L'		=> 'Large',
		'XL'	=> 'X-Large',
		'XXL'	=> 'XX-Large',
	    'NA'    => 'N/A',
	),
);