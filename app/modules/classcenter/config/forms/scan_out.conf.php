<?php
$settings = \Entity\ClassCenterBox::loadSettings();

return array(
    'form' => array(
        'method'        => 'post',
        'elements'      => array(

            'quantity_out' => array('text', array(
                'label' => 'Starting Quantity',
            )),
            
            'submit_btn' => array('submit', array(
                'type'  => 'submit',
                'label' => 'Save Changes',
                'helper' => 'formButton',
                'class' => 'ui-button',
            )),
        ),
    ),
);