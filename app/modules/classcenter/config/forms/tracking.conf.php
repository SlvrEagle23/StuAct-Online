<?php
$settings = \Entity\ClassCenterBox::loadSettings();

return array(
    'form' => array(
        'method'        => 'post',
        'elements'      => array(

            'type' => array('radio', array(
                'label' => 'Tracking Type',
                'multiOptions' => $settings['types'],
            )),

            'quantity_out' => array('text', array(
                'label' => 'Starting Quantity',
            )),

            'quantity_in' => array('text', array(
                'label' => 'Ending Quantity',
            )),

            'quantity_damaged' => array('text', array(
                'label' => 'Quantity Damaged',
            )),

            'tracking_note' => array('textarea', array(
                'label' => 'Tracking Notes',
                'class' => 'full-width half-height',
            )),
            
            'submit_btn' => array('submit', array(
                'type'  => 'submit',
                'label' => 'Save Changes',
                'helper' => 'formButton',
                'class' => 'ui-button',
            )),
        ),
    ),
);