<?php
class Util_GprController extends \DF\Controller\Action
{
	public function permissions()
	{
        return $this->acl->isAllowed('admin_sims');
	}
	
	public function indexAction()
	{}

    public function bulkAction()
    {
        if (isset($_REQUEST['uins']))
        {
            set_time_limit(1000);

            $term = $_REQUEST['term'];
            $uins_raw = explode("\n", $_REQUEST['uins']);
            
            $compass = new \DF\Service\DoitApi();
            $term_name = \StuAct\Grades::getTermName($term);
            
            $student_records = $compass->getStudentRecords($uins_raw);
            $grade_records = \StuAct\Grades::getBulkStudentRecords($uins_raw, $term);
            
            $export_data = array(
                array(
                    'UIN',
                    'First Name',
                    'Last Name',
                    'Status',
                    'Major Code',
                    'Major Name',
                    'Ethnicity',
                    'College',
                    'Athlete?',
                    'Corps?',

                    'GPR ('.$term_name.')',
                    'Hrs Attempted ('.$term_name.')',
                    'Hrs Earned ('.$term_name.')',
                    'Hrs Passed ('.$term_name.')',
                    'Quality Points ('.$term_name.')',
                    'GPA Credits ('.$term_name.')',
                    'QDrops ('.$term_name.')',

                    'GPR (CUMUL)',
                    'Hrs Attempted (CUMUL)',
                    'Hrs Earned (CUMUL)',
                    'Hrs Passed (CUMUL)',
                    'Quality Points (CUMUL)',
                    'GPA Credits (CUMUL)',

                    'Conduct Standing',
                    'Additional Text',
                ),
            );
                
            foreach($uins_raw as $uin_raw)
            {
                set_time_limit(10);
                
                $uin = \StuAct\Grades::getUIN($uin_raw);
                
                if (strlen($uin) == 9)
                {
                    $grade_record = $grade_records[$uin];
                    $student_record = $student_records[$uin];
                    
                    $export_row = array();
                    
                    // Basic information.
                    $export_row[] = $uin;
                    $export_row[] = $student_record['firstname']['val'];
                    $export_row[] = $student_record['lastname']['val'];
                    $export_row[] = $grade_record['classification'];
                    $export_row[] = $student_record['major1']['val'];
                    $export_row[] = $student_record['major1_desc']['val'];
                    $export_row[] = $student_record['primary_ethnicity']['val'];
                    $export_row[] = $student_record['college']['val'];
                    $export_row[] = ($student_record['athlete']['val'] != 0) ? 'Y' : 'N';
                    $export_row[] = ($student_record['corps_status']['val'] != 0) ? 'Y' : 'N';
                    
                    // GPR and conduct information.
                    $gpr_info = $grade_record['complete_current'];

                    $export_row[] = number_format($gpr_info['gpa'], 2);
                    $export_row[] = (int)$gpr_info['attempted'];
                    $export_row[] = (int)$gpr_info['earned'];
                    $export_row[] = (int)$gpr_info['passed'];
                    $export_row[] = (int)$gpr_info['quality_points'];
                    $export_row[] = (int)$gpr_info['gpa_credits'];
                    $export_row[] = (int)$gpr_info['qdrops'];

                    $gpr_info = $grade_record['complete_cumul'];

                    $export_row[] = number_format($gpr_info['gpa'], 2);
                    $export_row[] = (int)$gpr_info['attempted'];
                    $export_row[] = (int)$gpr_info['earned'];
                    $export_row[] = (int)$gpr_info['passed'];
                    $export_row[] = (int)$gpr_info['quality_points'];
                    $export_row[] = (int)$gpr_info['gpa_credits'];
                    
                    $conduct = \Entity\Conduct::check($uin);
                    $export_row[] = ($conduct) ? 'Y' : 'N';
                    
                    // Additional text from initial report.
                    $additional_text = htmlspecialchars_decode($uin_raw);
                    $additional_text_array = explode("\t", $additional_text);
                    array_shift($additional_text_array);
                    foreach($additional_text_array as $additional_text_item)
                    {
                        $export_row[] = $additional_text_item;
                    }
                    
                    $export_data[] = $export_row;
                }
            }
            
            \DF\Export::csv($export_data);
            exit;
        }
        else
        {
            $this->view->term_opts = \StuAct\Grades::getAvailableTerms();
            $this->render();
        }
    }
}