<?php

$form_config = array(
	'method' => 'post',
	'groups' => array(

		'general_info' => array(
			'legend'		=> 'Organization/Class Information',
			'elements'		=> array(

				'org_name' => array('text', array(
					'label' => 'Name of Organization/Class',
					'class' => 'full-width',
					'required' => true,
				)),

				'org_contact_email' => array('text', array(
					'label' => 'Contact E-mail Address',
					'class' => 'half-width',
					'validators' => array('EmailAddress'),
					'required' => true,
				)),

			),
		),

		'travel_info' => array(
			'legend' => 'General Travel Information',
			'elements' => array(

				'destination_city' => array('text', array(
					'label' => 'Destination City',
					'class' => 'half-width',
					'required' => true,
				)),

				'purpose_of_travel' => array('textarea', array(
					'label' => 'Purpose of Travel',
					'class' => 'full-width half-height',
					'required' => true,
				)),

				'scheduled_stops' => array('textarea', array(
					'label' => 'Description of Scheduled Stops',
					'class' => 'full-width half-height',
					'required' => true,
				)),

				'travel_route' => array('textarea', array(
					'label' => 'Description of Travel Route',
					'class' => 'full-width half-height',
					'required' => true,
				)),

			),
		),

		'contact_info' => array(
			'legend' => 'Contact Information',
			'elements' => array(
				
				'advisor_name' => array('text', array(
					'label' => 'Faculty/Staff Advisor Name',
					'class' => 'half-width',
					'required' => true,
				)),

				'advisor_email' => array('text', array(
					'label' => 'Faculty/Staff Advisor E-mail Address',
					'class' => 'half-width',
					'validators' => array('EmailAddress'),
					'required' => true,
				)),

				'is_advisor_accompanying' => array('radio', array(
					'label' => 'Is Advisor Accompanying?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				)),

				'trip_coordinator' => array('text', array(
					'label' => 'Trip Coordinator',
					'description' => 'This is usually the person traveling.',
					'class' => 'half-width',
					'required' => true,
				)),

				'emergency_contact_name' => array('text', array(
					'label' => 'Emergency Contact Name',
					'class' => 'half-width',
					'required' => true,
				)),

				'emergency_contact_number' => array('text', array(
					'label' => 'Emergency Contact Phone Number',
					'required' => true,
				)),

				'intravel_contact' => array('text', array(
					'label' => 'In-Travel Contact Information',
					'class' => 'full-width',
					'required' => true,
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'submit_btn' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);

return $form_config;