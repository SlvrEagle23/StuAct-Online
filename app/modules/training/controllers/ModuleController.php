<?php
/**
 * Training Center - Advisors Area
 */

use \Entity\Training;
use \Entity\Quiz;

class Training_ModuleController extends \DF\Controller\Action
{
	public function permissions()
	{
        return true;
	}
    
    protected $_training_session;
    protected $_course_name;
    protected $_course_info;
    protected $_step;
    protected $_step_info;
    
    public function indexAction()
    {
        $this->_training_session = new \Zend_Session_Namespace('Training');
        
        if (!$this->_hasParam('course'))
            throw new \DF\Exception\DisplayOnly('No course specified!');
        
        $this->_course_name = $this->_getParam('course');
        
        if ($this->auth->isLoggedIn())
            $user = $this->auth->getLoggedInUser();
        
        $visible_modules = $this->current_module_config->modules->toArray();
        
        if (!isset($visible_modules[$this->_course_name]))
            throw new \DF\Exception\DisplayOnly('The course you specified could not be found.');
        
        $this->_step = $this->_getStep();
        $this->_course_info = $visible_modules[$this->_course_name];
        $this->_step_info = $this->_course_info['steps'][$this->_step - 1];
        
        if (!$this->_step_info)
            throw new \DF\Exception\DisplayOnly('The step you specified is unavailable.');
        
        $num_steps = count($this->_course_info['steps']);
        $is_last_step = ($this->_step == $num_steps);
        $next_step = ($is_last_step) ? NULL : $this->_step + 1;
        
        $this->view->assign(array(
            'step' => $this->_step,
            'next_step' => $next_step,
            'is_last_step' => $is_last_step,
            'num_steps' => $num_steps,
            'step_progress' => round(($this->_step / $num_steps) * 100),
        ));
        
        $this->view->course_key = $this->_course_name;
        $this->view->course = $this->_course_info;
        $this->view->step_info = $this->_step_info;
        
        // Go back if needed.
        if ($this->_getParam('advance') == "back")
            return $this->_reverseStep();
        
        // Determine which view to render.
        switch($this->_step_info['type'])
        {
            // YouTube/Other video source.
            case "video":
                if ($this->_getParam('advance') == "next")
                    return $this->_advanceStep();
                
                $this->render('video');
                return;
            break;
            
            // Quiz.
            case "quiz":
                $quiz = Quiz::fetchByName($this->_step_info['quiz_name']);
                
                if (!($quiz instanceof Quiz))
                    throw new \DF\Exception\DisplayOnly('Quiz not found!');
                
                $form = $quiz->getForm();
                
                if (!empty($_POST) && $form->isValid($_POST))
                {
                    if ($this->auth->isLoggedIn())
                        $quiz->registerCredit();
                    
                    return $this->_advanceStep();
                }
                
                $this->view->form = $form;
                $this->render('quiz');
                return;
            break;
            
            // Custom form.
            case "form":
                $form_name = $this->_step_info['form'];

                $form_config_all = $this->current_module_config->forms->{$form_name}->toArray();
                $form_config = $form_config_all['form'];
                
                $form = new \StuAct\Form($form_config);
                
                if (!empty($_POST) && $form->isValid($_POST))
                {
                    return $this->_advanceStep();
                }
                
                $this->view->form_title = $form_config_all['title'];
                $this->view->form = $form;
                $this->render('form');
                return;
            break;
            
            // Custom template.
            case "template":
                if ($this->_getParam('advance') == "next")
                    return $this->_advanceStep();
                
                $this->render($this->_step_info['template_name']);
                return;
            break;
        }
    }
    
    protected function _getStep()
    {
        $session_key = 'training_'.$this->_course_name.'_step';
        
        if (isset($this->_training_session->{$session_key}))
            return (int)$this->_training_session->{$session_key};
        else
            return 1;
    }
    
    protected function _reverseStep()
    {
        $session_key = 'training_'.$this->_course_name.'_step';
        
        if ($this->_step != 1)
        {
            $this->_training_session->{$session_key} = $this->_step - 1;
        }
        
        $this->redirectFromHere(array('advance' => NULL));
        return;
    }
    
    protected function _advanceStep()
    {
        $session_key = 'training_'.$this->_course_name.'_step';
        $num_steps = count($this->_course_info['steps']);
        
        if ($this->_step == $num_steps)
        {
            unset($this->_training_session->{$session_key});
            
            if ($this->auth->isLoggedIn())
            {
                $user = $this->auth->getLoggedInUser();
                Training::assignCredit($user, $this->_course_name);
            }
            
            $this->render('complete');
            return;
        }
        else
        {
            $this->_training_session->{$session_key} = $this->_step + 1;
            
            $this->redirectFromHere(array('advance' => NULL));
            return;
        }
    }
}