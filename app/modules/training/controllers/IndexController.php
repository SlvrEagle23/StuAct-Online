<?php
/**
 * Training Center - Advisors Area
 */

use \Entity\Training;
use \Entity\Quiz;

class Training_IndexController extends \DF\Controller\Action
{
	public function permissions()
	{
        return true;
	}
    
	public function indexAction()
	{
        if ($this->auth->isLoggedIn())
        {
            $user = \DF\Auth::getLoggedInUser();
            $leader_status = $user->getLeaderStatus();
            
            $this->view->leader_status = $leader_status;
            $this->view->leader_type = $leader_status['type'];
            
            $training_components = Training::getUserTrainingComponents($user);
            $this->view->components = $training_components;
            
            // Categorize the training modules by type.
            $modules = Training::getVisibleTrainingByCategory($user);
            $this->view->modules = $modules;
            
            if (empty($modules['all']) || $leader_status['type'] == "none")
                $show_visitor_center = TRUE;
            else
                $show_visitor_center = FALSE;
        }
        else
        {
            $show_visitor_center = TRUE;
        }
        
        if ($show_visitor_center)
        {
            $all_modules = $this->current_module_config->modules->toArray();
            $modules = array();
            foreach($all_modules as $module_key => $module_info)
            {
                $type = ucfirst($module_info['type']);
                $modules[$type][$module_key] = $module_info;
            }
            $this->view->modules = $modules;
            
            $this->render('visitors');
            return;
        }
        else
        {
            $this->render();
            return;
        }
	}
    
    public function moduleAction()
    {
        $this->redirectFromHere(array(
            'controller' => 'module',
            'action' => 'index',
        ));
    }
}