<?php
/**
 * Advisor Acknowledgement of Expectations
 */

return array(	
	/**
	 * Form Configuration
	 */
    'title'         => 'Acknowledgement of Advisor Expectations',

	'form' => array(
        'method'		=> 'post',
        'groups'      => array(
            
            /** Overall Qualifications **/
            'overall' => array(
                'legend' => 'Overall Expectations',
                'elements' => array(
                    
                    'intro_text' => array('markup', array(
                        'markup' => '<p>The Department of Student Activities appreciates your commitment to serve as an advisor to a recognized student organization(s) at Texas A&M University. The advisor plays an integral role in helping student leaders create an environment within their organizations that is productive, safe, enjoyable, and educational. To this end, we believe it is important to provide clear guidance and support regarding the expected role you will play as you interface with the organization.</p>
                            <p>Please affirm your understanding of the following advisor expectations with regard to advisor qualifications by placing a check in the box underneath the statement.</p>',
                    )),
                    
                    'qualifications_text' => array('markup', array(
                        'label' => 'Qualifications',
                        'markup' => 'I am a Texas A&M University employee as defined by the Texas A&M Human Resources Department, an employee of the Texas A&M Health Science Center in the Bryan/College Station area, or an employee of a Bryan/College Station-based agency in the TAMU System (such as the Texas Engineering Experiment Station, Texas Engineering Extension Service, or Texas Transportation Institute).',
                    )),
                    
                    'advisor_check_1' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                    'employment_text' => array('markup', array(
                        'label' => 'Employment',
                        'markup' => 'I am employed at a level consistent with the categorization of the organization I advise (to find your organization\'s category, please find the organization using the Search tab at the top of the page):
                        <ul>
                            <li>To advise a registered organization, I must be a faculty member, professional or associate staff member, or graduate assistant.</li>
                            <li>To advise an affiliated organization, I must be a full-time university employee (faculty or staff) with the skills and/or training necessary to advise the organization.</li>
                            <li>To advise a sponsored organization, I must be a full-time professional staff member whose job description designates me as the primary advisor to the sponsored organization.</li>
                            <li>For Corps of Cadet advisors only: Under the authority of the Commandant of the Corps of Cadets, military personnel assigned to Texas A&M University through the ROTC program may volunteer to serve as advisors to affiliated organizations within the Corps of Cadets.</li>
                        </ul>',
                    )),
                    
                    'advisor_check_2' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                    'knowledge_text' => array('markup', array(
                        'label' => 'Knowledge of the Organization',
                        'markup' => 'I am familiar with the activities of the organization and have (or am willing to obtain) an appropriate level of experience, resources, or knowledge related to those activities and mission of the organization.',
                    )),
                    
                    'advisor_check_3' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                    'training_text' => array('markup', array(
                        'label' => 'Training',
                        'markup' => 'I will participate in an annually required advisor development seminar. This seminar will be aimed at enhancing my advising skills, introducing me to the resources available for student organizations, and helping me meet the expectations outlined above and below.',
                    )),
                    
                    'advisor_check_4' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                ),
            ),
            
            /** Finances **/
            'finances' => array(
                'legend' => 'Finances',
                'elements' => array(
                    
                    'finances_intro_text' => array('markup', array(
                        'markup' => '<p>Recognized student organizations at Texas A&M University are required to coordinate financial transactions with the Student Organization Finance Center (SOFC). Valuable information regarding fiscal management procedures and guidelines can be found online at <a href="http://sofc.tamu.edu/">sofc.tamu.edu</a>.</p>
                        
                        <p>Please affirm your understanding of the following advisor expectations with regard to organizational finances by placing a check in the box underneath the statement.</p>',
                    )),
                    
                    'sofc_statements_text' => array('markup', array(
                        'label' => 'SOFC Statements',
                        'markup' => 'I will regularly receive statement notices from the SOFC. These statements will be posted on StuAct Online.',
                    )),
                    
                    'advisor_check_5' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                    'finance_awareness_text' => array('markup', array(
                        'label' => 'Awareness of Financial Status',
                        'markup' => 'I will be aware of the organization\'s financial status via a review of these statements and the approval of expenditures relating to the organization.',
                    )),
                    
                    'advisor_check_6' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                ),
            ),
            
            /** Student Rules **/
            'student_rules' => array(
                'legend' => 'Student Rules',
                'elements' => array(
                    
                    'rules_intro_text' => array('markup', array(
                        'markup' => '<p>As an advisor, you should be aware of the University Student Rules and other institutional guidelines that establish expectations for student behavior and activities.</p>
                        
                        <p>Please affirm your understanding of the following advisor expectations with regard to University Student Rules by placing a check in the box underneath the statement.</p>',
                    )),
                    
                    'rules_awareness_text' => array('markup', array(
                        'label' => 'Awareness of Student Rules',
                        'markup' => 'I will ensure that the organization and its officers know where rules and guidelines are published, what the rules are, why they exist, and the consequences for choosing to operate outside their parameters.',
                    )),
                    
                    'advisor_check_7' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                    'reporting_violations_text' => array('markup', array(
                        'label' => 'Reporting Rule Violations',
                        'markup' => 'I will report all rule violations or potential violations to the appropriate university officials.',
                    )),
                    
                    'advisor_check_8' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                    'guiding_documents_text' => array('markup', array(
                        'label' => 'Awareness of Supporting Documents',
                        'markup' => 'I am familiar with the organization\'s constitution and all other governing documents including by-laws, risk management policies, and new member requirements (if applicable).',
                    )),
                    
                    'advisor_check_9' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                ),
            ),
            
            /** Advisor/Officer Relationship **/
            'advisor_officer_relationship' => array(
                'legend' => 'Advisor/Officer Relationship',
                'elements' => array(
                    
                    'relationship_intro' => array('markup', array(
                        'markup' => '<p>The organization-advisor relationship is not a one-way street, in that the student organization and its leaders also have responsibilities. These responsibilities include an appropriate level of communication, providing opportunities for advisor interaction, and a commitment to the success of the organization as a whole.</p>
                        
                        <p>Please affirm your understanding of the following advisor expectations with regard to this advisor agreement by placing a check in the box underneath the statement.</p>',
                    )),
                    
                    'understanding_expectations_text' => array('markup', array(
                        'label' => 'Understanding of Expectations',
                        'markup' => 'I have read and understand the expectations of a student organization advisor and am willing to serve as the student organization advisor in this capacity.',
                    )),
                    
                    'advisor_check_10' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                    'discussion_with_leadership_text' => array('markup', array(
                        'label' => 'Discussion with Organization',
                        'markup' => 'A discussion regarding these expectations has taken place between me and the student organization.',
                    )),
                    
                    'advisor_check_11' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                    'intent_to_withdraw_text' => array('markup', array(
                        'label' => 'Withdrawal as Advisor',
                        'markup' => 'If I feel I am unable to function in the capacity of advisor, I will provide advanced written notification to the Department of Student Activities and the organization\'s officers. If my employment status changes, I will notify the Department of Student Activities immediately.',
                    )),
                    
                    'advisor_check_12' => array('multiCheckbox', array(
                        'multiOptions' => array('confirm' => 'I understand and agree to comply with this statement.'),
                        'required' => true,
                    )),
                    
                ),
            ),
            
            /** Final Section **/
            'final' => array(
                'legend' => 'Intials',
                'elements' => array(
                    
                    'initials_text' => array('markup', array(
                        'markup' => '<p>By entering your initials below, you affirm that you have read and understand these expectations and are willing to serve as a student organization advisor. If you have any questions regarding the information presented in this document, please contact the Department of Student Activities. Additional information may be found throughout this web site. Thank you for your time and effort in support of student involvement at Texas A&M University.</p>',
                    )),
                    
                    'initials' => array('text', array(
                        'label' => 'Your Initials',
                        'required' => true,
                    )),
                    
                    'submit' => array('submit', array(
                        'type'	=> 'submit',
                        'label'	=> 'Complete Advisor Training',
                        'helper' => 'formButton',
                        'class' => 'ui-button positive',
                    )),
                    
                    'ogc_tag' => array('markup', array(
                        'markup' => 'TAMUS-OGC 20081218',
                    )),
                    
                ),
            ),
		),
	),
);