<?php
/**
 * Post-Training Assessment
 */

$score_options = array(
    5   => '5 (One of the best)',
    4   => '4 (Above average)',
    3   => '3 (Average)',
    2   => '2 (Below average)',
    1   => '1 (One of the worst)',
);

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
        'method'		=> 'post',
        
        'groups'      => array(
            
            'module' => array(
                'legend' => 'About This Module',
                'elements' => array(
                    'module_info' => array('markup', array()),
                ),
            ),
            
            'overall' => array(
                'legend' => 'General Questions',
                'description' => 'How would you rate this module with regard to the following items:',
                'elements' => array(
                    
                    'quality_of_content' => array('radio', array(
                        'label' => 'Quality of content',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                    'length' => array('radio', array(
                        'label' => 'Length',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                    'module_format_and_design' => array('radio', array(
                        'label' => 'Module format and design',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                    'comprehensiveness_of_quiz' => array('radio', array(
                        'label' => 'Comprehensiveness of quiz',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                ),
            ),
            
            'officers' => array(
                'legend' => 'Officer Questions',
                'description' => 'How well did this module:',
                'elements' => array(
                    
                    'officer_prepare_to_lead' => array('radio', array(
                        'label' => 'Prepare you to effectively lead',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                    'officer_informative' => array('radio', array(
                        'label' => 'Provide you with information, tools, and resources',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                    'officer_encouragement' => array('radio', array(
                        'label' => 'Encourage you to create a safe and productive environment',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                ),
            ),
            
            'advisors' => array(
                'legend' => 'Advisor Questions',
                'description' => 'How well did this module:',
                'elements' => array(
                    
                    'advisor_role' => array('radio', array(
                        'label' => 'Provide you with important information regarding your role as an advisor',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                    'advisor_outline_expectations' => array('radio', array(
                        'label' => 'Outline advisor expectations',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                    'advisor_resources' => array('radio', array(
                        'label' => 'Identify resources that will assist you in the education of your students',
                        'multiOptions' => $score_options,
                        'separator' => ' ',
                    )),
                    
                ),
            ),
            
            'comments' => array(
                'legend' => 'Additional Comments',
                'elements' => array(
                    
                    'additional_comments' => array('textarea', array(
                        'label' => 'Additional comments about this module',
                        'class' => 'full-width half-height',
                    )),
                    
                ),
            ),
            
            /** Final Section **/
            'submit' => array(
                'elements' => array(
                    'submit' => array('submit', array(
                        'type'	=> 'submit',
                        'label'	=> 'Submit Answers',
                        'helper' => 'formButton',
                        'class' => 'ui-button positive',
                    )),
                ),
            ),
		),
	),
);