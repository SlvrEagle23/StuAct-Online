<?php
/**
 * Training Configuration - Modules
 */

return array(
    
    /**
     * Advisor Training
     */
    
    // Advisor Core
    'advisor_essentials' => array(
        'name' => 'Advisor Essentials',
        'type' => 'advisor',
        'is_required' => TRUE,
        'credits' => 1,
        'description' => 'This training session covers the core details of advising and the expectations of all advisors. All advisors are required to complete this course as part of their training.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/advisor.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/advisor.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_advisor_essentials',
            ),
            array(
                'type' => 'form',
                'form' => 'advisor',
            ),
        ),
    ),
    
    // Advisor Officer Essentials
    'advisor_officer_essentials' => array(
        'name' => 'Officer Essentials for Advisors',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'This module allows advisors to view the information that is shared with student leaders as part of their officer training.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/officer.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/officer.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_officer_essentials',
            ),
        ),
    ),
    
    // Advisor Officer SOFC Training
    'advisor_officer_sofc' => array(
        'name' => 'SOFC for Advisors',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'The module allows advisors to view the information that is shared with student leaders regarding the purpose and use of the Student Organization Finance Center.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/sofc.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/sofc.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_sofc',
            ),
        ),
    ),
    
    // Student Counseling Services
    'advisor_scs' => array(
        'name' => 'The Basics of Student Counseling Services',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'If you\'ve ever wondered how to refer a student to Student Counseling Services? Or about the range of services they provide? This module will cover the basic information need to support, refer, and help you help students.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_type' => 'local',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/scs.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/scs.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'advisor_scs',
            ),
        ),
    ),
    
    // Disability Services
    'advisor_ds' => array(
        'name' => 'Supporting and Including Students with Disabilities',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'This training module includes live interviews with students who answer some of your questions regarding inclusivity, accommodations, and helpful resources every advisor should know about the Department of Disability Services.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_type' => 'local',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/disability.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/disability.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'advisor_ds',
            ),
        ),
    ),
    
    // Hazing
    'advisor_hazing' => array(
        'name' => 'Hazing: What you need to know, and what you need to do!',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'Get an honest look at what hazing is and isn\'t through this training module designed to give you the facts about hazing including clear definitions, statistics, and information on what to do if you have been asked to participate in hazing or have been hazed. ',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_type' => 'local',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/hazing.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/hazing.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_hazing',
            ),
        ),
    ),
    
    // Special Event Insurance
    'advisor_insurance' => array(
        'name' => 'Insurance Made Easy! The How-To Guide for Special Event Insurance',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'Special Events can sometimes require insurance coverage, but knowing the process and all the forms to fill out and turn in could potentially be confusing.  This training offers you the opportunity to find out all the important information about Special Event insurance including forms you\'ll need, the process, and cost of insurance coverage.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/insurance.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/insurance.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_insurance',
            ),
        ),
    ),
    
    // How-To
    'advisor_howto' => array(
        'name' => 'How-To Plan that Program: Common Events on Campus',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'This adventure quiz walks student organization leaders and advisors through programming common events on campus and demonstrates how to access the necessary resources for accomplishing the event successfully.',
        
        'steps' => array(
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_how_to',
            ),
        ),
    ),
    
    // Pre-Event Form
    'advisor_pep' => array(
        'name' => 'Knowing what to do with your PEP: Pre-Event Planning Form Training',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'This module covers all stages of the Pre-Event Planning process including risk management plans, implementation tips, and assessment of the event. You will also learn about the review process done by Student Activities.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/pep.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/pep.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_PEP',
            ),
        ),
    ),

    /*
    // Multicultural Services
    'advisor_dms' => array(
        'name' => 'Welcome to the Department of Multicultural Services',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'Ever wonder what "under-represented populations" really means? Or perhaps you just need more information on programs that discuss multicultural topics.  This training module will give you all you need and more! Meet the staff and learn about all the great organizations and services supported by the Department of Multicultural Services.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/multicultural.mp4',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_dms',
            ),
        ),
    ),
    */

    // Officer Transition for Advisors
    'advisor_transition' => array(
        'name' => 'Officer Transition',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'This module will provide students and advisors with the information and tools necessary for a successful officer transition.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/officer_transition.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/officer_transition.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_transition',
            ),
        ),
    ),

    // Fundraising
    'advisor_fundraising' => array(
        'name' => 'Fundraising',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'This module will focus on some of the ways in which Student Organizations can fundraise.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/fundraising.mp4',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_fundraising',
            ),
        ),
    ),

    // Road Trips and Risk Mgmt
    'advisor_roadtrips' => array(
        'name' => 'Road Trips and Risk Management',
        'type' => 'advisor',
        'credits' => 1,
        'description' => 'This module will focus on risk management practices as they apply to student organizations traveling away from the Bryan/College Station area.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/roadtrips.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/roadtrips.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_roadtrips',
            ),
        ),
    ),
    
    /**
     * Student Leader Training
     */
    
    'officer_essentials' => array(
        'name' => 'Officer Essentials Part 1: Putting the Pieces Together',
        'type' => 'officer',
        'is_required' => 1,
        'credits' => 1,
        'description' => 'The Officer Essentials module will prepare you to effectively lead your student organizations by living the core values of Texas A&M. This training will provide you with information, tools, and resources that will encourage the creation of a safe and productive environment.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/officer.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/officer.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_officer_essentials',
            ),
        ),
    ),
    
    'officer_essentials_sofc' => array(
        'name' => 'Officer Essentials Part 2:  Student Organization Finance Center',
        'type' => 'officer',
        'is_required' => 1,
        'credits' => 1,
        'description' => 'This module will focus specifically on working with the Student Organization Finance Center. An on-campus account at the Student Organization Finance Center is one of the many benefits you receive as a recognized student organization at Texas A&M.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/sofc.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/sofc.pdf',
                
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_sofc',
            ),
        ),
    ),
    
    // Advisor Core
    'officer_advisor_essentials' => array(
        'name' => 'Advisor Essentials for Officers',
        'type' => 'officer',
        'credits' => 1,
        'description' => 'This module allows officers to view the information that is shared with their organization\'s advisor(s) during the advisor training process.  This will help officers better understand the role and responsibilities of their faculty/staff advisor.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/advisor.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/advisor.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_advisor_essentials',
            ),
        ),
    ),
    
    // Hazing
    'officer_hazing' => array(
        'name' => 'Hazing: What you need to know, and what you need to do!',
        'type' => 'officer',
        'credits' => 1,
        'description' => 'Get an honest look at what hazing is and isn\'t through this training module designed to give you the facts about hazing including clear definitions, statistics, and information on what to do if you have been asked to participate in hazing or have been hazed. ',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_type' => 'local',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/hazing.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/hazing.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_hazing',
            ),
        ),
    ),
    
    // Special Event Insurance
    'officer_insurance' => array(
        'name' => 'Insurance Made Easy! The How-To Guide for Special Event Insurance',
        'type' => 'officer',
        'credits' => 1,
        'description' => 'Special Events can sometimes require insurance coverage, but knowing the process and all the forms to fill out and turn in could potentially be confusing.  This training offers you the opportunity to find out all the important information about Special Event insurance including forms you\'ll need, the process, and cost of insurance coverage.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/insurance.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/insurance.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_insurance',
            ),
        ),
    ),
    
    // How-To
    'officer_howto' => array(
        'name' => 'How-To Plan that Program: Common Events on Campus',
        'type' => 'officer',
        'credits' => 1,
        'description' => 'This adventure quiz walks student organization leaders and advisors through programming common events on campus and demonstrates how to access the necessary resources for accomplishing the event successfully.',
        
        'steps' => array(
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_how_to',
            ),
        ),
    ),
    
    // Pre-Event Form
    'officer_pep' => array(
        'name' => 'Knowing what to do with your PEP: Pre-Event Planning Form Training',
        'type' => 'officer',
        'credits' => 1,
        'description' => 'This module covers all stages of the Pre-Event Planning process including risk management plans, implementation tips, and assessment of the event. You will also learn about the review process done by Student Activities.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/pep.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/pep.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_PEP',
            ),
        ),
    ),

    /*
    // Multicultural Services
    'officer_dms' => array(
        'name' => 'Welcome to the Department of Multicultural Services',
        'type' => 'officer',
        'credits' => 1,
        'description' => 'Ever wonder what "under-represented populations" really means? Or perhaps you just need more information on programs that discuss multicultural topics.  This training module will give you all you need and more! Meet the staff and learn about all the great organizations and services supported by the Department of Multicultural Services.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/multicultural.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/multicultural.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_dms',
            ),
        ),
    ),
    */

    // Officer Transition for Advisors
    'officer_transition' => array(
        'name' => 'Officer Transition',
        'type' => 'officer',
        'credits' => 1,
        'description' => 'This module will provide students and advisors with the information and tools necessary for a successful officer transition.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/officer_transition.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/officer_transition.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_transition',
            ),
        ),
    ),

    // Fundraising
    'officer_fundraising' => array(
        'name' => 'Fundraising',
        'type' => 'officer',
        'credits' => 1,
        'description' => 'This module will focus on some of the ways in which Student Organizations can fundraise.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/fundraising.mp4',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_fundraising',
            ),
        ),
    ),

    // Road Trips and Risk Mgmt
    'officer_roadtrips' => array(
        'name' => 'Road Trips and Risk Management',
        'type' => 'officer',
        'credits' => 1,
        'description' => 'This module will focus on risk management practices as they apply to student organizations traveling away from the Bryan/College Station area.',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/roadtrips.mp4',
                'video_alt' => 'http://studentactivities.tamu.edu/uploads/training/roadtrips.pdf',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'training_roadtrips',
            ),
        ),
    ),
    
    // Sport Clubs Batch Training
    'misc_sportclubs' => array(
        'name' => 'Sport Clubs Officer Orientation',
        'type' => 'officer',
        'credits' => 1,
        'is_required' => 1,
        'description' => 'This is the required training for sport club leaders.',
        'batch' => 'sportclubs',
        
        'steps' => array(
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/sportclubs_intro.mp4',
            ),
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/sportclubs_1.mp4',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'recsports_sportclubs_1',
            ),
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/sportclubs_2.mp4',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'recsports_sportclubs_2',
            ),
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/sportclubs_3.mp4',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'recsports_sportclubs_3',
            ),
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/sportclubs_4.mp4',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'recsports_sportclubs_4',
            ),
            array(
                'type' => 'video',
                'video_src' => 'http://studentactivities.tamu.edu/uploads/training/sportclubs_5.mp4',
            ),
            array(
                'type' => 'quiz',
                'quiz_name' => 'recsports_sportclubs_5',
            ),
        ),
    ),
);