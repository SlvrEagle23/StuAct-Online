<?php
/**
 * Of the Year Form Configuration
 */

return array(
	'types'						=> array(
		'organization'		=> 'Organization of the Year',
		'advisor'			=> 'Advisor of the Year',
		'new_advisor'		=> 'New Advisor of the Year',
	),
	
	'staff_contact_name'		=> 'Emily Ancinec',
	'staff_contact_email'		=> 'eancinec@stuact.tamu.edu',
	'staff_contact_phone'		=> '979-862-3295',
	
	'notify'				=> array(
		'eancinec@stuact.tamu.edu',
		'bneece@stuact.tamu.edu',
	),
	
	'deadline'				=> 'March 1, 2013 5:00 pm',
	'ceremony'				=> 'May 1, 2013',
	'parentsweekend'		=> 'April 14, 2013',
);