<?php
$org_settings = \Entity\Organization::loadSettings();

$frequency_options = array(
	'never'			=> 'Never',
	'annual'		=> 'Once per year',
	'onesemester'	=> 'Once per semester',
	'twosemester'	=> 'Twice per semester',
	'threesemester'	=> 'Three or more times per semester',
);

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method' => 'post',
		'groups' => array(

			'about' => array(
				'legend' => 'About This Organization',
				'elements' => array(

					'profile_abbreviation' => array('text', array(
						'label' => 'Abbreviation(s)',
						'class' => 'full-width',
					)),

					'profile_purpose' => array('textarea', array(
						'label' => 'Purpose',
						'class' => 'full-width half-height',
						'required' => true,
					)),

					'info_year_founded' => array('text', array(
						'label' => 'Year Founded',
					)),

					'info_membership_dues' => array('text', array(
						'label' => 'Membership Dues',
						'description' => 'Examples: "$50 per year", "$40 one-time", "No dues"',
					)),

					'info_members_selected' => array('text', array(
						'label' => 'Admits Members',
						'description' => 'Time period when your organization admits members, or "All Year"',
					)),

					'info_meeting_location' => array('text', array(
						'label' => 'Meeting Locations',
					)),
				
				),
			),

			'public_contact' => array(
				'legend' => 'Public Contact Information',
				'elements' => array(

					'profile_contact_name' => array('text', array(
						'label' => 'Public Contact Name',
						'class' => 'half-width',
						'required' => true,
					)),
					'profile_contact_email' => array('text', array(
						'label' => 'Public Contact E-mail Address',
						'class' => 'half-width',
						'validators' => array('EmailAddress'),
						'required' => true,
					)),
					'profile_contact_phone' => array('text', array(
						'label' => 'Public Contact Phone Number',
						'class' => 'half-width',
						'required' => true,
					)),

				),
			),

			'group_contact' => array(
				'legend' => 'Group Contact Information',
				'elements' => array(
					
					'profile_web' => array('text', array(
						'label' => 'Organization Web Site',
						'filters' => array('WebAddress'),
						'class' => 'half-width',
					)),
					'profile_email' => array('text', array(
						'label' => 'Organization E-mail Address',
						'class' => 'half-width',
						'validators' => array('EmailAddress'),
					)),

				),
			),

			'survey_questions' => array(
				'legend' => 'Survey Questions',
				'description' => 'As you complete the annual recognition process, we ask that you help us better serve your organization by answering the question(s) below. These questions are optional and will not be listed on your organization\'s public profile.',
				'elements' => array(

					'info_banking_usage' => array('radio', array(
						'label' => 'Primary Banking Method Used',
						'multiOptions' => $org_settings['banking_usage_opts'],
					)),

					'info_computer_equipment' => array('radio', array(	
						'label' => 'Does your organization have computer equipment on campus (workstations, servers, etc.) that maintains a wired connection to the campus network (i.e. Ethernet cable is plugged in)?',
						'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					)),

				),
			),

			'orgmatch_affiliations' => array(
				'legend' => 'Organization Associations',
				'description' => 'If your organization is associated with any non-academic or academic group on campus, please specify this below.',
				'elements' => array(

					'affiliation_academic' => array('radio', array(
						'belongsTo' => 'orgmatch_profile',
						'label' => 'Academic Department',
						'multiOptions' => array(
							'none'				=> 'General (No College)',
							'interdiscipline'	=> 'Interdisciplinary Degree Programs',
							'aglifesciences'	=> 'College of Agriculture and Life Sciences',
							'architecture'		=> 'College of Architecture',
							'maysschool'		=> 'Mays Business School',
							'edhumandev'		=> 'College of Education and Human Development',
							'engineering'		=> 'Dwight Look College of Engineering',
							'geosciences'		=> 'College of Geosciences',
							'liberalarts'		=> 'College of Liberal Arts',
							'science'			=> 'College of Science',
							'vetmedicine'		=> 'College of Veterinary Medicine and Biomedical Sciences',
							'bushschool'		=> 'George Bush School of Government and Public Service',
							'galveston'			=> 'Texas A&M University at Galveston',
							'other'				=> 'Other',
						),
					)),

					'affiliation_nonacademic' => array('radio', array(
						'belongsTo' => 'orgmatch_profile',
						'label' => 'Non-Academic Department',
						'multiOptions' => array(
							'none'			=> 'None',
							'corps'			=> 'Corps of Cadets',
							'recsports'		=> 'Recreational Sports',
							'greeklifeifc'	=> 'Greek Life - Interfraternity Council',
							'greeklifecpc'	=> 'Greek Life - Collegiate Panhellenic Council',
							'greeklifemgc'	=> 'Greek Life - Multicultural Greek Council',
							'greeklifenphc'	=> 'Greek Life - National Pan-Hellenic Council',
							'greeklife'		=> 'Greek Life - Other',
							'reslife'		=> 'Residence Life',
							'mcs'			=> 'Multicultural Services',
							'studentlife'	=> 'Department of Student Life',
							'sga'			=> 'Student Government Association',
							'other'			=> 'Other',
						),
					)),

				),
			),

			'orgmatch_membership' => array(
				'legend' => 'Membership',
				'elements' => array(
					
					'membership_population' => array('radio', array(
						'label' => 'Approximate membership of the organization',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => array(
							'30'			=> '1-30',
							'50'			=> '31-50',
							'100'			=> '51-100',
							'300'			=> '100-300',
							'1000'			=> '300-1000',
							'5000'			=> '1000+',
						),
					)),
					'membership_policies' => array('multiCheckbox', array(
						'label' => 'Membership policies',
						'belongsTo' => 'orgmatch_profile',
						'description' => 'Please check all that apply to the membership of your organization',
						'multiOptions' => array(
							'undergraduate'	=> 'Our organization accepts undergraduate students as members',
							'graduate'		=> 'Our organization accepts graduate students as members',
							'facultystaff'	=> 'Our organization accepts faculty/staff as members',
							'community'		=> 'Our organization accepts community members as members',
							'gpr'			=> 'Our organization membership will be required to meet specific GPR requirements',
							'otherorg'		=> 'Our organization membership will be contingent on membership in another organization (i.e. Corps of Cadets, etc.)',
							'titleix'		=> 'Our organization has a Title IX exemption (you must provide proof of a Title IX exemption for your organization)',
						),
					)),
					'membership_timerequirement' => array('radio', array(
						'label' => 'Weekly time committment required of organization members',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => array(
							'3'				=> '0-3 Hours',
							'5'				=> '3-5 Hours',
							'10'			=> '5-10 Hours',
							'20'			=> '10-20 Hours',
							'30'			=> '20+ Hours',
						),
					)),
					'membership_intake' => array('radio', array(
						'label' => 'Membership intake process',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => array(
							'open'			=> 'Open',
							'appint'		=> 'Application/Interview',
							'apponly'		=> 'Application Only',
							'pledgeship'	=> 'Pledgeship',
							'invitation'	=> 'Invitation Only',
						),
					)),

				),
			),

			'orgmatch_events' => array(
				'legend' => 'Events',
				'description' => 'How often does your organization participate in the following activities?',
				'elements' => array(
					
					'commserv_frequency' => array('radio', array(
						'label' => 'Community Service Projects',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'fundraising_frequency' => array('radio', array(
						'label' => 'Fundraising Events',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'speakers_frequency' => array('radio', array(
						'label' => 'Speakers/Lectures',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'oncampussocial_frequency' => array('radio', array(
						'label' => 'On-Campus Social Events',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'offcampussocial_frequency' => array('radio', array(
						'label' => 'Off-Campus Social Events',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'conferences_frequency' => array('radio', array(
						'label' => 'Hosting Conferences',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'specialevents_frequency' => array('radio', array(
						'label' => 'Sponsoring Special Events (Concerts, one-time events, etc.)',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'orientation_frequency' => array('radio', array(
						'label' => 'New Member Orientation/Training',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'alcohol_frequency' => array('radio', array(
						'label' => 'Events with Alcohol',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'travel_instate' => array('radio', array(
						'label' => 'In-State Travel',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'travel_outofstate' => array('radio', array(
						'label' => 'Domestic US Travel',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),
					'travel_international' => array('radio', array(
						'label' => 'International Travel',
						'belongsTo' => 'orgmatch_profile',
						'multiOptions' => $frequency_options,
					)),

				),
			),

			'submit_group' => array(
				'elements' => array(
					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Save Changes',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),

		),
	),
);