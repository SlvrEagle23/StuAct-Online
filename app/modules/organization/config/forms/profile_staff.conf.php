<?php
use \DF\Utilities;

$config = \Zend_Registry::get('config');

$general_settings = $config->general->toArray();
$org_settings = \Entity\Organization::loadSettings();

$flag_elements = array();
foreach($org_settings['flags'] as $flag => $flag_name)
{
    $flag_elements['flag_'.$flag] = array('select', array(
        'label' => $flag_name,
        'multiOptions' => $org_settings['flag_codes'],
    ));
}

return array(   
    /**
     * Form Configuration
     */
    'form' => array(
        'method' => 'post',
        'groups' => array(

            'public_info' => array(
                'legend' => 'Public Profile Information',
                'elements' => array(

                    'name' => array('text', array(
                        'label' => 'Organization Name',
                        'class' => 'full-width',
                        'required' => true,
                    )),
                    'profile_mailing_address' => array('textarea', array(
                        'label' => 'Mailing Address',
                        'class' => 'full-width half-height',
                    )),
                
                ),
            ),

            'sofc' => array(
                'legend' => 'SOFC Settings',
                'elements' => array(
                    
                    'account_number' => array('text', array(
                        'label' => 'SOFC Account Number',
                    )),

                    'profile_mail_stop' => array('text', array(
                        'label' => 'SOFC Mail Slot #',
                    )),

                    'sofc_exempt' => array('radio', array(
                        'label' => 'Exempt from SOFC',
                        'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                        'default' => 0,
                    )),

                    'sofc_audit' => array('radio', array(
                        'label' => 'SOFC Audit',
                        'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                        'default' => 0,
                    )),
                
                ),
            ),

            'recognition' => array(
                'legend' => 'Recognition Settings',
                'elements' => array(                    
                    'category' => array('radio', array(
                        'label' => 'Category',
                        'multiOptions' => Utilities::pairs($org_settings['categories']),
                    )),
                    'classification' => array('select', array(
                        'label' => 'Classification',
                        'multiOptions' => Utilities::pairs($org_settings['classifications']),
                    )),
                    'advising_department' => array('select', array(
                        'label' => 'Advising Department',
                        'multiOptions' => Utilities::pairs($org_settings['advising_departments']),
                    )),
                    'batch_id' => array('select', array(
                        'label' => 'Batch Type',
                        'multiOptions' => \Entity\OrganizationBatchGroup::fetchSelect(TRUE),
                    )),
                    'ignore_batch_generator' => array('radio', array(
                        'label' => 'Ignore Batch Generator Settings',
                        'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                    )),
                    'status' => array('radio', array(
                        'label' => 'Recognition Status',
                        'multiOptions' => $org_settings['status_codes'],
                        'required' => true,
                    )),
                    'status_desc' => array('textarea', array(
                        'label' => 'Recognition Status Description',
                        'class' => 'full-width half-height',
                    )),
                    'cycle_override' => array('radio', array(
                        'label' => 'Lock Recognition Status',
                        'description' => 'If this is set to "Yes", StuAct Online will not automatically modify the organization\'s recognition status until the group\'s cycle changes.',
                        'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                    )),
                    'cycle' => array('select', array(
                        'label' => 'Recognition Cycle Month',
                        'multiOptions' => $general_settings['months'],
                    )),
                    'sact_comments' => array('textarea', array(
                        'label' => 'Staff Comments',
                        'class' => 'full-width half-height',
                    )),

                ),
            ),

            'flags' => array(
                'legend' => 'Recognition Flags',
                'description' => 'Any changes to recognition flags that are maintained automatically by StuAct Online will be reset upon the next synchronization.',
                'elements' => $flag_elements,
            ),

            'misc_settings' => array(
                'legend' => 'Miscellaneous Organization Settings',
                'elements' => array(

                    'display_on_search' => array('radio', array(
                        'label' => 'Show in Public Searches',
                        'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                        'default' => 1,
                    )),

                    'roster_num_advisors' => array('select', array(
                        'label' => 'Advisors Visible on Roster',
                        'multiOptions' => Utilities::pairs(array(1, 2, 3, 4, 5)),
                        'default' => 2,
                    )),
                    'roster_num_officers' => array('select', array(
                        'label' => 'Officers Visible on Roster',
                        'multiOptions' => Utilities::pairs(array(10, 15, 20, 25, 30)),
                        'default' => 10,
                    )),
                ),
            ),

            'submit_group' => array(
                'elements' => array(
                    'submit'        => array('submit', array(
                        'type'  => 'submit',
                        'label' => 'Save Changes',
                        'helper' => 'formButton',
                        'class' => 'ui-button',
                    )),
                ),
            ),

        ),
    ),
);