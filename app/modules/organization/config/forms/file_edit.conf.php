<?php
$org_settings = \Entity\Organization::loadSettings();

return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements' 		=> array(

			'title' => array('text', array(
				'label' => 'File Title',
				'class' => 'full-width',
				'required' => true,
			)),

			'description' => array('textarea', array(
				'label' => 'File Description',
				'class' => 'full-width half-height',
			)),

			'type' => array('radio', array(
				'label' => 'File Folder',
				'multiOptions' => $org_settings['file_types'],
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);