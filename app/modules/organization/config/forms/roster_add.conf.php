<?php
return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method' => 'post',
		'elements' => array(

			'user' => array('text', array(
				'label' => 'User UIN or NetID',
				'class' => 'half-width',
				'required' => true,
			)),

			'position' => array('radio', array(
				'label' => 'User Position',
				'required' => true,
				'multiOptions' => array(),
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Add User to Roster',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);