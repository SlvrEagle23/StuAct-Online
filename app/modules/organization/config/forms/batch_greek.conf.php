<?php
return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'enctype'		=> 'multipart/form-data',
		'elements'		=> array(

			'Affiliated' => array('radio', array(
				'label' => 'Is your chapter affiliated with a national or international organization?',
				'multiOptions' => array('No' => 'No', 'Yes' => 'Yes'),
				'required' => true,
			)),

			'ChapterDesignation' => array('text', array(
				'label' => 'If yes, what is your chapter designation or name?',
				'class' => 'full-width',
			)),

			'GoverningCouncil' => array('radio', array(
				'label' => 'Our chapter is a member of the following governing council',
				'multiOptions' => array(
					'IFC'		=> 'IFC (Interfraternity Council)',
					'MGC'		=> 'MGC (Multicultural Greek Council)',
					'CPC'		=> 'CPC (Collegiate Panhellenic Council)',
					'NPHC'		=> 'NPHC (National Pan-Hellenic Council)',
					'AC'		=> 'AC (Affiliate Council)',
				),
				'required' => true,
			)),

			'WebSite' => array('text', array(
				'label' => 'Additional public information about inter/national policies and procedures can be found at the following website',
				'description' => 'Only complete this if you are affiliated with an inter/national organization.',
				'class' => 'half-width',
			)),

			'certification' => array('markup', array(
				'markup' => 'I hereby certify that a current copy of our fraternity/sorority constitution and/or bylaws have been submitted and are on file with the Department of Greek Life.',
			)),

			'CSLName' => array('text', array(
				'label' => 'Chief Student Leader Name',
				'class' => 'half-width',
				'required' => true,
			)),

			'CSLInitials' => array('text', array(
				'label' => 'Chief Student Leader Initials',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Generate and Submit Constitution',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);