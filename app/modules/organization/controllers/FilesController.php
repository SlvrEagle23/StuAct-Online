<?php
use \Entity\Organization;
use \Entity\OrganizationFile;
use \Entity\User;

class Organization_FilesController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return $this->_isAllowed('resources_view');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'files';
	}

	public function indexAction()
	{
		$folder = $this->_getParam('folder');
		$this->view->folder = $folder;

		// List of folders.
		$folders_raw = $this->_settings['file_types'];
		$folders = array();

		$count_query = $this->em->createQuery('SELECT COUNT(f.id) AS num_rows FROM Entity\OrganizationFile f WHERE f.org_id = :org_id AND f.type = :folder GROUP BY f.type')
			->setParameter('org_id', $this->_org_id);

		foreach($folders_raw as $folder_id => $folder_name)
		{
			try	{
				$count = $count_query->setParameter('folder', $folder_id)->getSingleScalarResult();
			} catch(\Exception $e) {
				$count = 0;
			}

			$folders[$folder_id] = array(
				'name'		=> $folder_name,
				'count'		=> (int)$count,
				'active'	=> ($folder == $folder_id),
			);
		}

		// Add file form.
		$add_file_form_config = $this->current_module_config->forms->file_add->form->toArray();
		$add_file_form_config['action'] = $this->view->routeFromHere(array('action' => 'add'));

		$add_file_form = new \DF\Form($add_file_form_config);
		$this->view->add_file_form = $add_file_form;

		// Show folder or "New Files" view.
		$this->view->folders = $folders;

		if ($folder)
		{
			$file_query = $this->em->createQuery('SELECT f FROM Entity\OrganizationFile f WHERE f.org_id = :org_id AND f.type = :folder ORDER BY f.timestamp DESC')
				->setParameter('org_id', $this->_org_id)
				->setParameter('folder', $folder);

			$pager = new \DF\Paginator\Doctrine($file_query, $this->_getParam('page', 1));
			$this->view->files = $pager;
		}
		else
		{
			// Most recent 10 files.
			$latest_files = $this->em->createQuery('SELECT f, u FROM Entity\OrganizationFile f LEFT JOIN f.user u WHERE f.org_id = :org_id ORDER BY f.timestamp DESC')
				->setParameter('org_id', $this->_org_id)
				->setMaxResults(10)
				->getResult();

			$this->view->files = $latest_files;
		}
	}

	public function detailsAction()
	{
		$id = (int)$this->_getParam('file_id');
		$file = OrganizationFile::getRepository()->findOneBy(array('id' => $id, 'org_id' => $this->_org_id));

		if (!$file)
			throw new \DF\Exception\DisplayOnly('File not found!');

		$this->view->file = $file;
	}

	public function addAction()
	{
		$form_config = $this->current_module_config->forms->file_add->form->toArray();
		$form = new \DF\Form($form_config);

		if ($_POST && $form->isValid($_POST))
		{
			$form->getValues();
			
			$uploaded_files = (array)$form->processFiles('organization', $this->_org_id);
			$file_path = $uploaded_files['file'][1];

			// Attach to organization's filesystem.
			$file = new OrganizationFile;
			$file->organization = $this->_organization;
			$file->name = $file_path;
			$file->title = 'File Uploaded '.date('F j, Y g:ia');
			if ($this->_hasParam('folder'))
				$file->type = $this->_getParam('folder');

			$file->save();

			$this->alert('<b>File uploaded!</b><br>Enter the file\'s description below.', 'green');
			$this->redirectFromHere(array('action' => 'edit', 'file_id' => $file->id));
			return;
		}

		$this->view->headTitle('Add File');
		$this->renderForm($form);
	}

	public function editAction()
	{
		$id = (int)$this->_getParam('file_id');
		$file = OrganizationFile::getRepository()->findOneBy(array('id' => $id, 'org_id' => $this->_org_id));

		if (!$file instanceof OrganizationFile)
			throw new \DF\Exception\DisplayOnly('File not found!');

		$form_config = $this->current_module_config->forms->file_edit->form->toArray();
		$form = new \DF\Form($form_config);

		$form->setDefaults($file->toArray(TRUE, TRUE));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$file->fromArray($data);
			$file->save();

			$this->alert('<b>File updated!</b>', 'green');
			$this->redirectFromHere(array('action' => 'index', 'file_id' => NULL));
			return;
		}

		$this->view->headTitle('Edit File');
		$this->renderForm($form);
	}

	public function deleteAction()
	{
		$id = (int)$this->_getParam('file_id');
		$file = OrganizationFile::getRepository()->findOneBy(array('id' => $id, 'org_id' => $this->_org_id));

		if ($file instanceof OrganizationFile)
			$file->delete();

		$this->alert('<b>File deleted!</b>', 'green');
		$this->redirectFromHere(array('action' => 'index', 'file_id' => NULL));
		return;
	}
}