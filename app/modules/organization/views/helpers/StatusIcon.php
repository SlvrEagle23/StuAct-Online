<?php
namespace DF\View\Helper;

class StatusIcon extends \Zend_View_Helper_Abstract
{
	public function statusIcon($status, $size = 'large')
	{
		$settings = \Entity\Organization::loadSettings();
		
		if ($status == ORG_FLAG_DOES_NOT_MEET_REQ)
			$image = 'cross';
		elseif ($status == ORG_FLAG_MEETS_REQ)
			$image = 'tick';
		else
			$image = 'error';
		
		return $this->view->icon(array(
			'type'		=> ($size == "large") ? 'png32' :'png',
			'image'		=> $image,
			'alt'		=> '(Status)',
			'title'		=> $settings['flag_codes'][$status],
		));
	}
}