<?php
namespace DF\View\Helper;

class StatusColor extends \Zend_View_Helper_Abstract
{
	public function statusColor($status)
	{
		$settings = \Entity\Organization::loadSettings();
		
		if ($status == ORG_FLAG_DOES_NOT_MEET_REQ)
			return 'red';
		elseif ($status == ORG_FLAG_MEETS_REQ)
			return 'green';
		else
			return 'yellow';
	}
}