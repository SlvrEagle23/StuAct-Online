<h2>Mail Slot Unassigned</h2>

<?=$this->org->email_header ?>

<p>The organization above has had its mail slot removed from the StuAct Online database and replaced with another mailing address. The group's SOFC mail slot can be reassigned. The number assigned to the organization was <b>#{$org_info.profile_mail_stop}</b>.</p>