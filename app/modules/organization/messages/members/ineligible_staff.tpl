<h2>Ineligible Officer</h2>

<p>An organization officer was made ineligible to serve in their role by a recent update.</p>

<ul>
	<li><b>Officer Name:</b> {$this_user_info.firstname} {$this_user_info.lastname} (<a href="{$config_data.url_base}/account/{$this_user_info.username}/profile/personal/view">{$this_user_info.username}</a>)</li>
</ul>

<p>This user is involved in the following organizations in the following positions:</p>

<ul>
{foreach from=$gpr_requiring_positions item="position"}
	<li><b>{$position.org.org_name} (<a href="{$position.org.org_home_url}">{$position.org.org_id}</a>)</b>: {$position.position_text}</li>
{/foreach}
</ul>