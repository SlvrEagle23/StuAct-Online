<?php
/**
 * SLI Journal entries
 */

use \Entity\Sli;

class Sli_IndexController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('is logged in');
	}
	
	public function indexAction()
	{
		$user = \DF\Auth::getInstance()->getLoggedInUser();
        
        $entries = $this->em->createQuery('SELECT s FROM \Entity\Sli s WHERE s.user_id = :user_id ORDER BY s.entry_timestamp DESC')
            ->setParameter('user_id', $user->id)
            ->getArrayResult();
		
		foreach($entries as &$entry)
		{
			$entry['entry_data'] = \Zend_Json::decode($entry['entry_data']);
		}
		$this->view->entries = $entries;
	}
	
	public function submitAction()
	{
		$user = \DF\Auth::getInstance()->getLoggedInUser();
		
		if (isset($_REQUEST['entry']))
		{
			$sli = new Sli();
			$sli->user = $user;
			$sli->entry_timestamp = time();
			$sli->entry_data = \Zend_Json::encode($_REQUEST['entry']);
			$sli->save();
			
			$this->alert('Journal entry submitted!');
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}
	}
}