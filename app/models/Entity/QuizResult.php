<?php
namespace Entity;


/**
 * QuizResult
 *
 * @Table(name="quiz_result")
 * @Entity
 * @HasLifecycleCallbacks
 */
class QuizResult extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime("now");
    }
    
    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime('NOW');
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="quiz_id", type="integer") */
    protected $quiz_id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;
    
    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Entity\Quiz", inversedBy="results")
     * @JoinColumn(name="quiz_id", referencedColumnName="id")
     */
    protected $quiz;

    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;    
}