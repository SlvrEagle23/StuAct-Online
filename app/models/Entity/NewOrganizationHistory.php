<?php
namespace Entity;


/**
 * NewOrganizationHistory
 *
 * @Table(name="new_organization_history")
 * @Entity
 */
class NewOrganizationHistory extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="log_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $log_id;

    /** @Column(name="org_id", type="integer") */
    protected $org_id;

    /** @Column(name="log_timestamp", type="integer", length=4) */
    protected $log_timestamp;

    /** @Column(name="log_message", type="text") */
    protected $log_message;
}