<?php
namespace Entity;

/**
 * @Table(name="maroonout_tracking")
 * @Entity
 */
class ClassCenterTracking extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->created_at = new \DateTime('NOW');
        $this->is_pending_completion = true;
    }

    /**
     * @Column(name="tracking_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="box_id", type="integer", nullable=true) */
    protected $box_id;

    /** @Column(name="tracking_type", type="smallint", nullable=true) */
    protected $type;

    /** @Column(name="timestamp", type="unixdatetime", nullable=true) */
    protected $created_at;

    /** @Column(name="quantity_in", type="integer", length=2, nullable=true) */
    protected $quantity_out;

    /** @Column(name="quantity_out", type="integer", length=2, nullable=true) */
    protected $quantity_in;

    /** @Column(name="quantity_damaged", type="integer", length=2, nullable=true) */
    protected $quantity_damaged;

    /** @Column(name="tracking_note", type="text", nullable=true) */
    protected $tracking_note;

    /** @Column(name="is_inaccurate", type="boolean", nullable=true) */
    protected $is_inaccurate;

    /** @Column(name="is_pending_completion", type="boolean", nullable=true) */
    protected $is_pending_completion;

    public function getTotalSold()
    {
        return ($this->quantity_out - $this->quantity_in) - $this->quantity_damaged;
    }
    public function getTypeName()
    {
        $settings = ClassCenterBox::loadSettings();
        return $settings['types'][$this->type];
    }
    public function getColor()
    {
        if ($this->isNormal())
            return ($this->isActive()) ? 'zebra' : 'red';
        else
            return 'yellow';
    }

    public function canEdit()
    {
        $entry_date = $this->created_at->format('Y-m-d');
        $current_date = date('Y-m-d');
        return ($entry_date == $current_date);
    }
    
    public function isNormal()
    {
        ClassCenterBox::loadSettings();
        return ($this->type == MAROONOUT_TYPE_NORMAL);
    }
    public function isActive()
    {
        return (!$this->is_inaccurate && !$this->is_deleted);
    }

    /**
     * @ManyToOne(targetEntity="ClassCenterBox", inversedBy="logs")
     * @JoinColumn(name="box_id", referencedColumnName="id")
     */
    protected $box;
}