<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_cirt_sessions")
 * @Entity
 * @HasLifecycleCallbacks
 */
class TravelFormSession extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /** @Column(name="form_id", type="integer", length=4) */
    protected $form_id;

    /** @Column(name="outbound_departure", type="integer", nullable=true) */
    protected $outbound_departure;

    /** @Column(name="outbound_arrival", type="integer", nullable=true) */
    protected $outbound_arrival;

    /** @Column(name="inbound_departure", type="integer", nullable=true) */
    protected $inbound_departure;

    /** @Column(name="inbound_arrival", type="integer", nullable=true) */
    protected $inbound_arrival;

    public function getStartDate()
    {
        return $this->outbound_departure;
    }
    
    public function getEndDate()
    {
        return $this->inbound_arrival;
    }

    /**
     * @ManyToOne(targetEntity="Entity\TravelForm")
     * @JoinColumn(name="form_id", referencedColumnName="app_id")
     */
    protected $form;
}