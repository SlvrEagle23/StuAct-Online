<?php
namespace Entity;


/**
 * @Table(name="organization_officers", indexes={
 *   @index(name="search_idx", columns={"off_position"})
 * })
 * @Entity
 * @HasLifecycleCallbacks
 */
class OrganizationOfficer extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->timestamp = time();
    }

    /** @PostPersist */
    public function added()
    {
        $em = self::getEntityManager();
        $current_user = \DF\Auth::getLoggedInUser();

        // Write the officer addition to the log.
        Log::post(array(
            'user'      => $this->user,
            'organization' => $this->organization,
            'message'   => 'Added to the roster as "'.$this->getName().'" by '.$current_user->firstname.' '.$current_user->lastname.'.',
            'type'      => 'officers',
        ));

        // Create new archive entry.
        $archive = new OrganizationOfficerArchive;
        $archive->fromArray(array(
            'organization' => $this->organization,
            'position'  => $this->position,
            'uin'       => $this->user->uin,
            'name'      => $this->user->firstname.' '.$this->user->lastname,
            'email'     => $this->user->email,
        ));
        $archive->save();

        // Delete all duplicate roster positions.
        $existing_positions = $em->createQuery('SELECT oo FROM Entity\OrganizationOfficer oo WHERE oo.organization = :org AND oo.id != :id AND (oo.user = :user OR oo.position = :position)')
            ->setParameter('org', $this->organization)
            ->setParameter('user', $this->user)
            ->setParameter('position', $this->position)
            ->setParameter('id', $this->id)
            ->execute();

        if (count($existing_positions) > 0)
        {
            foreach($existing_positions as $oo)
                $oo->delete();
        }

        // Delete all other requests for the same position or by the same user.
        $delete_requests = $em->createQuery('DELETE FROM Entity\OrganizationOfficerRequest oor WHERE oor.organization = :org AND (oor.user = :user OR oor.position = :position)')
            ->setParameter('org', $this->organization)
            ->setParameter('user', $this->user)
            ->setParameter('position', $this->position)
            ->execute();

        // Clear all permissions for this position.
        $delete_privs = $em->createQuery('DELETE FROM Entity\OrganizationPermission op WHERE op.organization = :org AND op.position = :position')
            ->setParameter('org', $this->organization)
            ->setParameter('position', $this->position)
            ->execute();

        // Notify the new officer of their status.
        $additional_info = array(
            'position'      => $position,
            'position_text' => $this->getPositionText($position),
            'is_student'    => (in_array($position, $organization_officer_groups['gpr'])),
            'user'          => $current_user->info['firstname'].' '.$current_user->info['lastname'].' ('.$current_user->username.')',
        );
        $this->organization->notify('member_added', $additional_info);
    }

    /** @PostRemove */
    public function removed()
    {
        $em = self::getEntityManager();
        $current_user = \DF\Auth::getLoggedInUser();
        
        // Update the end-of-tenure timestamp in the officers archive.
        $archive_record = $em->createQuery('SELECT ooa FROM Entity\OrganizationOfficerArchive ooa WHERE ooa.org_id = :org_id AND ooa.uin = :uin AND ooa.date_end = 0')
            ->setMaxResults(1)
            ->setParameter('org_id', $this->org_id)
            ->setParameter('uin', $this->user->uin)
            ->getOneOrNullResult();

        if ($archive_record)
        {
            $archive_record->date_end = time();
            $archive_record->save();
        }

        // Post to log.
        Log::post(array(
            'user'      => $this->user,
            'organization' => $this->organization,
            'message'   => 'Removed from the roster by '.$current_user->firstname.' '.$current_user->lastname.'.',
            'type'      => 'officers',
        ));

        // Notify the new officer of their status.
        $additional_info = array(
            'position'      => $this->position,
            'position_text' => $this->name,
            'user'          => ($current_user instanceof User) ? $current_user->firstname.' '.$current_user->lastname : 'StuAct Online',
        );
        $this->organization->notify('member_removed', $additional_info);
    }

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="org_id", type="integer") */
    protected $org_id;

    /** @Column(name="off_position", type="integer", length=5) */
    protected $position;

    public function getName()
    {
        $settings = Organization::loadSettings();
        return $settings['officer_codes'][$this->position];
    }

    /** @Column(name="off_timestamp", type="integer", length=4) */
    protected $timestamp;

    /**
     * @ManyToOne(targetEntity="Entity\User", inversedBy="officer_positions")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="Entity\Organization", inversedBy="officer_positions")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;

    public function getGroups()
    {
        $settings = Organization::loadSettings();
        return (array)$settings['officer_group_membership'][$this->position];
    }

    public function isInGroup($group_name)
    {
        $groups = $this->getGroups();
        return (in_array($group_name, $groups));
    }

    public function isRenewed()
    {
        $settings = Organization::loadSettings();

        // Non-transition groups and non-student leaders don't complete this process.
        if ($this->status != ORG_STATUS_IN_TRANSITION || !$this->isInGroup('student_officers'))
            return TRUE;

        $start_of_cycle = (int)$this->_org->date_status_changed;
        $first_notification = $start_of_cycle - $settings['prerec_first_notification'];

        return ($this->timestamp > $first_notification);
    }

    public function isTrained()
    {
        if (!$this->isInGroup('leaders'))
            return true;
        else
            return ($this->user->flag_training != 0);
    }

    public function isEligible()
    {
        // Always return positive for non-GPR-based positions.
        if (!$this->isInGroup('gpr'))
            return true;

        // Check for exemption.
        try
        {
            $em = self::getEntityManager();
            $exemption = $em->createQuery('SELECT ge FROM Entity\GprExemption ge WHERE ge.org_id = :org_id AND (ge.uin = :uin OR ge.user_id = :user_id)')
                ->setParameter('org_id', $this->org_id)
                ->setParameter('uin', $this->user->uin)
                ->setParameter('user_id', $this->user_id)
                ->setMaxResults(1)
                ->getSingleResult();

            if ($exemption instanceof GprExemption)
                return true;
        }
        catch(\Exception $e) {}

        // Run standard grade check.
        return \StuAct\Grades::checkGrade($this->user);
    }

    public function canModifyPosition($position_num)
    {
        if ($position_num instanceof self)
            $position_num = $position_num->position;

        $org_acl = \StuAct\Acl\Organization::getInstance();
        return $org_acl->canModifyPosition($position_num, $this->organization, $this->user);
    }

    public function getPermissions()
    {
        static $priv_list;

        if ($priv_list === NULL)
            $priv_list = array();

        if (!isset($priv_list[$this->id]))
        {
            $settings = Organization::loadSettings();
            $privs = array('granted' => array(), 'grantable' => array());

            // Set up list of "grantable" permissions.
            foreach($settings['permissions']['modifiable_types'] as $permission_key)
            {
                $privs['grantable'][$permission_key] = $settings['permissions']['types'][$permission_key];
            }

            // Add default permissions.        
            $position_groups = (array)$settings['officer_group_membership'][$this->position];
            $position_groups = array_reverse($position_groups);

            foreach($position_groups as $group)
            {
                if (isset($settings['permissions'][$group]))
                {
                    foreach($settings['permissions'][$group] as $permission_key)
                    {
                        $privs['granted'][$permission_key] = array(
                            'name'  => $settings['permissions']['types'][$permission_key],
                            'type'  => 'default',
                            'anonymous' => ($group == "anonymous"),
                        );
                        unset($privs['grantable'][$permission_key]);
                    }
                }
            }

            // Add special permissions.
            $special = OrganizationPermission::getRepository()->findby(array('org_id' => $this->org_id, 'position' => $this->position));

            if ($special)
            {
                foreach($special as $priv)
                {
                    $permission_key = $priv->permission;

                    $privs['granted'][$permission_key] = array(
                        'name'  => $settings['permissions']['types'][$permission_key],
                        'type'  => 'custom',
                        'anonymous' => false,
                    );
                    unset($privs['grantable'][$permission_key]);
                }
            }

            $priv_list[$this->id] = $privs;
        }

        return $priv_list[$this->id];
    }

    public function hasPermission($priv_name)
    {
        $privs = $this->getPermissions();
        return isset($privs['granted'][$priv_name]);
    }

    /**
     * Static Functions
     */
    
    public static function getUserPosition(User $user = NULL, Organization $org)
    {
        if ($user === NULL)
            $user = \DF\Auth::getInstance()->getLoggedInUser();

        return self::getRepository()->findOneBy(array(
            'user'          => $user,
            'organization'  => $org,
        ));
    }
}