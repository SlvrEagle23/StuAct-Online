<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_camps_session")
 * @Entity
 */
class CampsFormSession extends \DF\Doctrine\Entity
{   
    public function __construct()
    {}
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="form_id", type="integer") */
    protected $form_id;

    /** @Column(name="start_date", type="date") */
    protected $start_date;

    /** @Column(name="end_date", type="date") */
    protected $end_date;

    public function getNumDays()
    {
        $start = $this->start_date->getTimestamp();
        $end = $this->end_date->getTimestamp();
        return round(($end - $start) / 86400);
    }

    /** @Column(name="type", type="string", length=5, nullable=true) */
    protected $type;

    /** @Column(name="num_participants", type="smallint", nullable=true) */
    protected $num_participants;

    /** @Column(name="num_counselors", type="smallint", nullable=true) */
    protected $num_counselors;

    /** @Column(name="roster", type="json", nullable=true) */
    protected $roster;

    /**
     * @ManyToOne(targetEntity="CampsForm", inversedBy="sessions")
     * @JoinColumns({
     *   @JoinColumn(name="form_id", referencedColumnName="app_id", onDelete="CASCADE")
     * })
     */
    protected $form;
}