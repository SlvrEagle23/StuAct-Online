<?php
namespace Entity;

/**
 * PreEventFormLog
 *
 * @Table(name="forms_pep_log")
 * @Entity
 */
class PreEventFormLog extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->timestamp = time();
    }

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="app_id", type="integer", nullable=true) */
    protected $app_id;

    /** @Column(name="timestamp", type="integer", nullable=true) */
    protected $timestamp;

    /** @Column(name="note", type="text", nullable=true) */
    protected $note;

    /**
     * @ManyToOne(targetEntity="Entity\PreEventForm", inversedBy="logs")
     * @JoinColumn(name="app_id", referencedColumnName="app_id")
     */
    protected $form;
}