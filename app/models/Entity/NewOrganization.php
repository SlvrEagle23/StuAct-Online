<?php
namespace Entity;

/**
 * @Table(name="new_organizations")
 * @Entity
 */
class NewOrganization extends \DF\Doctrine\Entity
{
    const PHASE_INITIAL = 0;
    const PHASE_AWAITING_ASSIGNMENT = 1;
    const PHASE_AWAITING_REVIEW = 2;
    const PHASE_ADVISOR_NEEDED = 7;
    const PHASE_ADVISOR_PENDING = 8;
    const PHASE_DEPT_SOFC = 3;
    const PHASE_ARCHIVED = 10;

    /**
     * @Column(name="org_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="app_id_hash", type="string", length=255, nullable=true) */
    protected $access_code;

    /** @Column(name="app_phase", type="integer", length=1, nullable=true) */
    protected $phase;

    /** @Column(name="user_id", type="integer", length=4, nullable=true) */
    protected $user_id;

    /** @Column(name="advisor_user_id", type="integer", length=4, nullable=true) */
    protected $advisor_user_id;

    /** @Column(name="approval_phase", type="integer", length=2, nullable=true) */
    protected $approval_phase;

    /** @Column(name="claimant_user_id", type="integer", length=4, nullable=true) */
    protected $claimant_user_id;

    /** @Column(name="org_name", type="string", length=255, nullable=true) */
    protected $org_name;

    /** @Column(name="org_category", type="string", length=255, nullable=true) */
    protected $org_category;

    /** @Column(name="org_classification", type="string", length=255, nullable=true) */
    protected $org_classification;

    /** @Column(name="org_abbr", type="string", length=255, nullable=true) */
    protected $org_abbr;

    /** @Column(name="org_cycle", type="integer", length=2, nullable=true) */
    protected $org_cycle;

    /** @Column(name="org_affiliation_national", type="string", length=255, nullable=true) */
    protected $org_affiliation_national;

    /** @Column(name="org_affiliation_regional", type="string", length=255, nullable=true) */
    protected $org_affiliation_regional;

    /** @Column(name="org_affiliation_academic", type="string", length=255, nullable=true) */
    protected $org_affiliation_academic;

    /** @Column(name="org_affiliation_nonacademic", type="string", length=255, nullable=true) */
    protected $org_affiliation_nonacademic;

    /** @Column(name="org_purpose", type="text", nullable=true) */
    protected $org_purpose;

    /** @Column(name="org_mission_dependence", type="integer", length=2, nullable=true) */
    protected $org_mission_dependence;

    /** @Column(name="org_mission_dependence_explanation", type="text", nullable=true) */
    protected $org_mission_dependence_explanation;

    /** @Column(name="org_uniqueness", type="text", nullable=true) */
    protected $org_uniqueness;

    /** @Column(name="org_other_activities", type="text", nullable=true) */
    protected $org_other_activities;

    /** @Column(name="org_activity_scope", type="integer", length=2, nullable=true) */
    protected $org_activity_scope;

    /** @Column(name="org_activity_scope_explanation", type="text", nullable=true) */
    protected $org_activity_scope_explanation;

    /** @Column(name="org_risk_level", type="integer", length=2, nullable=true) */
    protected $org_risk_level;

    /** @Column(name="org_risk_level_explanation", type="text", nullable=true) */
    protected $org_risk_level_explanation;

    /** @Column(name="org_advisor_special", type="text", nullable=true) */
    protected $org_advisor_special;

    /** @Column(name="profile_mail_stop", type="string", length=20, nullable=true) */
    protected $profile_mail_stop;

    /** @Column(name="official_org_id", type="integer", nullable=true) */
    protected $official_org_id;

    /** @Column(name="initials", type="string", length=255, nullable=true) */
    protected $initials;

    /** @Column(name="orgmatch_profile", type="text", nullable=true) */
    protected $orgmatch_profile;

    /** @Column(name="enhanced_expectations", type="integer", length=1, nullable=true) */
    protected $enhanced_expectations;

    /** @Column(name="enhanced_desc", type="text", nullable=true) */
    protected $enhanced_desc;

    /** @Column(name="sact_comments", type="text", nullable=true) */
    protected $sact_comments;

    /** @Column(name="date_submitted", type="integer", length=4, nullable=true) */
    protected $date_submitted;

    /** @Column(name="is_duplicate", type="integer", length=1, nullable=true) */
    protected $is_duplicate;

    /** @Column(name="duplicate_org_id", type="integer", length=4, nullable=true) */
    protected $duplicate_org_id;

    /** @Column(name="is_archived", type="integer", length=1, nullable=true) */
    protected $is_archived;
}