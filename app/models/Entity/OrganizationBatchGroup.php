<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * OrganizationBatchGroup
 *
 * @Table(name="organization_batch_groups")
 * @Entity
 */
class OrganizationBatchGroup extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->managers = new ArrayCollection;
		$this->organizations = new ArrayCollection;
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="short_name", type="string", length=50, nullable=true) */
    protected $short_name;

    /** @Column(name="use_generator", type="integer", length=1, nullable=true) */
    protected $use_generator;

    /**
     * @ManyToMany(targetEntity="Entity\User", inversedBy="batchgroups", cascade={"remove"})
     * @JoinTable(name="organization_batch_group_managers",
     *      joinColumns={@JoinColumn(name="org_batch_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="user_id")}
     * )
     */
    protected $managers;

    /**
     * @OneToMany(targetEntity="Entity\Organization", mappedBy="batch_group")
     * @OrderBy({"name" = "ASC"})
     */
    protected $organizations;

    /**
     * Static Functions
     */
    
    public static function fetchSelect($add_blank = FALSE)
    {
        $options = array();

        if ($add_blank)
            $options[''] = 'No Batch Group';
        
        $records = self::fetchArray('name');

        foreach($records as $record)
        {
            $options[$record['id']] = $record['name'];
        }

        return $options;
    }
}