<?php
namespace Entity;


/**
 * GprAppealForm
 *
 * @Table(name="forms_gpr_appeal", indexes={@index(name="search_idx", columns={"app_id_hash"})})
 * @Entity
 */
class GprAppealForm extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
		$this->organization = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="app_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $app_id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="org_id", type="integer", nullable=true) */
    protected $org_id;

    /** @Column(name="app_id_hash", type="string", length=20, nullable=true) */
    protected $app_id_hash;

    /** @Column(name="app_data", type="text", nullable=true) */
    protected $app_data;

    /** @Column(name="app_phase", type="integer", length=1, nullable=true) */
    protected $app_phase;

    /** @Column(name="app_review_advisor", type="integer", length=1, nullable=true) */
    protected $app_review_advisor;

    /** @Column(name="app_review_csl", type="integer", length=1, nullable=true) */
    protected $app_review_csl;

    /** @Column(name="app_review_final", type="integer", length=1, nullable=true) */
    protected $app_review_final;

    /** @Column(name="app_timestamp", type="integer", length=4, nullable=true) */
    protected $app_timestamp;

    /** @Column(name="app_timestamp_updated", type="integer", length=4, nullable=true) */
    protected $app_timestamp_updated;
    
    /** @Column(name="app_routing_type", type="string", length=20, nullable=true) */
    protected $app_routing_type;

    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;
}