<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_camps", indexes={
 *   @index(name="search_idx", columns={"app_id_hash"})
 * })
 * @Entity
 * @HasLifecycleCallbacks
 */
class CampsForm extends \DF\Doctrine\Entity
{
    const PHASE_INCOMPLETE = 0;
    const PHASE_DECLINED = 1;
    const PHASE_NEEDS_MODIFICATION = 5;
    const PHASE_SECONDARY_REVIEW = 10;
    const PHASE_DEPT_REVIEW = 30;
    const PHASE_RISK_INITIAL = 50;
    const PHASE_DEAN_REVIEW = 70;
    const PHASE_APPROVED_NEEDS_ROSTER = 90;
    const PHASE_APPROVED_UNPAID = 95;
    const PHASE_COMPLETE = 100;
    const PHASE_ARCHIVED = 2;

    const VERSION_LEGACY = 0;
    const VERSION_DFFORM = 1;

    public function __construct()
    {
        $this->version = self::VERSION_DFFORM;
        $this->phase = self::PHASE_INCOMPLETE;
        $this->time_created = time();
        $this->time_notified = 0;

        $this->logs = new ArrayCollection;
        $this->incidents = new ArrayCollection;
    }

    /** @PostPersist */
    public function initialize()
    {
        // Generate access code.
        $this->access_code = 'SA-'.substr(strtoupper(sha1('CampsForm_'.$this->id)), 0, 5).'-'.str_pad($this->id, 5, '0', STR_PAD_LEFT);
        $this->save();
    }

    /** @PostUpdate */
    public function updated()
    {
        $this->updateCampsDirectory();
    }

    public function updateCampsDirectory()
    {
        $phases_to_use = array(
            self::PHASE_DEAN_REVIEW, 
            self::PHASE_APPROVED_NEEDS_ROSTER, 
            self::PHASE_APPROVED_UNPAID, 
            self::PHASE_COMPLETE, 
            self::PHASE_ARCHIVED
        );

        if (in_array($this->phase, $phases_to_use) && $this->index_starttime)
        {
            if ($this->version == self::VERSION_LEGACY)
            {
                if (count($this->data['Sessions']) > 0)
                {
                    foreach($this->data['Sessions'] as $session)
                    {
                        if ($session['End_date'] && $session['Start_date'])
                        {
                            $startdate = strtotime($session['Start_date'].' 11:59:59');
                            $enddate = strtotime($session['End_date'].' 00:00:00');
                            $days = ($enddate - $startdate) / 86400;

                            if ($enddate && $startdate && $days < 60)
                            {
                                $record = new Camp;
                                $record->form_id = $this->access_code;
                                $record->name = $this->index_name;
                                $record->date_start = $startdate;
                                $record->date_end = $enddate;
                                $record->contact_name = $this->data['Contact_Info']['Contact__Name'];
                                $record->contact_email = $this->data['General_Info']['Email_address'];
                                $record->save();
                            }
                        }
                    }
                }
            }
            else
            {
                if (count($this->sessions) > 0)
                {
                    foreach($this->sessions as $session)
                    {
                        $startdate = $session->startdate->getTimestamp();
                        $enddate = $session->enddate->getTimestamp();
                        $days = ($enddate - $startdate) / 86400;

                        if ($startdate && $enddate && $days < 60)
                        {
                            $record = new Camp;
                            $record->form_id = $this->access_code;
                            $record->name = $this->index_name;
                            $record->date_start = $startdate;
                            $record->date_end = $enddate;
                            $record->contact_name = $this->program->cpm_sponsor_name;
                            $record->contact_email = $this->program->cpm_sponsor_email;
                            $record->contact_web = $this->program->web;
                            $record->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * @Column(name="app_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="app_id_hash", type="string", length=50, nullable=true) */
    protected $access_code;

    public function getPhaseCode($phase = NULL)
    {
        if ($phase === null)
            $phase = $this->phase;

        return self::getPhaseHash($this->access_code, $phase);
    }

    /** @Column(name="app_version_id", type="smallint", nullable=true) */
    protected $version;

    public function getVersion()
    {
        if ($this->version === null)
            return self::VERSION_LEGACY;
        else
            return $this->version;
    }

    /** @Column(name="app_user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="app_program_id", type="integer", nullable=true) */
    protected $program_id;

    /** @Column(name="app_secondary_user_id", type="integer", length=4, nullable=true) */
    protected $secondary_user_id;
    
    /** @Column(name="app_org_id", type="integer", nullable=true) */
    protected $org_id;

    /** @Column(name="app_data", type="json", nullable=true) */
    protected $data;

    public function setData($data)
    {
        $this->data = $data;
        $this->reloadIndex();
    }

    public function replaceData($section, $data)
    {
        return $this->_updateSection($section, $data, FALSE);
    }
    public function appendData($section, $data)
    {
        return $this->_updateSection($section, $data, TRUE);
    }
    
    protected function _updateSection($section, $data, $append = TRUE)
    {
        $all_data = (array)$this->data;
        if (!$append || !isset($all_data[$section]))
            $all_data[$section] = array();

        if (!empty($data))
            $all_data[$section] = array_merge((array)$all_data[$section], (array)$data);

        $this->setData($all_data);

        return $this;
    }

    public function reloadIndex()
    {
        if ($this->getVersion() == self::VERSION_LEGACY)
        {
            $this->index_name = trim(substr($this->data['General_Info']['Program_name'], 0, 255));

            $latest_session = 0;
            $earliest_session = 0;

            if (count($this->data['Sessions']) > 0)
            {
                foreach($this->data['Sessions'] as $session)
                {
                    if ($session['End_date'])
                    {
                        $enddate = strtotime($session['End_date'].' 00:00:00');
                        if ($enddate && ($enddate > $latest_session || $latest_session == 0))
                            $latest_session = $enddate;
                    }
                    if ($session['Start_date'])
                    {
                        $startdate = strtotime($session['Start_date'].' 00:00:00');
                        if ($startdate && ($startdate < $earliest_session || $earliest_session == 0))
                            $earliest_session = $startdate;
                    }
                }
            }

            $this->index_starttime = $earliest_session;
            $this->index_endtime = $latest_session;
        }
        else
        {
            $this->index_name = $this->data['Logistics']['name'];

            $latest_session = 0;
            $earliest_session = 0;

            if (count($this->sessions) > 0)
            {
                foreach($this->sessions as $session)
                {
                    $enddate = $session->enddate->getTimestamp();
                    if ($enddate && ($enddate > $latest_session || $latest_session == 0))
                        $latest_session = $enddate;

                    $startdate = $session->startdate->getTimestamp();
                    if ($startdate && ($startdate < $earliest_session || $earliest_session == 0))
                        $earliest_session = $startdate;
                }
            }

            if ($earliest_session == 0)
                $earlest_session = $this->data['Logistics']['start_of_first_session'];

            if ($latest_session == 0)
                $latest_session = $this->data['Logistics']['end_of_last_session'];

            $this->index_starttime = $earliest_session;
            $this->index_endtime = $latest_session;
        }
    }

    /** @Column(name="app_admin_data", type="json", nullable=true) */
    protected $admin_data;
    
    protected function setAdminData($data, $append = TRUE)
    {
        $all_data = (array)$this->admin_data;

        if (!$append)
            $all_data = array();

        $data = array_filter($data, function($element) { return !empty($element); });
        if (!empty($data))
            $all_data = array_merge((array)$all_data, (array)$data);

        $this->admin_data = $all_data;
        return $this;
    }

    /** @Column(name="app_timestamp", type="integer", nullable=true) */
    protected $timestamp;

    /** @Column(name="app_time_created", type="integer", nullable=true) */
    protected $time_created;

    /** @Column(name="app_time_notified", type="integer", nullable=true) */
    protected $time_notified;

    /** @Column(name="app_is_completed", type="boolean", nullable=true) */
    protected $is_completed;

    /** @Column(name="app_phase", type="smallint", nullable=true) */
    protected $phase;

    public function getPhaseName()
    {
        $phases = self::getPhaseNames();
        return $phases[$this->phase];
    }

    public function setPhase($phase, $reason = NULL)
    {
        $old_phase = $this->phase;
        $this->phase = $phase;

        if ($old_phase != $phase)
        {
            $phase_names = self::getPhaseNames();
            $old_phase_name = $phase_names[$old_phase];
            $new_phase_name = $phase_names[$phase];

            $this->notify($phase);

            $log_text = 'Approval phase changed from "'.$old_phase_name.'" to "'.$new_phase_name.'".';
            if ($reason)
                $log_text .= ' Reason provided: '.$reason;

            $this->addLogEntry($log_text);
        }
    }

    /** @Column(name="app_claimant_user_id", type="integer", nullable=true) */
    protected $claimant_user_id;

    /** @Column(name="index_name", type="string", length=255, nullable=true) */
    protected $index_name;

    /** @Column(name="index_starttime", type="integer", nullable=true) */
    protected $index_starttime;

    /** @Column(name="index_endtime", type="integer", nullable=true) */
    protected $index_endtime;

    /**
     * @ManyToOne(targetEntity="Organization")
     * @JoinColumn(name="app_org_id", referencedColumnName="org_id", onDelete="CASCADE")
     */
    protected $organization;

    /**
     * @ManyToOne(targetEntity="CampsFormProgram", inversedBy="forms")
     * @JoinColumn(name="app_program_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $program;

    /**
     * @ManyToOne(targetEntity="CampsFormUser")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="app_claimant_user_id", referencedColumnName="user_id")
     */
    protected $claimant;

    /** @OneToMany(targetEntity="CampsFormLog", mappedBy="form") */
    protected $logs;

    /** @OneToMany(targetEntity="CampsFormSession", mappedBy="form") */
    protected $sessions;

    /** @OneToMany(targetEntity="CampsFormIncident", mappedBy="form") */
    protected $incidents;
    
    /**
     * Static Functions
     */

    public function renderView()
    {
        $sections = $this->getViewData();

        $output = '';
        foreach((array)$sections as $section_name => $section_html)
        {
            $output .= '<h2>'.$section_name.'</h2>'.$section_html;
        }

        return $output;
    }

    public function getViewData()
    {
        $module_config = \Zend_Registry::get('module_config');
        $output = array();

        if ($this->getVersion() == self::VERSION_DFFORM)
        {
            $settings = self::loadSettings();
            $steps = $this->getSteps();

            $data = (array)$this->data;
            foreach($data as $step_name => $step_data)
            {
                if (!empty($step_data))
                {
                    if (in_array($step_name, $steps))
                    {
                        $config_file = 'step_'.strtolower(preg_replace("/[^A-Za-z0-9]/", '', $step_name));
                        $form_config = $module_config['form_camps']->forms->{$config_file}->toArray();

                        $form = new \DF\Form($form_config);
                        $form->setDefaults($step_data);
                        $output[$step_name] = $form->renderView(NULL, TRUE);
                    }
                    else
                    {
                        $output[$step_name] = \StuAct\Form\Legacy::renderView($step_data, TRUE, TRUE);
                    }
                }
            }
        }
        else
        {
            $output = \StuAct\Form\Legacy::renderView((array)$this->data, TRUE);
        }

        // Render sessions.
        if (count($this->sessions) > 0)
        {
            $session_forms = array();

            $i = 0;
            foreach($this->sessions as $session)
            {
                $i++;

                $form_config = $module_config['form_camps']->forms->session->toArray();

                $form = new \DF\Form($form_config);
                $form->setDefaults($session->toArray());
                $session_forms[] = '<h4>Session #'.$i.'</h4>'.$form->renderView(NULL, TRUE);
            }

            $output['Sessions'] = implode('<hr>', $session_forms);
        }

        return $output;
    }

    public function getSteps()
    {
        $settings = self::loadSettings();
        $steps = (array)$settings['steps'];

        return $steps;
    }

    public function hasErrors()
    {
        $output = array();

        if ($this->getVersion() == self::VERSION_DFFORM)
        {
            $module_config = \Zend_Registry::get('module_config');
            $settings = self::loadSettings();
            $steps = $this->getSteps();

            $errors = array();

            foreach($steps as $step_name)
            {
                $step_data = (array)$this->data[$step_name];

                $config_file = 'step_'.strtolower($step_name);
                $form_config = $module_config['form_camps']->forms->{$config_file}->toArray();

                $form = new \DF\Form($form_config);

                if (!$form->isValid($step_data))
                {
                    $form_errors = $form->getMessages(null, true);

                    if ($form_errors)
                        $errors[$step_name] = $form_errors;
                }
            }

            if ($errors)
                return $errors;
        }

        return FALSE;
    }

    public function isAllowed($action = 'view')
    {
        $acl = \DF\Registry::get('acl');
        if ($acl->isAllowed('admin_forms_camps'))
            return true;

        switch(strtolower($action))
        {
            case "roster":
                $phases = array(
                    self::PHASE_APPROVED_NEEDS_ROSTER
                );
            break;

            case "incident":
                $phases = array(
                    self::PHASE_DEAN_REVIEW, 
                    self::PHASE_APPROVED_NEEDS_ROSTER, 
                    self::PHASE_APPROVED_UNPAID, 
                    self::PHASE_COMPLETE
                );
            break;

            case "edit":
                $phases = array(
                    self::PHASE_INCOMPLETE, 
                    self::PHASE_NEEDS_MODIFICATION,
                    self::PHASE_SECONDARY_REVIEW,
                );
            break;

            case "view":
            default:
                return true;
            break;
        }

        return in_array($this->phase, $phases);
    }

    public function getUrls()
    {
        if ($this->getVersion() == self::VERSION_LEGACY)
        {
            return array(
                'home' => 'https://studentactivities.tamu.edu/online/forms/camps/index',
                'edit' => 'https://studentactivities.tamu.edu/online/forms/camps/'.$this->access_code.'/start',
                'review' => \DF\Url::route(array(
                    'module' => 'form_camps',
                    'controller' => 'form',
                    'action' => 'review',
                    'code' => $this->access_code,
                    'access' => $this->getPhaseCode($phase),
                )),
                'view' => \DF\Url::route(array(
                    'module' => 'form_camps',
                    'controller' => 'form',
                    'action' => 'view',
                    'code' => $this->access_code,
                )),
            );
        }
        else
        {
            return array(
                'home' => \DF\Url::route(array(
                    'module' => 'form_camps',
                    'controller' => 'index',
                )),
                'review' => \DF\Url::route(array(
                    'module' => 'form_camps',
                    'controller' => 'form',
                    'action' => 'review',
                    'code' => $this->access_code,
                    'access' => $this->getPhaseCode($phase),
                )),
                'view' => \DF\Url::route(array(
                    'module' => 'form_camps',
                    'controller' => 'form',
                    'action' => 'view',
                    'code' => $this->access_code,
                )),
                'edit' => \DF\Url::route(array(
                    'module' => 'form_camps',
                    'controller' => 'form',
                    'action' => 'begin',
                    'code' => $this->access_code,
                    'access' => $this->getPhaseCode($phase),
                )),
            );
        }
    }

    public function notify($phase = NULL)
    {
        if ($phase === NULL)
            $phase = $this->phase;

        $settings = self::loadSettings();

        $email_vars = array();
        $email_vars['form'] = $this;
        $email_vars['form_settings'] = $settings;
        
        // Determine URLs that will be used on several different templates.
        $urls = $this->getUrls();
        $email_vars['home_url'] = $urls['home'];
        $email_vars['review_url'] = $urls['review'];
        $email_vars['view_url'] = $urls['view'];
        $email_vars['edit_url'] = $urls['edit'];

        $email_vars['admin_url'] = \DF\Url::route(array('module' => 'form_camps', 'controller' => 'manage', 'action' => 'view', 'id' => $this->id));
        
        // Determine the proper routing destination.
        $routing_area = $this->getRoutingArea();
        $email_to = $this->getContactForPhase($phase);
        
        // Send a message to the person responsible for current phase approval.
        switch($phase)
        {
            case self::PHASE_INCOMPLETE:
            case self::PHASE_NEEDS_MODIFICATION:
                $email_subject = 'Needs Modifications';
                $email_template = 'need_modifications';
            break;
            
            case self::PHASE_SECONDARY_REVIEW:
                $email_subject = 'Needs Completion';
                $email_template = 'need_completion';
            break;
            
            case self::PHASE_DEPT_REVIEW:
            case self::PHASE_DEAN_REVIEW:
            case self::PHASE_RISK_INITIAL:
                $email_subject = 'Needs Review & Approval';
                $email_template = 'need_review';
            break;

            case self::PHASE_APPROVED_NEEDS_ROSTER:
                $email_subject = 'Approved';
                $email_template = 'completed';
            break;
            
            case self::PHASE_APPROVED_UNPAID:
                $email_subject = 'Approved, Needs Payment';
                $email_template = 'approved_unpaid';
            break;
        }

        if (DF_APPLICATION_ENV == 'development')
            $email_to = 'bneece@doit.tamu.edu';
        
        if ($email_subject && $email_to)
        {
            $this->addLogEntry('Notification sent to <b>'.implode(', ', (array)$email_to).'</b>.');

            \DF\Messenger::send(array(
                'to'        => $email_to,
                'subject'   => 'Camps Form #'.$this->access_code.': '.$email_subject,
                'module'    => 'form_camps',
                'template'  => $email_template,
                'vars'      => $email_vars,
            ));
        }

        $notify_everyone_phases = array(
            self::PHASE_NEEDS_MODIFICATION,
            self::PHASE_RISK_INITIAL,
            self::PHASE_DECLINED,
            self::PHASE_COMPLETE,
        );
        
        if (in_array($phase, $notify_everyone_phases))
            $status_email_to = $this->getContactForPhase(self::PHASE_COMPLETE);
        else if ($phase != self::PHASE_INCOMPLETE)
            $status_email_to = $this->getContactForPhase(self::PHASE_INCOMPLETE);
        
        if ($status_email_to)
        {
            \DF\Messenger::send(array(
                'to'        => $status_email_to,
                'subject'   => 'Camps Form #'.$this->access_code.' Status Update',
                'module'    => 'form_camps',
                'template'  => 'status_change',
                'vars'      => $email_vars,
            ));
        }
    }

    public function getRoutingArea()
    {
        $is_legacy = ($this->getVersion() == self::VERSION_LEGACY);

        if ($is_legacy)
            $sponsor_affiliation = $this->data['Contact_Info']['Sponsor_affiliation'];
        else
            $sponsor_affiliation = $this->program->sponsor;

        switch($sponsor_affiliation)
        {
            case "University":
                if ($is_legacy)
                    return $this->data['Contact_Info']['University_Dept'];
                else
                    return $this->program->sponsoring_dept;
            break;
            
            case "Athletic":
                return 'Athletics Department';
            break;
            
            case "Third Party":
                if ($is_legacy)
                    return $this->data['Contact_Info']['ThirdParty_Dept'];
                else
                    return $this->program->sponsoring_dept;
            break;
            
            case "Student Organization":
                return 'Student Activities';
            break;
            
            case "Sport Club":
            default:
                return $sponsor_affiliation;
            break;
        }
        return null;
    }

    public function hasSecondaryReviewer()
    {
        $contact = $this->getContactForPhase(self::PHASE_SECONDARY_REVIEW);
        return (!empty($contact));
    }

    // Retrieves the student organization associated with the form if applicable.
    public function getStudentOrganization()
    {
        if ($this->getVersion() == self::VERSION_LEGACY)
        {
            if ($this->organization instanceof Organization)
                return $this->organization;

            if ($this->data['Contact_Info']['Sponsor_affiliation'] != "Student Organization")
                return NULL;
            
            $org_name = htmlspecialchars_decode($this->data['Contact_Info']['Organization_name']);
            $org_id_string = substr(trim($this->data['Account_Info']['Insurance_account_number']), 0, 6);

            $em = self::getEntityManager();
            $org_raw = $em->createQuery('SELECT o FROM Entity\Organization o WHERE ((o.org_name LIKE :org_name OR o.org_id = :org_id) AND (o.org_id != 0))')
                ->setParameter('org_name', $org_name)
                ->setParameter('org_id', $org_id_string)
                ->execute();

            if (count($org_raw) > 0)
            {
                $org = $org_raw[0];

                $this->organization = $org;
                $this->save();

                return $this->organization;
            }
        }
        else
        {
            return $this->program->organization;
        }

        return null;
    }

    public function getContactForPhase($phase = NULL)
    {
        if ($phase === null)
            $phase = $this->phase;
        
        // Determine the proper routing destination.
        $routing_area = $this->getRoutingArea();
        
        $settings = self::loadSettings();
        
        // Send a message to the person responsible for current phase approval.
        switch($phase)
        {
            case self::PHASE_INCOMPLETE:
            case self::PHASE_NEEDS_MODIFICATION:
                return $this->email;
            break;
            
            case self::PHASE_SECONDARY_REVIEW:
                $org = $this->getStudentOrganization();

                if ($org)
                {
                    Organization::loadSettings();
                    $advisor = $org->getOfficerByPosition(ORG_OFFICER_PRIMARY_ADVISOR);
                    if ($advisor)
                        $email_to = $advisor->user->email;
                }
                else
                {
                    $email_to = $settings['department_routing'][$routing_area]['secondary'];
                }
                
                return $email_to;
            break;
            
            case self::PHASE_DEPT_REVIEW:               
                return $settings['department_routing'][$routing_area]['dept'];
            break;
            
            case self::PHASE_DEAN_REVIEW:
                return $settings['department_routing'][$routing_area]['dean'];
            break;
            
            case self::PHASE_RISK_INITIAL:
                return $settings['contact_email'];
            break;

            case self::PHASE_APPROVED_NEEDS_ROSTER:
            case self::PHASE_APPROVED_UNPAID:
                return $this->_raw_info['email'];
            break;
            
            case self::PHASE_DECLINED:
            case self::PHASE_COMPLETE:
                $email_to = array_merge(
                    (array)$settings['department_routing'][$routing_area]['dept'],
                    (array)$settings['department_routing'][$routing_area]['dean'],
                    (array)$this->_raw_info['email'],
                    (array)$settings['contact_email']
                );
                return $email_to;
            break;
        }
    }

    public function addLogEntry($text)
    {
        $log = new CampsFormLog;
        $log->form = $this;
        $log->note = $text;
        $log->save();

        return $log;
    }

    /**
     * Static Functions
     */

    public static function getPhaseHash($id_hash, $phase)
    {
        $hash_string = $id_hash.'-'.$phase;
        return sha1($hash_string."1ad1afc09c07f162fe993a88b5c9fbb4");
    }

    public static function loadSettings()
    {
        static $settings;

        if ($settings === NULL)
        {
            $module_config = \Zend_Registry::get('module_config');
            $settings = $module_config['form_camps']->general->toArray();

            $settings['phases'] = self::getPhaseNames();

            $routing_raw = CampsFormRouting::fetchArray('name');
            $settings['department_routing'] = array();
            foreach($routing_raw as $routing_row)
                $settings['department_routing'][$routing_row['name']] = $routing_row;

            $settings['department_options'] = array();
            foreach($routing_raw as $routing_row)
                $settings['department_options'][$routing_row['name']] = $routing_row['name'];
        }

        return $settings;
    }

    public static function getPhaseNames()
    {
        return array(
            self::PHASE_INCOMPLETE          => 'Incomplete',
            self::PHASE_DECLINED            => 'Declined',
            self::PHASE_NEEDS_MODIFICATION  => 'Needs Modifications',
            self::PHASE_SECONDARY_REVIEW    => 'Secondary Sponsor',
            self::PHASE_DEPT_REVIEW         => 'Sponsoring Department',
            self::PHASE_RISK_INITIAL        => 'Risk Management',
            self::PHASE_DEAN_REVIEW         => 'Sponsoring Dean',
            self::PHASE_APPROVED_NEEDS_ROSTER => 'Roster Needed',
            self::PHASE_APPROVED_UNPAID     => 'Unpaid',
            self::PHASE_COMPLETE            => 'Complete',
            self::PHASE_ARCHIVED            => 'Archived',
        );
    }
}