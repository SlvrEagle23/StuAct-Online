<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Organization
 *
 * @Table(name="organizations", indexes={
 *   @index(name="account_idx", columns={"account_number"}),
 *   @index(name="search_idx", columns={"org_name","profile_abbreviation","it_web_addr"}),
 *   @index(name="sort_idx", columns={"org_category","org_classification","org_status"})
 * })
 * @Entity
 */

class Organization extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        self::loadSettings();

        $this->officer_positions = new ArrayCollection;
        $this->officer_requests = new ArrayCollection;
        $this->officer_archives= new ArrayCollection;

        $this->permissions = new ArrayCollection;
        $this->miscellaneous_requirements = new ArrayCollection;
        $this->files = new ArrayCollection;
        $this->logs = new ArrayCollection;

        $this->display_on_search = 1;
    }

    // Auto-flag setting override.
    public function __set($key, $value)
    {
        if (substr($key, 0, 4) == "flag" && substr_count($key, '_') == 1)
            $this->setFlag(str_replace('flag_', '', $key), $value);
        else
            parent::__set($key, $value);
    }

    /**
     * @Column(name="org_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="account_number", type="integer", nullable=true) */
    protected $account_number;

    /** @Column(name="org_afs_id", type="integer", nullable=true) */
    protected $afs_id;

    /** @Column(name="org_name", type="string", length=255, nullable=true) */
    protected $name;

    // E-mail header including name.
    public function getEmailHeader()
    {
        $account_num = ($this->account_number) ? ' ('.$this->account_number.')' : '';
        return '<p>Organization: <b>'.$this->name.$account_num.'</b></p>';
    }

    /** @Column(name="org_category", type="string", length=255, nullable=true) */
    protected $category;

    /** @Column(name="org_classification", type="string", length=255, nullable=true) */
    protected $classification;

    /** @Column(name="org_advising_department", type="string", length=250, nullable=true) */
    protected $advising_department;

    /** @Column(name="org_cycle", type="smallint", nullable=true) */
    protected $cycle;

    /** @Column(name="org_cycle_override", type="smallint", nullable=true) */
    protected $cycle_override;

    /** @Column(name="org_status", type="smallint", nullable=true) */
    protected $status;

    public function setStatus($new_status)
    {
        $settings = self::loadSettings();
        $old_status = $this->status;
        $this->status = $new_status;

        if ($new_status != $old_status)
        {
            $this->date_status_changed = time();
            $this->date_status_changed_official = time();

            $old_status_text = $settings['status_codes'][$old_status];
            $new_status_text = $settings['status_codes'][$new_status];
            $this->log('Recognition status changed from "'.$old_status_text.'" to "'.$new_status_text.'".', 'status');
        }

        $this->status = $new_status;
    }
    public function getStatusIcon()
    {
        $settings = self::loadSettings();
        return $settings['status_icons'][$this->status];
    }
    public function getStatusName()
    {
        $settings = self::loadSettings();
        return $settings['status_codes'][$this->status];
    }

    /** @Column(name="org_status_desc", type="text", nullable=true) */
    protected $status_desc;

    /** @Column(name="org_batch_id", type="integer", nullable=true) */
    protected $batch_id;

    /** @Column(name="ignore_batch_generator", type="smallint", nullable=true) */
    protected $ignore_batch_generator;

    /** @Column(name="org_display_on_search", type="smallint", nullable=true) */
    protected $display_on_search;

    /** @Column(name="date_last_updated", type="integer", nullable=true) */
    protected $date_last_updated;

    /** @Column(name="date_status_changed", type="integer", nullable=true) */
    protected $date_status_changed;

    /** @Column(name="date_status_changed_official", type="integer", nullable=true) */
    protected $date_status_changed_official;

    /** @Column(name="date_recognized", type="integer", nullable=true) */
    protected $date_recognized;

    /** @Column(name="date_last_synchronized", type="integer", nullable=true) */
    protected $date_last_synchronized;

    /** @Column(name="date_last_notified", type="integer", nullable=true) */
    protected $date_last_notified;

    /** @Column(name="sact_comments", type="text", nullable=true) */
    protected $sact_comments;

    /** @Column(name="enhanced_expectations", type="smallint", nullable=true) */
    protected $enhanced_expectations;

    /** @Column(name="enhanced_annual", type="smallint", nullable=true) */
    protected $enhanced_annual;

    /** @Column(name="enhanced_desc", type="text", nullable=true) */
    protected $enhanced_desc;

    /** @Column(name="risk_approved", type="smallint", nullable=true) */
    protected $risk_approved;

    /** @Column(name="sofc_exempt", type="smallint", nullable=true) */
    protected $sofc_exempt;

    /** @Column(name="sofc_audit", type="smallint", nullable=true) */
    protected $sofc_audit;

    /** @Column(name="sofc_send_email", type="smallint", nullable=true) */
    protected $sofc_send_email;

    /**
     * Profile Information
     */

    /** @Column(name="profile_abbreviation", type="string", length=255, nullable=true) */
    protected $profile_abbreviation;

    /** @Column(name="profile_purpose", type="text", nullable=true) */
    protected $profile_purpose;

    /** @Column(name="profile_web", type="string", length=255, nullable=true) */
    protected $profile_web;

    /** @Column(name="profile_email", type="string", length=255, nullable=true) */
    protected $profile_email;

    /** @Column(name="profile_contact_name", type="string", length=255, nullable=true) */
    protected $profile_contact_name;

    /** @Column(name="profile_contact_phone", type="string", length=255, nullable=true) */
    protected $profile_contact_phone;

    /** @Column(name="profile_contact_email", type="string", length=255, nullable=true) */
    protected $profile_contact_email;

    /** @Column(name="profile_mail_stop", type="string", length=150, nullable=true) */
    protected $profile_mail_stop;

    /** @Column(name="profile_mailing_address", type="text", nullable=true) */
    protected $profile_mailing_address;

    /** @Column(name="orgmatch_profile", type="array", nullable=true) */
    protected $orgmatch_profile;

    /** @Column(name="info_year_founded", type="string", length=255, nullable=true) */
    protected $info_year_founded;

    /** @Column(name="info_membership_dues", type="string", length=255, nullable=true) */
    protected $info_membership_dues;

    /** @Column(name="info_members_selected", type="string", length=255, nullable=true) */
    protected $info_members_selected;

    /** @Column(name="info_member_intake", type="string", length=255, nullable=true) */
    protected $info_member_intake;

    /** @Column(name="info_membership", type="string", length=255, nullable=true) */
    protected $info_membership;

    /** @Column(name="info_meeting_location", type="string", length=255, nullable=true) */
    protected $info_meeting_location;

    /** @Column(name="info_time_required", type="string", length=255, nullable=true) */
    protected $info_time_required;

    /** @Column(name="info_allow_follow_up", type="smallint", nullable=true) */
    protected $info_allow_follow_up;

    /** @Column(name="info_willing_to_volunteer", type="smallint", nullable=true) */
    protected $info_willing_to_volunteer;

    /** @Column(name="info_banking_usage", type="string", length=50, nullable=true) */
    protected $info_banking_usage;

    /** @Column(name="info_computer_equipment", type="boolean", nullable=true) */
    protected $info_computer_equipment;

    /** @Column(name="positions_on_sigcard", type="array", nullable=true) */
    protected $positions_on_sigcard;

    public function getPositionsOnSigcard()
    {
        $positions = array();
        if (count($this->officer_positions) > 0)
        {
            foreach($this->officer_positions as $position)
            {
                if ($position->hasPermission('sigcard') || $position->isInGroup('sigcard_req'))
                    $positions[$position->position] = $position->user_id;
            }
        }

        ksort($positions);
        return $positions;
    }
    public function getPositionsOnSigcardOld()
    {
        return $this->positions_on_sigcard;
    }

    /** @Column(name="position_titles", type="array", nullable=true) */
    protected $position_titles;

    /** @Column(name="roster_num_advisors", type="smallint", nullable=true) */
    protected $roster_num_advisors;

    public function getRosterNumAdvisors()
    {
        return ((int)$this->roster_num_advisors) ? $this->roster_num_advisors : 2;
    }

    /** @Column(name="roster_num_officers", type="smallint", nullable=true) */
    protected $roster_num_officers;

    public function getRosterNumOfficers()
    {
        return ((int)$this->roster_num_officers) ? $this->roster_num_officers : 10;
    }

    /**
     * Recognition Flags
     */

    /** @Column(name="flag_advisors_present", type="smallint", nullable=true) */
    protected $flag_advisors_present;

    /** @Column(name="flag_advisors_present_timestamp", type="integer", nullable=true) */
    protected $flag_advisors_present_timestamp;

    /** @Column(name="flag_advisors", type="smallint", nullable=true) */
    protected $flag_advisors_trained;

    /** @Column(name="flag_advisors_timestamp", type="integer", nullable=true) */
    protected $flag_advisors_trained_timestamp;

    /** @Column(name="flag_leadership", type="smallint", nullable=true) */
    protected $flag_leaders_present;

    /** @Column(name="flag_leadership_timestamp", type="integer", nullable=true) */
    protected $flag_leaders_present_timestamp;

    /** @Column(name="flag_renewal", type="smallint", nullable=true) */
    protected $flag_leaders_renewed;

    /** @Column(name="flag_renewal_timestamp", type="integer", nullable=true) */
    protected $flag_leaders_renewed_timestamp;

    /** @Column(name="flag_workshops", type="smallint", nullable=true) */
    protected $flag_leaders_trained;

    /** @Column(name="flag_workshops_timestamp", type="integer", nullable=true) */
    protected $flag_leaders_trained_timestamp;

    /** @Column(name="flag_gpr", type="smallint", nullable=true) */
    protected $flag_officers_eligible;

    /** @Column(name="flag_gpr_timestamp", type="integer", nullable=true) */
    protected $flag_officers_eligible_timestamp;

    /** @Column(name="flag_updates", type="smallint", nullable=true) */
    protected $flag_updates;

    /** @Column(name="flag_updates_timestamp", type="integer", nullable=true) */
    protected $flag_updates_timestamp;

    /** @Column(name="flag_updates_notification", type="integer", nullable=true) */
    protected $flag_updates_notification;

    /** @Column(name="flag_sigcard", type="smallint", nullable=true) */
    protected $flag_sigcard;

    /** @Column(name="flag_sigcard_timestamp", type="integer", nullable=true) */
    protected $flag_sigcard_timestamp;

    /** @Column(name="flag_sigcard_approved_positions", type="array", nullable=true) */
    protected $flag_sigcard_approved_positions;

    /** @Column(name="flag_constitution", type="smallint", nullable=true) */
    protected $flag_constitution;

    /** @Column(name="flag_constitution_timestamp", type="integer", nullable=true) */
    protected $flag_constitution_timestamp;

    /** @Column(name="flag_misc", type="smallint", nullable=true) */
    protected $flag_misc;

    /** @Column(name="flag_misc_timestamp", type="integer", nullable=true) */
    protected $flag_misc_timestamp;

    public function setFlag($flag_name, $new_value)
    {
        $settings = self::loadSettings();

        if (!property_exists($this, 'flag_'.$flag_name))
            die($flag_name);

        $current_flag = $this->{'flag_'.$flag_name};

        // Update SOFC signature card details.
        if ($flag_name == "sigcard" && $new_value == ORG_FLAG_MEETS_REQ)
        {
            $this->flag_sigcard_approved_positions = $this->getPositionsOnSigcard();
        }

        // Detect change in flag.
        if ($new_value != $current_flag)
        {
            $this->{'flag_'.$flag_name} = $new_value;
            $this->{'flag_'.$flag_name.'_timestamp'} = time();

            // Post log entry.
            $current_flag_text = $settings['flag_codes'][$current_flag];
            $new_flag_text = $settings['flag_codes'][$new_value];
            $flag_name_text = $settings['flags'][$flag_name];
            $this->log('Recognition requirement <b>'.$flag_name_text.'</b> changed from "'.$current_flag_text.'" to "'.$new_flag_text.'"', 'status');
        }
    }
    public function getFlag($flag_name)
    {
        return $this->{'flag_'.$flag_name};
    }

    /**
     * IT Web/E-mail Account Information
     */

    /** @Column(name="it_web_pending", type="smallint", nullable=true) */
    protected $it_web_pending;
    
    /** @Column(name="it_web_pending_date", type="integer", nullable=true) */
    protected $it_web_pending_date;

    /** @Column(name="it_web_type", type="smallint", nullable=true) */
    protected $it_web_type;

    /** @Column(name="it_web_suspended", type="smallint", nullable=true) */
    protected $it_web_suspended;

    /** @Column(name="it_web_addr", type="string", length=255, nullable=true) */
    protected $it_web_addr;

    /** @Column(name="it_web_username", type="string", length=255, nullable=true) */
    protected $it_web_username;

    /** @Column(name="it_web_password", type="string", length=255, nullable=true) */
    protected $it_web_password;

    /** @Column(name="it_web_password_change_timestamp", type="integer", nullable=true) */
    protected $it_web_password_change_timestamp;

    /** @Column(name="it_web_drupal", type="smallint", nullable=true) */
    protected $it_web_drupal;

    /** @Column(name="it_web_drupal_pending", type="smallint", nullable=true) */
    protected $it_web_drupal_pending;

    /** @Column(name="it_web_drupal_version", type="string", length=50, nullable=true) */
    protected $it_web_drupal_version;

    /** @Column(name="it_web_drupal_installdate", type="integer", nullable=true) */
    protected $it_web_drupal_installdate;

    /** @Column(name="it_email_pending", type="smallint", nullable=true) */
    protected $it_email_pending;

    /** @Column(name="it_email_pending_date", type="integer", nullable=true) */
    protected $it_email_pending_date;

    /** @Column(name="it_email_type", type="smallint", nullable=true) */
    protected $it_email_type;

    /** @Column(name="it_email_addr", type="string", length=255, nullable=true) */
    protected $it_email_addr;

    /** @Column(name="it_email_username", type="string", length=255, nullable=true) */
    protected $it_email_username;

    /** @Column(name="it_email_password", type="string", length=255, nullable=true) */
    protected $it_email_password;

    /** @Column(name="it_email_forwarder", type="string", length=200, nullable=true) */
    protected $it_email_forwarder;

    /**
     * Constitution Information
     */

    /** @Column(name="constitution_exempt", type="smallint", nullable=true) */
    protected $constitution_exempt;

    /** @Column(name="constitution_filename", type="string", length=255, nullable=true) */
    protected $constitution_filename;

    /** @Column(name="constitution_pages", type="array", nullable=true) */
    protected $constitution_pages;

    /** @Column(name="constitution_declinereasons", type="array", nullable=true) */
    protected $constitution_declinereasons;

    /** @Column(name="constitution_comments", type="text", nullable=true) */
    protected $constitution_comments;

    /** @Column(name="constitution_claimant", type="string", length=255, nullable=true) */
    protected $constitution_claimant;

    /** @Column(name="constitution_data", type="json", nullable=true) */
    protected $constitution_data;

    public function setConstitutionData($new_const_data)
    {
        $new_const_data = (array)$new_const_data;
        $const_data = (array)$this->constitution_data;
        $items_changed = 0;

        foreach($new_const_data as $item_key => $item_val)
        {
            if (stristr($item_key, '_changed') === FALSE)
            {
                if (isset($const_data[$item_key]))
                    $old_data = $const_data[$item_key];
                else
                    $old_data = NULL;
                
                if (strcmp($old_data, $item_val) !== 0)
                {
                    $items_changed++;
                    $new_const_data[$item_key.'_changed'] = TRUE;
                }
                else
                {
                    $new_const_data[$item_key.'_changed'] = FALSE;
                }
            }
        }

        $new_const_data['items_changed'] = $items_changed;
        $this->constitution_data = $new_const_data;
    }
    
    /** @Column(name="constitution_submitted_by", type="integer", nullable=true) */
    protected $constitution_submitted_by;

    /**
     * Relations
     */
    
    /**
     * @OneToMany(targetEntity="Entity\OrganizationOfficer", mappedBy="organization")
     * @OrderBy({"position" = "ASC"})
     */
    protected $officer_positions;

    /** @OneToMany(targetEntity="Entity\OrganizationOfficerRequest", mappedBy="organization") */
    protected $officer_requests;

    /** @OneToMany(targetEntity="Entity\OrganizationOfficerArchive", mappedBy="organization") */
    protected $officer_archives;

    /** @OneToMany(targetEntity="Entity\OrganizationPermission", mappedBy="organization") */
    protected $permissions;

    /**
     * @OneToMany(targetEntity="Entity\OrganizationMiscellaneousRequirement", mappedBy="organization")
     * @OrderBy({"timestamp" = "DESC"})
     */
    protected $miscellaneous_requirements;

    /** @OneToMany(targetEntity="Entity\OrganizationFile", mappedBy="organization") */
    protected $files;
    
    /**
     * @ManyToOne(targetEntity="Entity\OrganizationBatchGroup", inversedBy="organizations")
     * @JoinColumn(name="org_batch_id", referencedColumnName="id")
     */
    protected $batch_group;

    /**
     * @OneToMany(targetEntity="Entity\Log", mappedBy="organization")
     * @OrderBy({"timestamp" = "DESC"})
     */
    protected $logs;

    /**
     * Single-Record Functions
     */
    
    public function notify($message_type, $vars = array())
    {
        $nm = $this->getNotificationManager();
        return $nm->notify($message_type, $vars);
    }

    public function getNotificationManager()
    {
        return new \StuAct\Organization\Notification($this);
    }

    public function log($message, $type = null, User $user = null)
    {
        $log_info = array(
            'message'   => $message,
            'type'      => $type,
            'user'      => $user,
            'organization' => $this,
        );
        Log::post($log_info);
    }

    /**
     * Roster Functions
     */

    // Get a user's position in the organization.
    public function getUserPosition(User $user = NULL)
    {
        return OrganizationOfficer::getUserPosition($user, $this);
    }

    // Get an officer position by its position number.
    public function getOfficerByPosition($position_num)
    {
        $all_officers = $this->officer_positions;
        if (count($all_officers) == 0)
            return NULL;

        foreach($all_officers as $officer)
        {
            if ($officer->position == $position_num)
                return $officer;
        }
        return NULL;
    }

    // Get all officer positions that are within a certain group.
    public function getOfficersByGroup($group_name)
    {
        $all_officers = $this->officer_positions;
        if (count($all_officers) == 0)
            return array();

        // Convert to standard array for easy filtering.
        $officers = array();
        foreach($all_officers as $officer)
            $officers[] = $officer;

        return array_filter($officers, function($o) use ($group_name) {
            return $o->isInGroup($group_name);
        });
    }

    // Shortcut functions for getting officers by group.
    public function getAdvisors()
    {
        return $this->getOfficersByGroup('advisors');
    }
    public function getStudentLeaders()
    {
        return $this->getOfficersByGroup('student_leaders');
    }

    /**
     * Recognition Functions
     */

    public function isBatch()
    {
        return ($this->org_batch_id != 0);
    }

    public function getRecognitionManager()
    {
        return new \StuAct\Organization\Recognition($this);
    }
    public function getCycleChange() { return $this->getRecognitionManager()->getCycleChange(); }
    public function getNextStatusChange() { return $this->getRecognitionManager()->getNextStatusChange(); }
    public function synchronize() { return $this->getRecognitionManager()->synchronize(); }
    public function isEarlyBird() { return $this->getRecognitionManager()->isEarlyBird(); }

    public function getBaseUrl()
    {
        return self::baseUrl($this->id);
    }
    public function route($path)
    {
        if (!is_array($path))
            $path = array('controller' => $path);

        $path['module'] = 'organization';
        $path['id'] = $this->id;

        return \DF\Url::route($path);
    }

    /**
     * Static Functions
     */
    
    public static function fetchSelect($add_blank = FALSE, \Closure $display = NULL)
	{
        if ($display === null)
        {
            $display = function($record) {
                return htmlspecialchars_decode($record['name']).' ('.$record['account_number'].')';
            };
        }
        return parent::fetchSelect($add_blank, $display);
	}
    
    public static function baseUrl($org_id, $controller = 'index')
	{
		return \DF\Url::route(array(
			'module' => 'organization', 
			'controller' => $controller, 
			'action' => 'index', 
			'id' => $org_id,
		));
	}
    
    public static function search($terms, $search_type='name', $return_as_array=false)
	{   
        $em = \Zend_Registry::get('em');
        $query = $em->createQueryBuilder()
            ->select('o')
            ->from(__CLASS__, 'o')
            ->where('(o.id != 0)')
            ->addOrderBy('o.status', 'DESC')
            ->addOrderBy('o.name', 'ASC');

        $acl = \Zend_Registry::get('acl');
        if (!$acl->isAllowed(array('admin_orgs', 'view all organizations')))
        {
            $query->andWhere('(o.status NOT IN (:hide_statuses) AND o.display_on_search != 0)')
                ->setParameter('hide_statuses', array(ORG_STATUS_NOT_RECOGNIZED, ORG_STATUS_EXEMPT));
        }
        
		if ($search_type == "name")
		{
            $query->andWhere('(o.id = :q_exact OR o.account_number = :q_exact OR o.name LIKE :q OR o.profile_abbreviation LIKE :q OR o.it_web_addr LIKE :q)')
                ->setParameter('q_exact', $terms)
                ->setParameter('q', '%'.$terms.'%');
		}
		else if ($search_type == "letter")
		{
            $query->andWhere('(o.name LIKE :letter OR o.profile_abbreviation LIKE :letter)')
                ->setParameter('letter', substr($terms, 0, 1).'%');
		}
		else if ($search_type == "category")
		{
            $query->andWhere('o.classification = :class')
                ->setParameter('class', $terms);
		}
		else
		{
			throw new \DF\Exception\DisplayOnly('Invalid Search Specified');
		}

        if ($return_as_array)
            $results = $query->getQuery()->getArrayResult();
        else
            $results = $query->getQuery()->getResult();
		
		return $results;
	}
    
    public static function loadSettings()
    {
        static $settings;
        
        if (!$settings)
        {
            $config = \Zend_Registry::get('config');
            $settings = $config->organizations->toArray();
        }
        
        return $settings;
    }
}