<?php
namespace Entity;


/**
 * StrengthsOrderFormLog
 *
 * @Table(name="forms_strengths_orders_log")
 * @Entity
 */
class StrengthsOrderFormLog extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="log_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $log_id;

    /** @Column(name="log_user_id", type="integer", length=4) */
    protected $log_user_id;

    /** @Column(name="log_timestamp", type="integer", length=4) */
    protected $log_timestamp;

    /** @Column(name="log_message", type="text", nullable=true) */
    protected $log_message;

    /** @Column(name="change_books", type="integer", length=2) */
    protected $change_books;

    /** @Column(name="change_codes", type="integer", length=2) */
    protected $change_codes;

    /** @Column(name="balance_books", type="integer", length=2) */
    protected $balance_books;

    /** @Column(name="balance_codes", type="integer", length=2) */
    protected $balance_codes;
}