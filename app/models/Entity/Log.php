<?php
namespace Entity;

/**
 * @Table(name="logs", indexes={
 *   @index(name="search_idx", columns={"log_type"}),
 *   @index(name="sort_idx", columns={"log_timestamp"})
 * })
 * @Entity
 */
class Log extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->timestamp = time();
        $this->privacy = 0;
    }

    /**
     * @Column(name="log_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $log_id;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="org_id", type="integer", nullable=true) */
    protected $org_id;

    /** @Column(name="log_type", type="string", length=255, nullable=true) */
    protected $type;

    /** @Column(name="log_message", type="text", nullable=true) */
    protected $message;

    /** @Column(name="log_timestamp", type="integer", length=4, nullable=true) */
    protected $timestamp;

    /** @Column(name="log_privacy", type="integer", length=1, nullable=true) */
    protected $privacy;

    /**
     * @ManyToOne(targetEntity="Entity\User", inversedBy="logs")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="Entity\Organization", inversedBy="logs")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;

    /**
     * Static Functions
     */
    
    public static function fetchByOrganization($org_id, $limit = NULL)
    {
        $em = \Zend_Registry::get('em');

        $query = $em->createQueryBuilder()
            ->select('l, u')
            ->from(__CLASS__, 'l')
            ->leftJoin('l.user', 'u')
            ->where('l.org_id = :org_id')
            ->setParameter('org_id', $org_id)
            ->orderBy('l.timestamp', 'DESC');

        if ($limit)
            $query->setMaxResults($limit);
        
        return $query->getQuery()->getArrayResult();
    }

    public static function fetchByUser($user_id, $limit = NULL)
    {
        $em = \Zend_Registry::get('em');

        $query = $em->createQueryBuilder()
            ->select('l, o')
            ->from(__CLASS__, 'l')
            ->leftJoin('l.organization', 'o')
            ->where('l.user_id = :user_id')
            ->setParameter('user_id', $user_id)
            ->orderBy('l.timestamp', 'DESC');

        if ($limit)
            $query->setMaxResults($limit);
        
        return $query->getQuery()->getArrayResult();
    }

    public static function post($log_info)
    {
        $record = new self;
        $record->fromArray($log_info);
        $record->save();
        return $record;
    }
}