<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_camps_routing")
 * @Entity
 */
class CampsFormRouting extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=100) */
    protected $name;

    /** @Column(name="secondary", type="string", nullable=true) */
    protected $secondary;

    /** @Column(name="dept", type="string", nullable=true) */
    protected $dept;

    /** @Column(name="dean", type="string", nullable=true) */
    protected $dean;
}