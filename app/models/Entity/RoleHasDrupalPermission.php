<?php
namespace Entity;


/**
 * RoleHasDrupalPermission
 *
 * @Table(name="role_has_drupal_permission")
 * @Entity
 */
class RoleHasDrupalPermission extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="role_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $role_id;

    /**
     * @Column(name="drupal_role_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $drupal_role_id;

    /**
     * @ManyToOne(targetEntity="Entity\Role")
     * @JoinColumn(name="role_id", referencedColumnName="id")
     */
    protected $role;
}