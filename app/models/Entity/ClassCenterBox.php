<?php
namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="maroonout_boxes")
 * @Entity
 */
class ClassCenterBox extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->created_at = new \DateTime('NOW');
        $this->is_deleted = FALSE;
        $this->logs = new ArrayCollection;
    }

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="box_code", type="string", length=20) */
    protected $box_code;

    /** @Column(name="shirt_type", type="string", length=50, nullable=true) */
    protected $category;

    /** @Column(name="box_size", type="string", length=5, nullable=true) */
    protected $size;

    /** @Column(name="time_created", type="unixdatetime", nullable=true) */
    protected $created_at;

    /** @Column(name="is_deleted", type="boolean", nullable=true) */
    protected $is_deleted;

    /** @OneToMany(targetEntity="ClassCenterTracking", mappedBy="box") */
    protected $logs;

    public function getCategoryName()
    {
        $settings = self::loadSettings();
        return $settings['categories'][$this->category];
    }
    public function getSizeName()
    {
        $settings = self::loadSettings();
        return $settings['sizes'][$this->size];
    }

    public function isActive()
    {
        return (!$this->is_deleted);
    }

    /**
     * Static Function
     */
    
    public static function find($id, $category)
    {
        return self::getRepository()->findOneBy(array('box_code' => trim($id), 'category' => $category));
    }

    public static function loadSettings()
    {
        static $settings;

        if ($settings === NULL)
        {
            $config = \Zend_Registry::get('module_config');
            $settings = $config['classcenter']->general->toArray();
        }

        return $settings;
    }
}