<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_camps_incidents")
 * @Entity
 */
class CampsFormIncident extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->timestamp = time();
    }

    /**
     * @Column(name="report_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="form_id", type="integer", nullable=true) */
    protected $form_id;

    /** @Column(name="report_form_id", type="string", length=50, nullable=true) */
    protected $form_old_id;

    /** @Column(name="report_timestamp", type="integer") */
    protected $timestamp;

    /** @Column(name="report_data", type="json") */
    protected $data;

    /**
     * @ManyToOne(targetEntity="CampsForm", inversedBy="incidents")
     * @JoinColumns({
     *   @JoinColumn(name="form_id", referencedColumnName="app_id")
     * })
     */
    protected $form;
}