<?php
namespace Entity;

/**
 * DrupalLoginTicket
 *
 * @Table(name="drupal_login_tickets")
 * @Entity
 */
class DrupalLoginTicket extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->timestamp = time();
    }

    /**
     * @Column(name="ticket_id", type="string", length=200)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $id;

    /** @Column(name="ticket_org_id", type="integer") */
    protected $org_id;

    /** @Column(name="ticket_timestamp", type="integer") */
    protected $timestamp;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="ticket_org_id", referencedColumnName="org_id")
     */
    protected $organization;
}