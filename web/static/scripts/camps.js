$(function() {
	processSelectableFields();

	$('form .conditional-decider').live('click', function() {
		processSelectableFields();
	});
});

var show_items;
var hide_items;

function processSelectableFields()
{
	show_items = $();
	hide_items = $('form .conditional');

	$('form .conditional-decider.yesno:checked').each(function() {
		var current_val, input_group;

		current_val = $(this).val();
		input_group = $(this).attr('name');

		var yes_elements = '.'+input_group+'-yes';
		var no_elements = '.'+input_group+'-no';

		if (current_val == 1)
		{
			show_items = show_items.add(yes_elements);
			hide_items = hide_items.not(yes_elements);
		}
		else
		{
			show_items = show_items.add(no_elements);
			hide_items = hide_items.not(no_elements);
		}
	});

	$('form .conditional-decider.checkbox:checked').each(function() {
		var current_val, input_group;

		current_val = $(this).val();
		input_group = $(this).attr('name').replace('[]', '');

		var elements = '.'+input_group+'-'+current_val;
		show_items = show_items.add(elements);
		hide_items = hide_items.not(elements);
	});

	show_items.closest('div').fadeIn();
	hide_items.closest('div').fadeOut();
}